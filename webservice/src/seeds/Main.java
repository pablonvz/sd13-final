package seeds;

import java.util.Date;

import com.buses.webservice.dto.agency.AgencyDTO;
import com.buses.webservice.dto.bus.BusDTO;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.client.ClientDTO;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.driver.DriverDTO;
import com.buses.webservice.dto.itinerary.ItineraryDTO;
import com.buses.webservice.dto.journey.JourneyDTO;
import com.buses.webservice.dto.person.PersonDTO;
import com.buses.webservice.dto.state.StateDTO;
import com.buses.webservice.dto.ticket.TicketDTO;
import com.buses.webservice.dto.user.UserDTO;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class Main {

	static final String BASE_URL = "http://localhost:8081/webservice/rest";
	static final Integer N = 28;

	public static void main(String[] args) {
		Client jerseyClient = Client.create();

		/* Countries */
		String[] countriesNames = { "Brasil", "Argentina", "Paraguay" };
		WebResource countryResource = jerseyClient.resource(BASE_URL
				+ "/country");
		System.out.println("Creating countries..");
		CountryDTO c = null;
		for (Integer i = 0; i < countriesNames.length; i++) {
			c = new CountryDTO();
			c.setName(countriesNames[i]);
			c.setCode("code" + i);
			c = countryResource.entity(c).post(CountryDTO.class);

		}
		System.out.println("Countries created.");

		/* States */
		String[] statesNames = { "Misiones", "Alto Parana", "Itapua" };
		WebResource stateResource = jerseyClient.resource(BASE_URL + "/state");
		System.out.println("Creating states..");
		StateDTO s = null;
		for (Integer i = 0; i < statesNames.length; i++) {
			s = new StateDTO();
			s.setName(statesNames[i]);
			s.setCountryId(c.getId());// paraguay
			s = stateResource.entity(s).post(StateDTO.class);

		}
		System.out.println("States created.");

		/* Cities */
		WebResource cityResource = jerseyClient.resource(BASE_URL + "/city");
		System.out.println("Creating cities..");
		String[] citiesNamesItapua = { "Hohenau", "Encarnacion", "Bella Vista" };
		String[] citiesNamesMisiones = { "San Juan Bautista", "San Ignacio",
				"Florida" };
		CityDTO cityDto = null;
		for (Integer i = 0; i < citiesNamesItapua.length; i++) {
			cityDto = new CityDTO();
			cityDto.setName(citiesNamesItapua[i]);
			cityDto.setStateId(3);
			cityDto = cityResource.entity(cityDto).post(CityDTO.class);

			cityDto = new CityDTO();
			cityDto.setName(citiesNamesMisiones[i]);
			cityDto.setStateId(1);
			cityDto = cityResource.entity(cityDto).post(CityDTO.class);
		}
		System.out.println("Cities created.");

		/* Itineraries */
		WebResource itineraryResource = jerseyClient.resource(BASE_URL
				+ "/itinerary");
		System.out.println("Creating itineraries..");
		ItineraryDTO itDto = new ItineraryDTO();
		itDto.setDestination(1);
		itDto.setOrigin(2);
		itineraryResource.entity(itDto).post(ItineraryDTO.class);

		itDto = new ItineraryDTO();
		itDto.setDestination(2);
		itDto.setOrigin(1);
		itineraryResource.entity(itDto).post(ItineraryDTO.class);

		itDto = new ItineraryDTO();
		itDto.setDestination(3);
		itDto.setOrigin(1);
		itineraryResource.entity(itDto).post(ItineraryDTO.class);

		itDto = new ItineraryDTO();
		itDto.setDestination(2);
		itDto.setOrigin(4);
		itineraryResource.entity(itDto).post(ItineraryDTO.class);

		itDto = new ItineraryDTO();
		itDto.setDestination(2);
		itDto.setOrigin(5);
		itineraryResource.entity(itDto).post(ItineraryDTO.class);

		itDto = new ItineraryDTO();
		itDto.setDestination(5);
		itDto.setOrigin(1);
		itineraryResource.entity(itDto).post(ItineraryDTO.class);

		System.out.println("itineraries created.");

		/* Agencies */
		WebResource agencyResource = jerseyClient
				.resource(BASE_URL + "/agency");
		System.out.println("Creating agencies..");
		AgencyDTO ag = null;
		for (Integer i = 0; i < citiesNamesItapua.length; i++) {
			ag = new AgencyDTO();
			ag.setCityId(i + 1);// no empieza de 0
			ag.setDescription("Agency comment" + i);
			// ag.setDeletedAt(new Date());
			ag = agencyResource.entity(ag).post(AgencyDTO.class);
		}
		System.out.println("Agencies created.");

		/* Admin user */

		System.out.println("Creating admin user..");
		/* person */
		WebResource personResource = jerseyClient
				.resource(BASE_URL + "/person");
		PersonDTO pdto = new PersonDTO();
		pdto.setBirthDate(new Date());
		pdto.setDocument("123456");
		pdto.setEmail("admin@admin.com");
		pdto.setFirstName("admin");
		pdto.setLastName("admin");
		pdto.setSex("male");
		pdto = personResource.entity(pdto).post(PersonDTO.class);
		/* user */
		WebResource userResource = jerseyClient.resource(BASE_URL + "/user");
		UserDTO udto = new UserDTO();
		udto.setAgencyId(2);// Encarnacion
		udto.setPasswordHash("21232f297a57a5a743894ae4a801fc3");
		udto.setPersonId(pdto.getId());
		userResource.entity(udto).post(UserDTO.class);
		System.out.println("Admin user created.");

		/* Buses */
		System.out.println("Creating buses..");
		WebResource busResource = jerseyClient.resource(BASE_URL + "/bus");
		String[] busBrands = { "Scania", "Volvo", "Mercedes Benz" };
		String[][] busModels = {
				{ "Touring", "HigherA30", "OmniExpress", "Irizar" },
				{ "9500", "9700", "9900" },
				{ "Integro Euro", "Integro", "Intouro" } };
		for (Integer i = 0; i < busBrands.length; i++) {
			BusDTO bdto = new BusDTO();
			bdto.setBrand(busBrands[i]);
			for (Integer j = 0; j < busModels.length; j++) {
				bdto.setCapacty(50);
				bdto.setFuel("Diesel");
				bdto.setModel(busModels[i][j]);
				bdto.setMotor("6000cc");

				bdto.setNumber(Integer.parseInt(i + "" + j));
				bdto.setYear(new Date());
				busResource.entity(bdto).post(BusDTO.class);
			}

		}
		System.out.println("Buses created.");

		/* Clients */
		System.out.println("Creating clients..");
		WebResource clientResource = jerseyClient
				.resource(BASE_URL + "/client");
		ClientDTO clientDto = new ClientDTO();

		/* persons needed first */
		pdto = new PersonDTO();
		pdto.setBirthDate(new Date());
		pdto.setDocument("127457");
		pdto.setEmail("");
		pdto.setFirstName("Ana");
		pdto.setLastName("Perez");
		pdto.setSex("female");
		pdto = personResource.entity(pdto).post(PersonDTO.class);

		clientDto.setPersonId(pdto.getId());
		clientResource.entity(clientDto).post(ClientDTO.class);

		clientDto = new ClientDTO();
		pdto = new PersonDTO();
		pdto.setBirthDate(new Date());
		pdto.setDocument("127485");
		pdto.setEmail("");
		pdto.setFirstName("Mario");
		pdto.setLastName("Lopez");
		pdto.setSex("male");
		pdto = personResource.entity(pdto).post(PersonDTO.class);

		clientDto.setPersonId(pdto.getId());
		clientResource.entity(clientDto).post(ClientDTO.class);

		clientDto = new ClientDTO();
		pdto = new PersonDTO();
		pdto.setBirthDate(new Date());
		pdto.setDocument("787485");
		pdto.setEmail("");
		pdto.setFirstName("Mercedes");
		pdto.setLastName("Aguero");
		pdto.setSex("female");
		pdto = personResource.entity(pdto).post(PersonDTO.class);

		clientDto.setPersonId(pdto.getId());
		clientResource.entity(clientDto).post(ClientDTO.class);

		clientDto = new ClientDTO();
		pdto = new PersonDTO();
		pdto.setBirthDate(new Date());
		pdto.setDocument("526485");
		pdto.setEmail("");
		pdto.setFirstName("Pablo");
		pdto.setLastName("Acosta");
		pdto.setSex("male");
		pdto = personResource.entity(pdto).post(PersonDTO.class);

		clientDto.setPersonId(pdto.getId());
		clientResource.entity(clientDto).post(ClientDTO.class);

		clientDto = new ClientDTO();
		pdto = new PersonDTO();
		pdto.setBirthDate(new Date());
		pdto.setDocument("741885");
		pdto.setEmail("");
		pdto.setFirstName("Carlos");
		pdto.setLastName("Rodriguez");
		pdto.setSex("male");
		pdto = personResource.entity(pdto).post(PersonDTO.class);

		clientDto.setPersonId(pdto.getId());
		clientResource.entity(clientDto).post(ClientDTO.class);

		System.out.println("Clients created.");

		/* Drivers */
		System.out.println("Creating drivers..");
		WebResource driverResource = jerseyClient
				.resource(BASE_URL + "/driver");
		DriverDTO driverDto = new DriverDTO();

		/* persons needed first */
		pdto = new PersonDTO();
		pdto.setBirthDate(new Date());
		pdto.setDocument("789456");
		pdto.setEmail("");
		pdto.setFirstName("Armando");
		pdto.setLastName("Paredes");
		pdto.setSex("male");
		pdto = personResource.entity(pdto).post(PersonDTO.class);

		driverDto.setPersonId(pdto.getId());
		driverDto.setAgencyId(2);
		driverResource.entity(driverDto).post(DriverDTO.class);

		driverDto = new DriverDTO();

		pdto = new PersonDTO();
		pdto.setBirthDate(new Date());
		pdto.setDocument("582456");
		pdto.setEmail("");
		pdto.setFirstName("Jorge");
		pdto.setLastName("Baez");
		pdto.setSex("male");
		pdto = personResource.entity(pdto).post(PersonDTO.class);

		driverDto.setPersonId(pdto.getId());
		driverDto.setAgencyId(2);
		driverResource.entity(driverDto).post(DriverDTO.class);

		driverDto = new DriverDTO();

		pdto = new PersonDTO();
		pdto.setBirthDate(new Date());
		pdto.setDocument("489456");
		pdto.setEmail("");
		pdto.setFirstName("Raul");
		pdto.setLastName("Arenas");
		pdto.setSex("male");
		pdto = personResource.entity(pdto).post(PersonDTO.class);

		driverDto.setPersonId(pdto.getId());
		driverDto.setAgencyId(2);
		driverResource.entity(driverDto).post(DriverDTO.class);

		System.out.println("Drivers created.");

		/* Journeys */
		System.out.println("Creating journeys..");
		WebResource journeyResource = jerseyClient.resource(BASE_URL
				+ "/journey");
		JourneyDTO journeyDto = null;
		int dId = 1;
		int itId = 1;
		for (Integer i = 0; i < 9; i++) {
			journeyDto = new JourneyDTO();
			journeyDto.setBusId(i + 1);
			journeyDto.setDate(new Date());
			if (dId > 3) {
				dId = 1;
				// drivers id go from 1 to 3
			}
			journeyDto.setDriverId(dId++);
			String iniKm = "4800".concat(i.toString());

			journeyDto.setInitialKm(Double.parseDouble(iniKm));
			if (itId > 6) {
				itId = 1;
				// itineraries id go from 1 to 6
			}
			journeyDto.setItineraryId(itId++);
			journeyDto.setPrice(50000);
			journeyResource.entity(journeyDto).post(JourneyDTO.class);

		}
		System.out.println("Journeys created.");

		/* Tickets */
		System.out.println("Creating tickets..");
		WebResource ticketResource = jerseyClient
				.resource(BASE_URL + "/ticket");
		TicketDTO ticketDto = null;

		for (Integer i = 0; i < 5; i++) {
			ticketDto = new TicketDTO();

			ticketDto.setClientId(i + 1);
			ticketDto.setJourneyId(i + 1);
			ticketDto.setSeat(i + 5);
			ticketDto.setStatus("paid");

			ticketResource.entity(ticketDto).post(TicketDTO.class);
		}
		System.out.println("Tickets created.");

	}
}