package com.buses.webservice.dao.person;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.person.PersonDomain;

@SuppressWarnings("deprecation")
@Repository
@Transactional
public class PersonDaoImpl extends BaseDaoImpl<PersonDomain> implements
		IPersonDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public PersonDomain save(PersonDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public PersonDomain getById(Integer id) {
		final PersonDomain d = (PersonDomain) sessionFactory
				.getCurrentSession().get(PersonDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PersonDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				PersonDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public boolean delete(Integer id) {
		PersonDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<PersonDomain> getByEmail(String email) {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				PersonDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		cr.add(Expression.eq("email", email));
		return cr.list();
	}

	@Override
	public List<PersonDomain> getByAny(String key) {
		return super.search(sessionFactory, PersonDomain.class.getName(),
				new String[] { "firstName", "lastName", "birthDate", "sex",
						"document", "email" }, key);
	}

	@Override
	public Integer count(String key) {
		return this.count(sessionFactory, PersonDomain.class.getName(),
				new String[] { "firstName", "lastName", "birthDate", "sex",
						"document", "email" }, key);
	}
}
