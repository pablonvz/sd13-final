package com.buses.webservice.dao.person;

import java.util.List;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.person.PersonDomain;

public interface IPersonDao extends IBaseDao<PersonDomain> {
	List<PersonDomain> getByEmail(String email);
}
