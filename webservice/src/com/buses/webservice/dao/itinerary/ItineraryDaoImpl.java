package com.buses.webservice.dao.itinerary;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.itinerary.ItineraryDomain;

@Repository
@Transactional
public class ItineraryDaoImpl extends BaseDaoImpl<ItineraryDomain> implements
		IItineraryDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public ItineraryDomain save(ItineraryDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public ItineraryDomain getById(Integer id) {
		final ItineraryDomain d = (ItineraryDomain) sessionFactory
				.getCurrentSession().get(ItineraryDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItineraryDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				ItineraryDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public boolean delete(Integer id) {
		ItineraryDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<ItineraryDomain> getByAny(String key) {
		return this.search(sessionFactory, ItineraryDomain.class.getName(),
				new String[] {}, key);
	}

	@Override
	public Integer count(String key) {
		return this.count(sessionFactory, ItineraryDomain.class.getName(),
				new String[] {}, key);
	}

}
