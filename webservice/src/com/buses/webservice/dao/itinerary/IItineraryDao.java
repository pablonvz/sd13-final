package com.buses.webservice.dao.itinerary;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.itinerary.ItineraryDomain;

public interface IItineraryDao extends IBaseDao<ItineraryDomain> {
}
