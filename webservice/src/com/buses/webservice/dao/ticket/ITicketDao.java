package com.buses.webservice.dao.ticket;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.ticket.TicketDomain;

public interface ITicketDao extends IBaseDao<TicketDomain> {

}
