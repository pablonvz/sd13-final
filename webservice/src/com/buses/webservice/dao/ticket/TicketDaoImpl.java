package com.buses.webservice.dao.ticket;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.ticket.TicketDomain;

@Repository
@Transactional
public class TicketDaoImpl extends BaseDaoImpl<TicketDomain> implements
		ITicketDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public TicketDomain save(TicketDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public TicketDomain getById(Integer id) {
		final TicketDomain d = (TicketDomain) sessionFactory
				.getCurrentSession().get(TicketDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TicketDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				TicketDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public boolean delete(Integer id) {
		TicketDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<TicketDomain> getByAny(String key) {
		return super.search(sessionFactory, TicketDomain.class.getName(),
				new String[] { "date", "seat", "status" }, key);
	}

	@Override
	public Integer count(String key) {
		return this.count(sessionFactory, TicketDomain.class.getName(),
				new String[] { "date", "seat", "status" }, key);
	}
}
