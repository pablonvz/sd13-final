package com.buses.webservice.dao.driver;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.driver.DriverDomain;

@Repository
@Transactional
public class DriverDaoImpl extends BaseDaoImpl<DriverDomain> implements
		IDriverDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public DriverDomain save(DriverDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public DriverDomain getById(Integer id) {
		final DriverDomain d = (DriverDomain) sessionFactory
				.getCurrentSession().get(DriverDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DriverDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				DriverDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public boolean delete(Integer id) {
		DriverDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<DriverDomain> getByAny(String key) {
		return this.search(sessionFactory, DriverDomain.class.getName(),
				new String[] {}, key);
	}

	@Override
	public Integer count(String key) {
		return this.count(sessionFactory, DriverDomain.class.getName(),
				new String[] {}, key);
	}
}
