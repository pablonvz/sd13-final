package com.buses.webservice.dao.driver;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.driver.DriverDomain;

public interface IDriverDao extends IBaseDao<DriverDomain> {

}
