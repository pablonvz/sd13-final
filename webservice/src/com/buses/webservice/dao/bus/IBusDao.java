package com.buses.webservice.dao.bus;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.bus.BusDomain;

public interface IBusDao extends IBaseDao<BusDomain> {

}
