package com.buses.webservice.dao.bus;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.bus.BusDomain;

@Repository
@Transactional
public class BusDaoImpl extends BaseDaoImpl<BusDomain> implements IBusDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public BusDomain save(BusDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public BusDomain getById(Integer id) {
		final BusDomain d = (BusDomain) sessionFactory.getCurrentSession().get(
				BusDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BusDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				BusDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public boolean delete(Integer id) {
		BusDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<BusDomain> getByAny(String key) {
		return super.search(sessionFactory, BusDomain.class.getName(),
				new String[] { "capacty", "number", "model", "fuel", "brand",
						"year", "motor" }, key);
	}

	@Override
	public Integer count(String key) {
		return this.count(sessionFactory, BusDomain.class.getName(),
				new String[] { "capacty", "number", "model", "fuel", "brand",
						"year", "motor" }, key);
	}
}
