package com.buses.webservice.dao.zone;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.zone.ZoneDomain;

@Repository
@Transactional
public class ZoneDaoImpl extends BaseDaoImpl<ZoneDomain> implements IZoneDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public ZoneDomain save(ZoneDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public ZoneDomain getById(Integer id) {
		final ZoneDomain d = (ZoneDomain) sessionFactory.getCurrentSession()
				.get(ZoneDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ZoneDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				ZoneDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public boolean delete(Integer id) {
		ZoneDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<ZoneDomain> getByAny(String key) {
		return super.search(sessionFactory, ZoneDomain.class.getName(),
				new String[] { "name", "comment" }, key);
	}

	@Override
	public Integer count(String key) {
		return this.count(sessionFactory, ZoneDomain.class.getName(),
				new String[] { "name", "comment" }, key);
	}
}
