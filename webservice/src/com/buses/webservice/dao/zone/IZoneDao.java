package com.buses.webservice.dao.zone;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.zone.ZoneDomain;

public interface IZoneDao extends IBaseDao<ZoneDomain> {

}
