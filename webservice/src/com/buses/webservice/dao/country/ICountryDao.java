package com.buses.webservice.dao.country;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.country.CountryDomain;

public interface ICountryDao extends IBaseDao<CountryDomain> {

}
