package com.buses.webservice.dao.country;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.country.CountryDomain;

@Repository
@Transactional
public class CountryDaoImpl extends BaseDaoImpl<CountryDomain> implements
		ICountryDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public CountryDomain save(CountryDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public CountryDomain getById(Integer id) {
		final CountryDomain d = (CountryDomain) sessionFactory
				.getCurrentSession().get(CountryDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CountryDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				CountryDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public boolean delete(Integer id) {
		CountryDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<CountryDomain> getByAny(String key) {
		return this.search(sessionFactory, CountryDomain.class.getName(),
				new String[] { "name", "code" }, key);
	}

	@Override
	public Integer count(String key) {
		return this.count(sessionFactory, CountryDomain.class.getName(),
				new String[] { "name", "code" }, key);
	}
}
