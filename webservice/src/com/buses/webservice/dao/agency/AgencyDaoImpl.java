package com.buses.webservice.dao.agency;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.agency.AgencyDomain;

@Repository
@Transactional
public class AgencyDaoImpl extends BaseDaoImpl<AgencyDomain> implements
		IAgencyDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public AgencyDomain save(AgencyDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public AgencyDomain getById(Integer id) {
		final AgencyDomain d = (AgencyDomain) sessionFactory
				.getCurrentSession().get(AgencyDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AgencyDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				AgencyDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public List<AgencyDomain> getByAny(String key) {
		return super.search(sessionFactory, AgencyDomain.class.getName(),
				new String[] { "description" }, key);
	}

	@Override
	public boolean delete(Integer id) {
		AgencyDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Integer count(String key) {
		return this.count(sessionFactory, AgencyDomain.class.getName(),
				new String[] { "description" }, key);
	}
}
