package com.buses.webservice.dao.agency;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.agency.AgencyDomain;

public interface IAgencyDao extends IBaseDao<AgencyDomain> {

}
