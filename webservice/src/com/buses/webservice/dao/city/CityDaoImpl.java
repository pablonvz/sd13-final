package com.buses.webservice.dao.city;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.city.CityDomain;

@Repository
@Transactional
public class CityDaoImpl extends BaseDaoImpl<CityDomain> implements ICityDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public CityDomain save(CityDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public CityDomain getById(Integer id) {
		final CityDomain d = (CityDomain) sessionFactory.getCurrentSession()
				.get(CityDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CityDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				CityDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public boolean delete(Integer id) {
		CityDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<CityDomain> getByAny(String key) {
		return super.search(sessionFactory, CityDomain.class.getName(),
				new String[] { "name" }, key);
	}

	@Override
	public Integer count(String key) {
		return this.count(sessionFactory, CityDomain.class.getName(),
				new String[] { "name" }, key);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CityDomain> getMostPopularOrigin(Date start, Date end) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String sql = "SELECT  city.* "
				// + "COUNT(city.id) "
				+ "FROM ticket, journey, itinerary, city "
				+ "WHERE ticket.journeyId = journey.id AND "
				+ "journey.itineraryId = itinerary.id AND "
				+ " ticket.status != 'canceled' AND "
				+ "itinerary.origin = city.id "
				+ "  AND journey.date BETWEEN '" + formatter.format(start)
				+ " 00:00:00' AND '" + formatter.format(end) + " 23:59:00'"
				+ "GROUP BY city.id";
		Query q = sessionFactory.getCurrentSession().createSQLQuery(sql)
				.addEntity(CityDomain.class);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getMostPopularOriginCount(Date start, Date end) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String sql = "SELECT  " + "COUNT(city.id) "
				+ "FROM ticket, journey, itinerary, city "
				+ "WHERE ticket.journeyId = journey.id AND "
				+ "journey.itineraryId = itinerary.id AND "
				+ " ticket.status != 'canceled' AND "
				+ "itinerary.origin = city.id "
				+ "  AND journey.date BETWEEN '" + formatter.format(start)
				+ " 00:00:00' AND '" + formatter.format(end) + " 23:59:00'"
				+ "GROUP BY city.id";
		Query q = sessionFactory.getCurrentSession().createSQLQuery(sql);
		List<Integer> l = new ArrayList<>();
		List<BigInteger> r = q.list();
		for (BigInteger o : r)
			l.add(o.intValue());
		return l;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CityDomain> getMostPopularDestination(Date start, Date end) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String sql = "SELECT  city.* "
				// + "COUNT(city.id) "
				+ "FROM ticket, journey, itinerary, city "
				+ "WHERE ticket.journeyId = journey.id AND "
				+ "journey.itineraryId = itinerary.id AND "
				+ " ticket.status != 'canceled' AND "
				+ "itinerary.destination = city.id "
				+ "  AND journey.date BETWEEN '" + formatter.format(start)
				+ " 00:00:00' AND '" + formatter.format(end) + " 23:59:00'"
				+ "GROUP BY city.id";
		Query q = sessionFactory.getCurrentSession().createSQLQuery(sql)
				.addEntity(CityDomain.class);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getMostPopularDestinationCount(Date start, Date end) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String sql = "SELECT  " + "COUNT(city.id) "
				+ "FROM ticket, journey, itinerary, city "
				+ "WHERE ticket.journeyId = journey.id AND "
				+ "journey.itineraryId = itinerary.id AND "
				+ " ticket.status != 'canceled' AND "
				+ "itinerary.destination = city.id "
				+ "  AND journey.date BETWEEN '" + formatter.format(start)
				+ " 00:00:00' AND '" + formatter.format(end) + " 23:59:00'"
				+ "GROUP BY city.id";
		Query q = sessionFactory.getCurrentSession().createSQLQuery(sql);
		List<Integer> l = new ArrayList<>();
		List<BigInteger> r = q.list();
		for (BigInteger o : r)
			l.add(o.intValue());
		return l;
	}

	@Override
	public CityDomain getCityWithMoreTickets(Date start, Date end, int id) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String sql = "SELECT  city.* "
				// + "COUNT(city.id) "
				+ "FROM ticket, journey, itinerary, city "
				+ "WHERE ticket.journeyId = journey.id AND "
				+ "journey.itineraryId = itinerary.id AND "
				+ " ticket.status = 'paid' AND "
				+ "itinerary.origin = city.id "
				+ "  AND journey.date BETWEEN '" + formatter.format(start)
				+ " 00:00:00' AND '" + formatter.format(end) + " 23:59:00'"
				+ " AND city.id = " + id + " " + "GROUP BY city.id";
		Query q = sessionFactory.getCurrentSession().createSQLQuery(sql)
				.addEntity(CityDomain.class);
		@SuppressWarnings("unchecked")
		List<Object> l = q.list();
		return l.isEmpty() ? new CityDomain() : (CityDomain) l.get(0);
	}

	@Override
	public Integer getCityWithMoreTicketsCount(Date start, Date end, int id) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String sql = "SELECT  " + "COUNT(city.id) "
				+ "FROM ticket, journey, itinerary, city "
				+ "WHERE ticket.journeyId = journey.id AND "
				+ "journey.itineraryId = itinerary.id AND "
				+ " ticket.status = 'paid' AND "
				+ "itinerary.origin = city.id "
				+ "  AND journey.date BETWEEN '" + formatter.format(start)
				+ " 00:00:00' AND '" + formatter.format(end) + " 23:59:00'"
				+ " AND city.id = " + id + " " + "GROUP BY city.id";
		Query q = sessionFactory.getCurrentSession().createSQLQuery(sql);
		@SuppressWarnings("unchecked")
		List<BigInteger> r = q.list();
		return r.isEmpty() ? -1 : r.get(0).intValue();
	}
}
