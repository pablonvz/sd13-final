package com.buses.webservice.dao.city;

import java.util.Date;
import java.util.List;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.city.CityDomain;

public interface ICityDao extends IBaseDao<CityDomain> {
	public List<CityDomain> getMostPopularOrigin(Date start, Date end);

	public List<Integer> getMostPopularOriginCount(Date start, Date end);

	public List<CityDomain> getMostPopularDestination(Date start, Date end);

	public List<Integer> getMostPopularDestinationCount(Date start, Date end);

	public CityDomain getCityWithMoreTickets(Date start, Date end, int id);

	public Integer getCityWithMoreTicketsCount(Date start, Date end, int id);

}
