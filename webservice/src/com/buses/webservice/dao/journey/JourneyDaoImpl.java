package com.buses.webservice.dao.journey;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.journey.JourneyDomain;

@Repository
@Transactional
public class JourneyDaoImpl extends BaseDaoImpl<JourneyDomain> implements
		IJourneyDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public JourneyDomain save(JourneyDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public JourneyDomain getById(Integer id) {
		final JourneyDomain d = (JourneyDomain) sessionFactory
				.getCurrentSession().get(JourneyDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JourneyDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				JourneyDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public boolean delete(Integer id) {
		JourneyDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<JourneyDomain> getByAny(String key) {
		return super
				.search(sessionFactory, JourneyDomain.class.getName(),
						new String[] { "price", "initialKm", "endKm",
								"incident" }, key);
	}

	@Override
	public Integer count(String key) {
		return this
				.count(sessionFactory, JourneyDomain.class.getName(),
						new String[] { "price", "initialKm", "endKm",
								"incident" }, key);
	}
}
