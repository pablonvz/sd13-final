package com.buses.webservice.dao.journey;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.journey.JourneyDomain;

public interface IJourneyDao extends IBaseDao<JourneyDomain> {

}
