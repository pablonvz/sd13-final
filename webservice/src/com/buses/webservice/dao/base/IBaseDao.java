package com.buses.webservice.dao.base;

import java.util.List;

import com.buses.webservice.domain.base.BaseDomain;

public interface IBaseDao<DOMAIN extends BaseDomain> {

	public DOMAIN save(DOMAIN domain);

	public DOMAIN getById(Integer domainId);

	public List<DOMAIN> findAll();

	public boolean delete(Integer domainId);

	public List<DOMAIN> getByAny(String key);

	public Integer count(String key);
}
