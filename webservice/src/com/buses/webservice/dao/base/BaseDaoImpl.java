package com.buses.webservice.dao.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.buses.webservice.domain.base.BaseDomain;

public abstract class BaseDaoImpl<DOMAIN extends BaseDomain> implements
		IBaseDao<DOMAIN> {

	private final Random _random;

	public BaseDaoImpl() {
		_random = new Random();
	}

	protected Integer generateId() {
		return Math.abs(_random.nextInt());
	}

	private Map<String, String> getQueryMap(String query) {
		if (query.indexOf("=") < 0)
			return null;
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();
		for (String param : params) {
			String name = param.split("=")[0];
			String value = param.split("=")[1];
			map.put(name, value);
		}
		return map;
	}

	private String getOrderQuery(String o) {
		o = o.toUpperCase();
		return o.equals("ASC") || o.equals("DESC") ? " " + o + " " : " ";
	}

	/*
	 * Criteria c=
	 * sessionFactory.getCurrentSession().createCriteria(ItineraryDomain.class)
	 * .setProjection( Projections.projectionList()
	 * .add(Projections.groupProperty("id"))
	 * .add(Projections.rowCount(),"rowcount") )
	 * .addOrder(org.hibernate.criterion.Order.desc("rowcount"));
	 * c.add(Restrictions.isNull("deletedAt")); List<Object[]> l =
	 * (List<Object[]>) c.list(); List<ItineraryDomain> is = new
	 * ArrayList<ItineraryDomain>(); for(Object[] o : l)
	 * is.add(this.getById((int)o[0])); return is;
	 */
	@SuppressWarnings("unchecked")
	public List<DOMAIN> search(SessionFactory sessionFactory, String tableName,
			String[] fields, String key) {
		StringBuilder hql = new StringBuilder();
		String element = "e";
		hql.append("from " + tableName + " " + element);
		hql.append(" where " + element + ".deletedAt is null ");

		Map<String, String> params = getQueryMap(key);
		Query query = null;

		if (params != null) {
			fields = (params.get("fields") == null ? "" : params.get("fields"))
					.split(",");

			String value = params.get("value");

			boolean willSearchBy = fields.length > 0 && value != null;

			if (willSearchBy) {
				hql.append(" and (");
				for (int i = 0; i < fields.length; i++) {
					String field = fields[i];
					if (i == 0) {
						hql.append("str(");
					} else {
						hql.append(" or str(");
					}
					hql.append(element);
					hql.append(".");
					hql.append(field);
					String criteria = params.get("criteria");
					criteria = criteria != null && criteria.length() > 0 ? criteria
							: "like";
					hql.append(") " + criteria + " :");
					hql.append(field);
				}
				hql.append(") ");
			}

			Integer limit = params.get("max") == null ? null : Integer
					.parseInt(params.get("max"));
			Integer offset = params.get("offset") == null ? null : Integer
					.parseInt(params.get("offset"));
			String sortBy = params.get("sort");
			String order = params.get("order");

			if (sortBy != null) {
				hql.append("order by " + sortBy);
				if (order != null)
					hql.append(getOrderQuery(order));
			} else if (order != null)
				hql.append("order " + getOrderQuery(order));

			query = sessionFactory.getCurrentSession().createQuery(
					hql.toString());

			if (offset != null)
				query.setFirstResult(offset);
			if (limit != null)
				query.setMaxResults(limit);

			if (willSearchBy) {
				String like = params.get("like");
				String d = (like == null ? "" : like).equalsIgnoreCase("false") ? ""
						: "%";
				for (int i = 0; i < fields.length; i++) {
					query.setParameter(fields[i], d + value + d);
				}
			}
		} else {
			query = sessionFactory.getCurrentSession().createQuery(
					hql.toString());
		}
		return query.list();
	}

	protected Integer count(SessionFactory sessionFactory, String tableName,
			String[] fields, String key) {

		StringBuilder hql = new StringBuilder();
		String element = "e";
		hql.append("select count(*) from " + tableName + " " + element);
		hql.append(" where " + element + ".deletedAt is null ");

		Map<String, String> params = getQueryMap(key);
		boolean willSearchBy = false;
		String value = null;
		if (params != null) {
			fields = (params.get("fields") == null ? "" : params.get("fields"))
					.split(",");

			value = params.get("value");

			willSearchBy = fields.length > 0 && value != null && key != null
					&& key.length() > 0;

			if (willSearchBy) {
				hql.append(" and (");
				for (int i = 0; i < fields.length; i++) {
					String field = fields[i];
					if (i == 0) {
						hql.append("str(");
					} else {
						hql.append(" or str(");
					}
					hql.append(element);
					hql.append(".");
					hql.append(field);
					hql.append(") like :");
					hql.append(field);
				}
				hql.append(") ");
			}
		}
		Query query = sessionFactory.getCurrentSession().createQuery(
				hql.toString());

		if (willSearchBy) {
			String like = params.get("like");
			String d = (like == null ? "" : like).equalsIgnoreCase("false") ? ""
					: "%";
			for (int i = 0; i < fields.length; i++) {
				query.setParameter(fields[i], d + value + d);
			}
		}

		return ((Long) query.uniqueResult()).intValue();
	}

	public abstract DOMAIN save(DOMAIN domain);

	public abstract DOMAIN getById(Integer domainId);

	public abstract List<DOMAIN> findAll();

	public abstract boolean delete(Integer domainId);

	public abstract List<DOMAIN> getByAny(String key);
}
