package com.buses.webservice.dao.state;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.state.StateDomain;

@Repository
@Transactional
public class StateDaoImpl extends BaseDaoImpl<StateDomain> implements IStateDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public StateDomain save(StateDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public StateDomain getById(Integer id) {
		final StateDomain d = (StateDomain) sessionFactory.getCurrentSession()
				.get(StateDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StateDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				StateDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public boolean delete(Integer id) {
		StateDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<StateDomain> getByAny(String key) {
		return super.search(sessionFactory, StateDomain.class.getName(),
				new String[] { "name" }, key);
	}

	@Override
	public Integer count(String key) {
		return this.count(sessionFactory, StateDomain.class.getName(),
				new String[] { "name" }, key);
	}
}
