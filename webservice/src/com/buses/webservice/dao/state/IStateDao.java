package com.buses.webservice.dao.state;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.state.StateDomain;

public interface IStateDao extends IBaseDao<StateDomain> {

}
