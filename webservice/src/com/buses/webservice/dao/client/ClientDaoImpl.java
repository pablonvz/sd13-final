package com.buses.webservice.dao.client;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.client.ClientDomain;

@Repository
@Transactional
public class ClientDaoImpl extends BaseDaoImpl<ClientDomain> implements
		IClientDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public ClientDomain save(ClientDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public ClientDomain getById(Integer id) {
		final ClientDomain d = (ClientDomain) sessionFactory
				.getCurrentSession().get(ClientDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ClientDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				ClientDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public boolean delete(Integer id) {
		ClientDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<ClientDomain> getByAny(String key) {
		return this.search(sessionFactory, ClientDomain.class.getName(),
				new String[] {}, key);
	}

	@Override
	public Integer count(String key) {
		return this.count(sessionFactory, ClientDomain.class.getName(),
				new String[] {}, key);
	}
}
