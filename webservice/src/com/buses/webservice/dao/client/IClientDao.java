package com.buses.webservice.dao.client;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.client.ClientDomain;

public interface IClientDao extends IBaseDao<ClientDomain> {

}
