package com.buses.webservice.dao.user;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.dao.person.IPersonDao;
import com.buses.webservice.domain.user.UserDomain;

@SuppressWarnings("deprecation")
@Repository
@Transactional
public class UserDaoImpl extends BaseDaoImpl<UserDomain> implements IUserDao {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private IPersonDao personDaoImpl;

	@Override
	public UserDomain save(UserDomain domain) {
		if (null == domain.getId()) {
			domain.setId(generateId());
			sessionFactory.getCurrentSession().save(domain);
		} else {
			sessionFactory.getCurrentSession().update(domain);
		}

		return domain;
	}

	@Override
	public UserDomain getById(Integer id) {
		final UserDomain d = (UserDomain) sessionFactory.getCurrentSession()
				.get(UserDomain.class, id);
		return d != null ? (d.getDeletedAt() != null ? null : d) : d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserDomain> findAll() {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				UserDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		return cr.list();
	}

	@Override
	public boolean delete(Integer id) {
		UserDomain domain = this.getById(id);
		if (domain != null) {
			domain.setDeletedAt(new Date());
			this.save(domain);
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserDomain> getByPersonId(Integer personId) {
		final Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				UserDomain.class);
		cr.add(Restrictions.isNull("deletedAt"));
		cr.add(Expression.eq("personId", personId));
		return cr.list();
	}

	@Override
	public List<UserDomain> getByAny(String key) {
		return this.search(sessionFactory, UserDomain.class.getName(),
				new String[] {}, key);
	}

	@Override
	public Integer count(String key) {
		return this.count(sessionFactory, UserDomain.class.getName(),
				new String[] {}, key);
	}
}
