package com.buses.webservice.dao.user;

import java.util.List;

import com.buses.webservice.dao.base.IBaseDao;
import com.buses.webservice.domain.user.UserDomain;

public interface IUserDao extends IBaseDao<UserDomain> {

	List<UserDomain> getByPersonId(Integer personId);

}
