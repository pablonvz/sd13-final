package com.buses.webservice.service.person;

import com.buses.webservice.dao.person.PersonDaoImpl;
import com.buses.webservice.domain.person.PersonDomain;
import com.buses.webservice.dto.person.PersonDTO;
import com.buses.webservice.dto.person.PersonResult;
import com.buses.webservice.service.base.IBaseService;

public interface IPersonService extends
		IBaseService<PersonDTO, PersonDomain, PersonDaoImpl, PersonResult> {

	PersonResult getByEmail(String email);
}
