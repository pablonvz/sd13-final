package com.buses.webservice.service.person;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.person.IPersonDao;
import com.buses.webservice.dao.person.PersonDaoImpl;
import com.buses.webservice.domain.person.PersonDomain;
import com.buses.webservice.dto.person.PersonDTO;
import com.buses.webservice.dto.person.PersonResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class PersonServiceImpl extends
		BaseServiceImpl<PersonDTO, PersonDomain, PersonDaoImpl, PersonResult>
		implements IPersonService {

	@Autowired
	private IPersonDao personDao;

	@Override
	@Transactional
	public PersonDTO save(PersonDTO dto) {
		final PersonDomain domain = convertDtoToDomain(dto);
		final PersonDomain personDomain = personDao.save(domain);
		return convertDomainToDto(personDomain);
	}

	@Override
	@Transactional
	public PersonDTO getById(Integer id) {
		final PersonDomain domain = personDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return personDao.delete(id);
	}

	@Override
	@Transactional
	public PersonResult getAll() {
		final List<PersonDTO> people = new ArrayList<>();
		for (PersonDomain domain : personDao.findAll()) {
			final PersonDTO dto = convertDomainToDto(domain);
			people.add(dto);
		}

		final PersonResult personResult = new PersonResult();
		personResult.setPersons(people);
		return personResult;
	}

	@Override
	protected PersonDTO convertDomainToDto(PersonDomain domain) {
		final PersonDTO dto = new PersonDTO();
		dto.setId(domain.getId());
		dto.setFirstName(domain.getFirstName());
		dto.setLastName(domain.getLastName());
		dto.setBirthDate(domain.getBirthDate());
		dto.setSex(domain.getSex());
		dto.setDocument(domain.getDocument());
		dto.setEmail(domain.getEmail());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected PersonDomain convertDtoToDomain(PersonDTO dto) {
		final PersonDomain domain = new PersonDomain();
		domain.setId(dto.getId());
		domain.setFirstName(dto.getFirstName());
		domain.setLastName(dto.getLastName());
		domain.setBirthDate(dto.getBirthDate());
		domain.setSex(dto.getSex());
		domain.setDocument(dto.getDocument());
		domain.setEmail(dto.getEmail());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	public PersonResult getByEmail(String email) {
		final List<PersonDTO> people = new ArrayList<>();
		for (PersonDomain domain : personDao.getByEmail(email)) {
			final PersonDTO dto = convertDomainToDto(domain);
			people.add(dto);
		}

		final PersonResult personResult = new PersonResult();
		personResult.setPersons(people);
		return personResult;
	}

	@Override
	@Transactional
	public PersonResult searchForAnyProperty(String key) {
		final List<PersonDTO> people = new ArrayList<>();
		for (PersonDomain domain : personDao.getByAny(key)) {
			final PersonDTO dto = convertDomainToDto(domain);
			people.add(dto);
		}

		final PersonResult personResult = new PersonResult();
		personResult.setPersons(people);
		return personResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return personDao.count(key);
	}
}
