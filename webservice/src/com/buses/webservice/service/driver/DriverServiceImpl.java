package com.buses.webservice.service.driver;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.driver.DriverDaoImpl;
import com.buses.webservice.dao.driver.IDriverDao;
import com.buses.webservice.domain.driver.DriverDomain;
import com.buses.webservice.dto.driver.DriverDTO;
import com.buses.webservice.dto.driver.DriverResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class DriverServiceImpl extends
		BaseServiceImpl<DriverDTO, DriverDomain, DriverDaoImpl, DriverResult>
		implements IDriverService {

	@Autowired
	private IDriverDao driverDao;

	@Override
	@Transactional
	public DriverDTO save(DriverDTO dto) {
		final DriverDomain domain = convertDtoToDomain(dto);
		final DriverDomain driverDomain = driverDao.save(domain);
		return convertDomainToDto(driverDomain);
	}

	@Override
	@Transactional
	public DriverDTO getById(Integer id) {
		final DriverDomain domain = driverDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return driverDao.delete(id);
	}

	@Override
	@Transactional
	public DriverResult getAll() {
		final List<DriverDTO> drivers = new ArrayList<>();
		for (DriverDomain domain : driverDao.findAll()) {
			final DriverDTO dto = convertDomainToDto(domain);
			drivers.add(dto);
		}

		final DriverResult driverResult = new DriverResult();
		driverResult.setDrivers(drivers);
		return driverResult;
	}

	@Override
	protected DriverDTO convertDomainToDto(DriverDomain domain) {
		final DriverDTO dto = new DriverDTO();
		dto.setId(domain.getId());
		dto.setPersonId(domain.getPersonId());
		dto.setAgencyId(domain.getAgencyId());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected DriverDomain convertDtoToDomain(DriverDTO dto) {
		final DriverDomain domain = new DriverDomain();
		domain.setId(dto.getId());
		domain.setPersonId(dto.getPersonId());
		domain.setAgencyId(dto.getAgencyId());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	@Transactional
	public DriverResult searchForAnyProperty(String key) {
		final List<DriverDTO> drivers = new ArrayList<>();
		for (DriverDomain domain : driverDao.getByAny(key)) {
			final DriverDTO dto = convertDomainToDto(domain);
			drivers.add(dto);
		}

		final DriverResult driverResult = new DriverResult();
		driverResult.setDrivers(drivers);
		return driverResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return driverDao.count(key);
	}
}
