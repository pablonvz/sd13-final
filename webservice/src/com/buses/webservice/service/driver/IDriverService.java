package com.buses.webservice.service.driver;

import com.buses.webservice.dao.driver.DriverDaoImpl;
import com.buses.webservice.domain.driver.DriverDomain;
import com.buses.webservice.dto.driver.DriverDTO;
import com.buses.webservice.dto.driver.DriverResult;
import com.buses.webservice.service.base.IBaseService;

public interface IDriverService extends
		IBaseService<DriverDTO, DriverDomain, DriverDaoImpl, DriverResult> {

}
