package com.buses.webservice.service.city;

import java.util.List;

import com.buses.webservice.dao.city.CityDaoImpl;
import com.buses.webservice.domain.city.CityDomain;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.city.CityResult;
import com.buses.webservice.service.base.IBaseService;

public interface ICityService extends
		IBaseService<CityDTO, CityDomain, CityDaoImpl, CityResult> {
	public CityResult getMostPopular(String kind);

	public List<Integer> getMostPopularCount(String kind);

	public CityDTO getCityWithMoreTickets(String params);

	public Integer getCityWithMoreTicketsCount(String params);

}
