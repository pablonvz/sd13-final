package com.buses.webservice.service.city;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.city.CityDaoImpl;
import com.buses.webservice.dao.city.ICityDao;
import com.buses.webservice.domain.city.CityDomain;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.city.CityResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class CityServiceImpl extends
		BaseServiceImpl<CityDTO, CityDomain, CityDaoImpl, CityResult> implements
		ICityService {

	@Autowired
	private ICityDao cityDao;

	@Override
	@Transactional
	public CityDTO save(CityDTO dto) {
		final CityDomain domain = convertDtoToDomain(dto);
		final CityDomain cityDomain = cityDao.save(domain);
		return convertDomainToDto(cityDomain);
	}

	@Override
	@Transactional
	public CityDTO getById(Integer id) {
		final CityDomain domain = cityDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return cityDao.delete(id);
	}

	@Override
	@Transactional
	public CityResult getAll() {
		final List<CityDTO> cities = new ArrayList<>();
		List<CityDomain> r = cityDao.findAll();
		for (CityDomain domain : r) {
			final CityDTO dto = convertDomainToDto(domain);
			cities.add(dto);
		}

		final CityResult cityResult = new CityResult();
		cityResult.setCitys(cities);
		return cityResult;
	}

	@Override
	protected CityDTO convertDomainToDto(CityDomain domain) {
		final CityDTO dto = new CityDTO();
		dto.setId(domain.getId());
		dto.setStateId(domain.getStateId());
		dto.setName(domain.getName());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected CityDomain convertDtoToDomain(CityDTO dto) {
		final CityDomain domain = new CityDomain();
		domain.setId(dto.getId());
		domain.setStateId(dto.getStateId());
		domain.setName(dto.getName());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	@Transactional
	public CityResult searchForAnyProperty(String key) {
		final List<CityDTO> cities = new ArrayList<>();
		for (CityDomain domain : cityDao.getByAny(key)) {
			final CityDTO dto = convertDomainToDto(domain);
			cities.add(dto);
		}

		final CityResult cityResult = new CityResult();
		cityResult.setCitys(cities);
		return cityResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return cityDao.count(key);
	}

	private CityResult convertListToResult(List<CityDomain> l) {
		final List<CityDTO> cities = new ArrayList<>();
		for (CityDomain domain : l) {
			final CityDTO dto = convertDomainToDto(domain);
			cities.add(dto);
		}

		final CityResult cityResult = new CityResult();
		cityResult.setCitys(cities);
		return cityResult;
	}

	@Override
	public CityResult getMostPopular(String params) {
		if (params == null)
			return new CityResult();
		Map<String, String> qm = getQueryMap(params);
		String kind = qm.get("kind");
		String between = qm.get("between");
		if (kind != null && between != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date start = null;
			try {
				start = formatter.parse(between.split(",")[0]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Date end = null;
			try {
				end = formatter.parse(between.split(",")[1]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (kind.equalsIgnoreCase("origin")) {
				final HashMap<CityDomain, Integer> hm = new HashMap<CityDomain, Integer>();
				List<CityDomain> lc = cityDao.getMostPopularOrigin(start, end);
				List<Integer> li = cityDao
						.getMostPopularOriginCount(start, end);
				for (int i = 0; i < lc.size(); i++) {
					hm.put(lc.get(i), li.get(i));
				}

				Collections.sort(lc, new Comparator<CityDomain>() {
					@Override
					public int compare(CityDomain o1, CityDomain o2) {
						Integer i1 = hm.get(o1);
						Integer i2 = hm.get(o2);
						return i1.compareTo(i2);
					}
				});
				Collections.reverse(lc);
				return convertListToResult(lc);
			}
			if (kind.equalsIgnoreCase("destination")) {
				final HashMap<CityDomain, Integer> hm = new HashMap<CityDomain, Integer>();
				List<CityDomain> lc = cityDao.getMostPopularDestination(start,
						end);
				List<Integer> li = cityDao.getMostPopularDestinationCount(
						start, end);
				for (int i = 0; i < lc.size(); i++) {
					hm.put(lc.get(i), li.get(i));
				}

				Collections.sort(lc, new Comparator<CityDomain>() {
					@Override
					public int compare(CityDomain o1, CityDomain o2) {
						Integer i1 = hm.get(o1);
						Integer i2 = hm.get(o2);
						return i1.compareTo(i2);
					}
				});
				Collections.reverse(lc);
				return convertListToResult(lc);
			}
		}
		return new CityResult();
	}

	@Override
	public List<Integer> getMostPopularCount(String params) {
		if (params == null)
			return new ArrayList<Integer>();
		Map<String, String> qm = getQueryMap(params);
		String kind = qm.get("kind");
		String between = qm.get("between");
		System.out.println(kind);
		System.out.println(between);
		if (kind != null && between != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date start = null;
			try {
				start = formatter.parse(between.split(",")[0]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Date end = null;
			try {
				end = formatter.parse(between.split(",")[1]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (kind.equalsIgnoreCase("origin")) {
				List<Integer> li = cityDao
						.getMostPopularOriginCount(start, end);
				Collections.sort(li);
				Collections.reverse(li);
				return li;
			}
			if (kind.equalsIgnoreCase("destination")) {
				List<Integer> li = cityDao.getMostPopularDestinationCount(
						start, end);
				Collections.sort(li);
				Collections.reverse(li);
				return li;
			}
		}
		return new ArrayList<Integer>();
	}

	@Override
	public CityDTO getCityWithMoreTickets(String params) {
		if (params == null)
			return new CityDTO();
		Map<String, String> qm = getQueryMap(params);
		String between = qm.get("between");
		String idS = qm.get("id");
		if (between != null && idS != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date start = null;
			int id = Integer.parseInt(idS);
			try {
				start = formatter.parse(between.split(",")[0]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Date end = null;
			try {
				end = formatter.parse(between.split(",")[1]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return convertDomainToDto(cityDao.getCityWithMoreTickets(start,
					end, id));
		}
		return new CityDTO();
	}

	@Override
	public Integer getCityWithMoreTicketsCount(String params) {
		if (params == null)
			return -1;
		Map<String, String> qm = getQueryMap(params);
		String between = qm.get("between");
		String idS = qm.get("id");
		if (between != null && idS != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date start = null;
			int id = Integer.parseInt(idS);
			try {
				start = formatter.parse(between.split(",")[0]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Date end = null;
			try {
				end = formatter.parse(between.split(",")[1]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return cityDao.getCityWithMoreTicketsCount(start, end, id);
		}
		return -1;
	}

}
