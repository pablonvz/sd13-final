package com.buses.webservice.service.journey;

import com.buses.webservice.dao.journey.JourneyDaoImpl;
import com.buses.webservice.domain.journey.JourneyDomain;
import com.buses.webservice.dto.journey.JourneyDTO;
import com.buses.webservice.dto.journey.JourneyResult;
import com.buses.webservice.service.base.IBaseService;

public interface IJourneyService extends
		IBaseService<JourneyDTO, JourneyDomain, JourneyDaoImpl, JourneyResult> {

}
