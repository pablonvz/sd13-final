package com.buses.webservice.service.journey;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.journey.IJourneyDao;
import com.buses.webservice.dao.journey.JourneyDaoImpl;
import com.buses.webservice.domain.journey.JourneyDomain;
import com.buses.webservice.dto.journey.JourneyDTO;
import com.buses.webservice.dto.journey.JourneyResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class JourneyServiceImpl
		extends
		BaseServiceImpl<JourneyDTO, JourneyDomain, JourneyDaoImpl, JourneyResult>
		implements IJourneyService {

	@Autowired
	private IJourneyDao journeyDao;

	@Override
	@Transactional
	public JourneyDTO save(JourneyDTO dto) {
		final JourneyDomain domain = convertDtoToDomain(dto);
		final JourneyDomain journeyDomain = journeyDao.save(domain);
		return convertDomainToDto(journeyDomain);
	}

	@Override
	@Transactional
	public JourneyDTO getById(Integer id) {
		final JourneyDomain domain = journeyDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return journeyDao.delete(id);
	}

	@Override
	@Transactional
	public JourneyResult getAll() {
		final List<JourneyDTO> journeys = new ArrayList<>();
		for (JourneyDomain domain : journeyDao.findAll()) {
			final JourneyDTO dto = convertDomainToDto(domain);
			journeys.add(dto);
		}

		final JourneyResult journeyResult = new JourneyResult();
		journeyResult.setJourneys(journeys);
		return journeyResult;
	}

	@Override
	protected JourneyDTO convertDomainToDto(JourneyDomain domain) {
		final JourneyDTO dto = new JourneyDTO();
		dto.setId(domain.getId());
		dto.setItineraryId(domain.getItineraryId());
		dto.setBusId(domain.getBusId());
		dto.setDate(domain.getDate());
		dto.setDriverId(domain.getDriverId());
		dto.setPrice(domain.getPrice());
		dto.setInitialKm(domain.getInitialKm());
		dto.setEndKm(domain.getEndKm());
		dto.setIncident(domain.getIncident());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected JourneyDomain convertDtoToDomain(JourneyDTO dto) {
		final JourneyDomain domain = new JourneyDomain();
		domain.setId(dto.getId());
		domain.setItineraryId(dto.getItineraryId());
		domain.setBusId(dto.getBusId());
		domain.setDate(dto.getDate());
		domain.setDriverId(dto.getDriverId());
		domain.setPrice(dto.getPrice());
		domain.setInitialKm(dto.getInitialKm());
		domain.setEndKm(dto.getEndKm());
		domain.setIncident(dto.getIncident());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	@Transactional
	public JourneyResult searchForAnyProperty(String key) {
		final List<JourneyDTO> journeis = new ArrayList<>();
		for (JourneyDomain domain : journeyDao.getByAny(key)) {
			final JourneyDTO dto = convertDomainToDto(domain);
			journeis.add(dto);
		}

		final JourneyResult journeyResult = new JourneyResult();
		journeyResult.setJourneys(journeis);
		return journeyResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return journeyDao.count(key);
	}
}
