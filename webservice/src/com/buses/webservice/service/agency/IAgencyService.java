package com.buses.webservice.service.agency;

import com.buses.webservice.dao.agency.AgencyDaoImpl;
import com.buses.webservice.domain.agency.AgencyDomain;
import com.buses.webservice.dto.agency.AgencyDTO;
import com.buses.webservice.dto.agency.AgencyResult;
import com.buses.webservice.service.base.IBaseService;

public interface IAgencyService extends
		IBaseService<AgencyDTO, AgencyDomain, AgencyDaoImpl, AgencyResult> {

}
