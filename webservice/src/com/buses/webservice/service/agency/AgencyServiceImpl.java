package com.buses.webservice.service.agency;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.agency.AgencyDaoImpl;
import com.buses.webservice.dao.agency.IAgencyDao;
import com.buses.webservice.domain.agency.AgencyDomain;
import com.buses.webservice.dto.agency.AgencyDTO;
import com.buses.webservice.dto.agency.AgencyResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class AgencyServiceImpl extends
		BaseServiceImpl<AgencyDTO, AgencyDomain, AgencyDaoImpl, AgencyResult>
		implements IAgencyService {

	@Autowired
	private IAgencyDao agencyDao;

	@Override
	@Transactional
	public AgencyDTO save(AgencyDTO dto) {
		final AgencyDomain domain = convertDtoToDomain(dto);
		final AgencyDomain agencyDomain = agencyDao.save(domain);
		return convertDomainToDto(agencyDomain);
	}

	@Override
	@Transactional
	public AgencyDTO getById(Integer id) {
		final AgencyDomain domain = agencyDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return agencyDao.delete(id);
	}

	@Override
	@Transactional
	public AgencyResult getAll() {
		final List<AgencyDTO> agencies = new ArrayList<>();
		for (AgencyDomain domain : agencyDao.findAll()) {
			final AgencyDTO dto = convertDomainToDto(domain);
			agencies.add(dto);
		}

		final AgencyResult agencyResult = new AgencyResult();
		agencyResult.setAgencys(agencies);
		return agencyResult;
	}

	@Override
	protected AgencyDTO convertDomainToDto(AgencyDomain domain) {
		final AgencyDTO dto = new AgencyDTO();
		dto.setId(domain.getId());
		dto.setCityId(domain.getCityId());
		dto.setDescription(domain.getDescription());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected AgencyDomain convertDtoToDomain(AgencyDTO dto) {
		final AgencyDomain domain = new AgencyDomain();
		domain.setId(dto.getId());
		domain.setCityId(dto.getCityId());
		domain.setDescription(dto.getDescription());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	@Transactional
	public AgencyResult searchForAnyProperty(String key) {
		final List<AgencyDTO> agencies = new ArrayList<>();
		for (AgencyDomain domain : agencyDao.getByAny(key)) {
			final AgencyDTO dto = convertDomainToDto(domain);
			agencies.add(dto);
		}

		final AgencyResult agencyResult = new AgencyResult();
		agencyResult.setAgencys(agencies);
		return agencyResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return agencyDao.count(key);
	}
}
