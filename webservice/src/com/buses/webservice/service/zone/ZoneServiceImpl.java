package com.buses.webservice.service.zone;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.zone.IZoneDao;
import com.buses.webservice.dao.zone.ZoneDaoImpl;
import com.buses.webservice.domain.zone.ZoneDomain;
import com.buses.webservice.dto.zone.ZoneDTO;
import com.buses.webservice.dto.zone.ZoneResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class ZoneServiceImpl extends
		BaseServiceImpl<ZoneDTO, ZoneDomain, ZoneDaoImpl, ZoneResult> implements
		IZoneService {

	@Autowired
	private IZoneDao zoneDao;

	@Override
	@Transactional
	public ZoneDTO save(ZoneDTO dto) {
		final ZoneDomain domain = convertDtoToDomain(dto);
		final ZoneDomain zoneDomain = zoneDao.save(domain);
		return convertDomainToDto(zoneDomain);
	}

	@Override
	@Transactional
	public ZoneDTO getById(Integer id) {
		final ZoneDomain domain = zoneDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return zoneDao.delete(id);
	}

	@Override
	@Transactional
	public ZoneResult getAll() {
		final List<ZoneDTO> zones = new ArrayList<>();
		for (ZoneDomain domain : zoneDao.findAll()) {
			final ZoneDTO dto = convertDomainToDto(domain);
			zones.add(dto);
		}

		final ZoneResult zoneResult = new ZoneResult();
		zoneResult.setZones(zones);
		return zoneResult;
	}

	@Override
	protected ZoneDTO convertDomainToDto(ZoneDomain domain) {
		final ZoneDTO dto = new ZoneDTO();
		dto.setId(domain.getId());
		dto.setName(domain.getName());
		dto.setComment(domain.getComment());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected ZoneDomain convertDtoToDomain(ZoneDTO dto) {
		final ZoneDomain domain = new ZoneDomain();
		domain.setId(dto.getId());
		domain.setName(dto.getName());
		domain.setComment(dto.getComment());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	@Transactional
	public ZoneResult searchForAnyProperty(String key) {
		final List<ZoneDTO> zones = new ArrayList<>();
		for (ZoneDomain domain : zoneDao.getByAny(key)) {
			final ZoneDTO dto = convertDomainToDto(domain);
			zones.add(dto);
		}

		final ZoneResult zoneResult = new ZoneResult();
		zoneResult.setZones(zones);
		return zoneResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return zoneDao.count(key);
	}
}
