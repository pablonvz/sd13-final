package com.buses.webservice.service.zone;

import com.buses.webservice.dao.zone.ZoneDaoImpl;
import com.buses.webservice.domain.zone.ZoneDomain;
import com.buses.webservice.dto.zone.ZoneDTO;
import com.buses.webservice.dto.zone.ZoneResult;
import com.buses.webservice.service.base.IBaseService;

public interface IZoneService extends
		IBaseService<ZoneDTO, ZoneDomain, ZoneDaoImpl, ZoneResult> {

}
