package com.buses.webservice.service.itinerary;

import com.buses.webservice.dao.itinerary.ItineraryDaoImpl;
import com.buses.webservice.domain.itinerary.ItineraryDomain;
import com.buses.webservice.dto.itinerary.ItineraryDTO;
import com.buses.webservice.dto.itinerary.ItineraryResult;
import com.buses.webservice.service.base.IBaseService;

public interface IItineraryService
		extends
		IBaseService<ItineraryDTO, ItineraryDomain, ItineraryDaoImpl, ItineraryResult> {

}
