package com.buses.webservice.service.itinerary;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.itinerary.IItineraryDao;
import com.buses.webservice.dao.itinerary.ItineraryDaoImpl;
import com.buses.webservice.domain.itinerary.ItineraryDomain;
import com.buses.webservice.dto.itinerary.ItineraryDTO;
import com.buses.webservice.dto.itinerary.ItineraryResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class ItineraryServiceImpl
		extends
		BaseServiceImpl<ItineraryDTO, ItineraryDomain, ItineraryDaoImpl, ItineraryResult>
		implements IItineraryService {

	@Autowired
	private IItineraryDao itineraryDao;

	@Override
	@Transactional
	public ItineraryDTO save(ItineraryDTO dto) {
		final ItineraryDomain domain = convertDtoToDomain(dto);
		final ItineraryDomain itineraryDomain = itineraryDao.save(domain);
		return convertDomainToDto(itineraryDomain);
	}

	@Override
	@Transactional
	public ItineraryDTO getById(Integer id) {
		final ItineraryDomain domain = itineraryDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return itineraryDao.delete(id);
	}

	@Override
	@Transactional
	public ItineraryResult getAll() {
		final List<ItineraryDTO> itineraries = new ArrayList<>();
		final List<ItineraryDomain> r = itineraryDao.findAll();
		for (ItineraryDomain domain : r) {
			final ItineraryDTO dto = convertDomainToDto(domain);
			itineraries.add(dto);
		}

		final ItineraryResult itineraryResult = new ItineraryResult();
		itineraryResult.setItinerarys(itineraries);
		return itineraryResult;
	}

	@Override
	protected ItineraryDTO convertDomainToDto(ItineraryDomain domain) {
		final ItineraryDTO dto = new ItineraryDTO();
		dto.setId(domain.getId());
		dto.setOrigin(domain.getOrigin());
		dto.setDestination(domain.getDestination());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected ItineraryDomain convertDtoToDomain(ItineraryDTO dto) {
		final ItineraryDomain domain = new ItineraryDomain();
		domain.setId(dto.getId());
		domain.setOrigin(dto.getOrigin());
		domain.setDestination(dto.getDestination());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	@Transactional
	public ItineraryResult searchForAnyProperty(String key) {
		final List<ItineraryDTO> itineraries = new ArrayList<>();
		for (ItineraryDomain domain : itineraryDao.getByAny(key)) {
			final ItineraryDTO dto = convertDomainToDto(domain);
			itineraries.add(dto);
		}

		final ItineraryResult itineraryResult = new ItineraryResult();
		itineraryResult.setItinerarys(itineraries);
		return itineraryResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return itineraryDao.count(key);
	}

}
