package com.buses.webservice.service.base;

import java.util.HashMap;
import java.util.Map;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.base.BaseDomain;
import com.buses.webservice.dto.base.BaseDTO;
import com.buses.webservice.dto.base.BaseResult;

public abstract class BaseServiceImpl<DTO extends BaseDTO, DOMAIN extends BaseDomain, DAO extends BaseDaoImpl<DOMAIN>, RESULT extends BaseResult<DTO>>
		implements IBaseService<DTO, DOMAIN, DAO, RESULT> {
	// private final DAO _dao;
	//
	// public BaseServiceImpl(DAO dao) {
	// _dao = dao;
	// }
	//
	// protected DAO getDao() {
	// return _dao;
	// }

	protected abstract DTO convertDomainToDto(DOMAIN domain);

	protected abstract DOMAIN convertDtoToDomain(DTO dto);

	protected Map<String, String> getQueryMap(String query) {
		if (query.indexOf("=") < 0)
			return null;
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();
		for (String param : params) {
			String name = param.split("=")[0];
			String value = param.split("=")[1];
			map.put(name, value);
		}
		return map;
	}
}
