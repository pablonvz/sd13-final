package com.buses.webservice.service.base;

import com.buses.webservice.dao.base.BaseDaoImpl;
import com.buses.webservice.domain.base.BaseDomain;
import com.buses.webservice.dto.base.BaseDTO;
import com.buses.webservice.dto.base.BaseResult;

public interface IBaseService<DTO extends BaseDTO, DOMAIN extends BaseDomain, DAO extends BaseDaoImpl<DOMAIN>, R extends BaseResult<DTO>> {
	public DTO save(DTO dto);

	public DTO getById(Integer id);

	public R getAll();

	public boolean delete(Integer id);

	public R searchForAnyProperty(String keyWord);

	public Integer count(String key);
}
