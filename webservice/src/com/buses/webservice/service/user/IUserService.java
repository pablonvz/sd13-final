package com.buses.webservice.service.user;

import com.buses.webservice.dao.user.UserDaoImpl;
import com.buses.webservice.domain.user.UserDomain;
import com.buses.webservice.dto.user.UserDTO;
import com.buses.webservice.dto.user.UserResult;
import com.buses.webservice.service.base.IBaseService;

public interface IUserService extends
		IBaseService<UserDTO, UserDomain, UserDaoImpl, UserResult> {

	UserResult getByPersonId(Integer personId);

}
