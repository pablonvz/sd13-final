package com.buses.webservice.service.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.user.IUserDao;
import com.buses.webservice.dao.user.UserDaoImpl;
import com.buses.webservice.domain.user.UserDomain;
import com.buses.webservice.dto.user.UserDTO;
import com.buses.webservice.dto.user.UserResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class UserServiceImpl extends
		BaseServiceImpl<UserDTO, UserDomain, UserDaoImpl, UserResult> implements
		IUserService {

	@Autowired
	private IUserDao userDao;

	@Override
	@Transactional
	public UserDTO save(UserDTO dto) {
		final UserDomain domain = convertDtoToDomain(dto);
		final UserDomain userDomain = userDao.save(domain);
		return convertDomainToDto(userDomain);
	}

	@Override
	@Transactional
	public UserDTO getById(Integer id) {
		final UserDomain domain = userDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return userDao.delete(id);
	}

	@Override
	@Transactional
	public UserResult getAll() {
		final List<UserDTO> users = new ArrayList<>();
		for (UserDomain domain : userDao.findAll()) {
			final UserDTO dto = convertDomainToDto(domain);
			users.add(dto);
		}

		final UserResult userResult = new UserResult();
		userResult.setUsers(users);
		return userResult;
	}

	@Override
	protected UserDTO convertDomainToDto(UserDomain domain) {
		final UserDTO dto = new UserDTO();
		dto.setId(domain.getId());
		dto.setPersonId(domain.getPersonId());
		dto.setAgencyId(domain.getAgencyId());
		dto.setPasswordHash(domain.getPasswordHash());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected UserDomain convertDtoToDomain(UserDTO dto) {
		final UserDomain domain = new UserDomain();
		domain.setId(dto.getId());
		domain.setPersonId(dto.getPersonId());
		domain.setAgencyId(dto.getAgencyId());
		domain.setPasswordHash(dto.getPasswordHash());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	public UserResult getByPersonId(Integer personId) {
		final List<UserDTO> users = new ArrayList<>();
		for (UserDomain domain : userDao.getByPersonId(personId)) {
			final UserDTO dto = convertDomainToDto(domain);
			users.add(dto);
		}

		final UserResult userResult = new UserResult();
		userResult.setUsers(users);
		return userResult;
	}

	@Override
	@Transactional
	public UserResult searchForAnyProperty(String key) {
		final List<UserDTO> users = new ArrayList<>();
		for (UserDomain domain : userDao.getByAny(key)) {
			final UserDTO dto = convertDomainToDto(domain);
			users.add(dto);
		}

		final UserResult userResult = new UserResult();
		userResult.setUsers(users);
		return userResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return userDao.count(key);
	}
}
