package com.buses.webservice.service.ticket;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.ticket.ITicketDao;
import com.buses.webservice.dao.ticket.TicketDaoImpl;
import com.buses.webservice.domain.ticket.TicketDomain;
import com.buses.webservice.dto.ticket.TicketDTO;
import com.buses.webservice.dto.ticket.TicketResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class TicketServiceImpl extends
		BaseServiceImpl<TicketDTO, TicketDomain, TicketDaoImpl, TicketResult>
		implements ITicketService {

	@Autowired
	private ITicketDao ticketDao;

	@Override
	@Transactional
	public TicketDTO save(TicketDTO dto) {
		final TicketDomain domain = convertDtoToDomain(dto);
		final TicketDomain ticketDomain = ticketDao.save(domain);
		return convertDomainToDto(ticketDomain);
	}

	@Override
	@Transactional
	public TicketDTO getById(Integer id) {
		final TicketDomain domain = ticketDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return ticketDao.delete(id);
	}

	@Override
	@Transactional
	public TicketResult getAll() {
		final List<TicketDTO> tickets = new ArrayList<>();
		for (TicketDomain domain : ticketDao.findAll()) {
			final TicketDTO dto = convertDomainToDto(domain);
			tickets.add(dto);
		}

		final TicketResult ticketResult = new TicketResult();
		ticketResult.setTickets(tickets);
		return ticketResult;
	}

	@Override
	protected TicketDTO convertDomainToDto(TicketDomain domain) {
		final TicketDTO dto = new TicketDTO();
		dto.setId(domain.getId());
		dto.setClientId(domain.getClientId());

		dto.setJourneyId(domain.getJourneyId());
		dto.setSeat(domain.getSeat());
		dto.setStatus(domain.getStatus());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected TicketDomain convertDtoToDomain(TicketDTO dto) {
		final TicketDomain domain = new TicketDomain();
		domain.setId(dto.getId());
		domain.setClientId(dto.getClientId());

		domain.setJourneyId(dto.getJourneyId());
		domain.setSeat(dto.getSeat());
		domain.setStatus(dto.getStatus());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	@Transactional
	public TicketResult searchForAnyProperty(String key) {
		final List<TicketDTO> tickets = new ArrayList<>();
		for (TicketDomain domain : ticketDao.getByAny(key)) {
			final TicketDTO dto = convertDomainToDto(domain);
			tickets.add(dto);
		}

		final TicketResult ticketResult = new TicketResult();
		ticketResult.setTickets(tickets);
		return ticketResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return ticketDao.count(key);
	}

}
