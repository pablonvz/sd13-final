package com.buses.webservice.service.ticket;

import com.buses.webservice.dao.ticket.TicketDaoImpl;
import com.buses.webservice.domain.ticket.TicketDomain;
import com.buses.webservice.dto.ticket.TicketDTO;
import com.buses.webservice.dto.ticket.TicketResult;
import com.buses.webservice.service.base.IBaseService;

public interface ITicketService extends
		IBaseService<TicketDTO, TicketDomain, TicketDaoImpl, TicketResult> {

}
