package com.buses.webservice.service.country;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.country.CountryDaoImpl;
import com.buses.webservice.dao.country.ICountryDao;
import com.buses.webservice.domain.country.CountryDomain;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.country.CountryResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class CountryServiceImpl
		extends
		BaseServiceImpl<CountryDTO, CountryDomain, CountryDaoImpl, CountryResult>
		implements ICountryService {

	@Autowired
	private ICountryDao countryDao;

	@Override
	@Transactional
	public CountryDTO save(CountryDTO dto) {
		final CountryDomain domain = convertDtoToDomain(dto);
		final CountryDomain countryDomain = countryDao.save(domain);
		return convertDomainToDto(countryDomain);
	}

	@Override
	@Transactional
	public CountryDTO getById(Integer id) {
		final CountryDomain domain = countryDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return countryDao.delete(id);
	}

	@Override
	@Transactional
	public CountryResult getAll() {
		final List<CountryDTO> countries = new ArrayList<>();
		for (CountryDomain domain : countryDao.findAll()) {
			final CountryDTO dto = convertDomainToDto(domain);
			countries.add(dto);
		}

		final CountryResult countryResult = new CountryResult();
		countryResult.setCountrys(countries);
		return countryResult;
	}

	@Override
	protected CountryDTO convertDomainToDto(CountryDomain domain) {
		final CountryDTO dto = new CountryDTO();
		dto.setId(domain.getId());
		dto.setCode(domain.getCode());
		dto.setName(domain.getName());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected CountryDomain convertDtoToDomain(CountryDTO dto) {
		final CountryDomain domain = new CountryDomain();
		domain.setId(dto.getId());
		domain.setCode(dto.getCode());
		domain.setName(dto.getName());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	@Transactional
	public CountryResult searchForAnyProperty(String key) {
		final List<CountryDTO> countries = new ArrayList<>();
		for (CountryDomain domain : countryDao.getByAny(key)) {
			final CountryDTO dto = convertDomainToDto(domain);
			countries.add(dto);
		}

		final CountryResult countryResult = new CountryResult();
		countryResult.setCountrys(countries);
		return countryResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return countryDao.count(key);
	}
}
