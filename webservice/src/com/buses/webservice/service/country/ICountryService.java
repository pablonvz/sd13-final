package com.buses.webservice.service.country;

import com.buses.webservice.dao.country.CountryDaoImpl;
import com.buses.webservice.domain.country.CountryDomain;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.country.CountryResult;
import com.buses.webservice.service.base.IBaseService;

public interface ICountryService extends
		IBaseService<CountryDTO, CountryDomain, CountryDaoImpl, CountryResult> {

}
