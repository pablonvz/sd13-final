package com.buses.webservice.service.state;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.state.IStateDao;
import com.buses.webservice.dao.state.StateDaoImpl;
import com.buses.webservice.domain.state.StateDomain;
import com.buses.webservice.dto.state.StateDTO;
import com.buses.webservice.dto.state.StateResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class StateServiceImpl extends
		BaseServiceImpl<StateDTO, StateDomain, StateDaoImpl, StateResult>
		implements IStateService {

	@Autowired
	private IStateDao stateDao;

	@Override
	@Transactional
	public StateDTO save(StateDTO dto) {
		final StateDomain domain = convertDtoToDomain(dto);
		final StateDomain stateDomain = stateDao.save(domain);
		return convertDomainToDto(stateDomain);
	}

	@Override
	@Transactional
	public StateDTO getById(Integer id) {
		final StateDomain domain = stateDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return stateDao.delete(id);
	}

	@Override
	@Transactional
	public StateResult getAll() {
		final List<StateDTO> states = new ArrayList<>();
		for (StateDomain domain : stateDao.findAll()) {
			final StateDTO dto = convertDomainToDto(domain);
			states.add(dto);
		}

		final StateResult stateResult = new StateResult();
		stateResult.setStates(states);
		return stateResult;
	}

	@Override
	protected StateDTO convertDomainToDto(StateDomain domain) {
		final StateDTO dto = new StateDTO();
		dto.setId(domain.getId());
		dto.setCountryId(domain.getCountryId());
		dto.setName(domain.getName());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected StateDomain convertDtoToDomain(StateDTO dto) {
		final StateDomain domain = new StateDomain();
		domain.setId(dto.getId());
		domain.setCountryId(dto.getCountryId());
		domain.setName(dto.getName());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	@Transactional
	public StateResult searchForAnyProperty(String key) {
		final List<StateDTO> states = new ArrayList<>();
		for (StateDomain domain : stateDao.getByAny(key)) {
			final StateDTO dto = convertDomainToDto(domain);
			states.add(dto);
		}

		final StateResult stateResult = new StateResult();
		stateResult.setStates(states);
		return stateResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return stateDao.count(key);
	}
}
