package com.buses.webservice.service.state;

import com.buses.webservice.dao.state.StateDaoImpl;
import com.buses.webservice.domain.state.StateDomain;
import com.buses.webservice.dto.state.StateDTO;
import com.buses.webservice.dto.state.StateResult;
import com.buses.webservice.service.base.IBaseService;

public interface IStateService extends
		IBaseService<StateDTO, StateDomain, StateDaoImpl, StateResult> {

}
