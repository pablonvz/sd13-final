package com.buses.webservice.service.client;

import com.buses.webservice.dao.client.ClientDaoImpl;
import com.buses.webservice.domain.client.ClientDomain;
import com.buses.webservice.dto.client.ClientDTO;
import com.buses.webservice.dto.client.ClientResult;
import com.buses.webservice.service.base.IBaseService;

public interface IClientService extends
		IBaseService<ClientDTO, ClientDomain, ClientDaoImpl, ClientResult> {

}
