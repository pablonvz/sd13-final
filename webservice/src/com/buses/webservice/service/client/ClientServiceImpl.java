package com.buses.webservice.service.client;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.client.ClientDaoImpl;
import com.buses.webservice.dao.client.IClientDao;
import com.buses.webservice.domain.client.ClientDomain;
import com.buses.webservice.dto.client.ClientDTO;
import com.buses.webservice.dto.client.ClientResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class ClientServiceImpl extends
		BaseServiceImpl<ClientDTO, ClientDomain, ClientDaoImpl, ClientResult>
		implements IClientService {

	@Autowired
	private IClientDao clientDao;

	@Override
	@Transactional
	public ClientDTO save(ClientDTO dto) {
		final ClientDomain domain = convertDtoToDomain(dto);
		final ClientDomain clientDomain = clientDao.save(domain);
		return convertDomainToDto(clientDomain);
	}

	@Override
	@Transactional
	public ClientDTO getById(Integer id) {
		final ClientDomain domain = clientDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return clientDao.delete(id);
	}

	@Override
	@Transactional
	public ClientResult getAll() {
		final List<ClientDTO> clients = new ArrayList<>();
		for (ClientDomain domain : clientDao.findAll()) {
			final ClientDTO dto = convertDomainToDto(domain);
			clients.add(dto);
		}

		final ClientResult clientResult = new ClientResult();
		clientResult.setClients(clients);
		return clientResult;
	}

	@Override
	protected ClientDTO convertDomainToDto(ClientDomain domain) {
		final ClientDTO dto = new ClientDTO();
		dto.setId(domain.getId());
		dto.setPersonId(domain.getPersonId());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected ClientDomain convertDtoToDomain(ClientDTO dto) {
		final ClientDomain domain = new ClientDomain();
		domain.setId(dto.getId());
		domain.setPersonId(dto.getPersonId());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	@Transactional
	public ClientResult searchForAnyProperty(String key) {
		final List<ClientDTO> clients = new ArrayList<>();
		for (ClientDomain domain : clientDao.getByAny(key)) {
			final ClientDTO dto = convertDomainToDto(domain);
			clients.add(dto);
		}

		final ClientResult clientResult = new ClientResult();
		clientResult.setClients(clients);
		return clientResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return clientDao.count(key);
	}
}
