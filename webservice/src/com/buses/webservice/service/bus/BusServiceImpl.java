package com.buses.webservice.service.bus;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.buses.webservice.dao.bus.BusDaoImpl;
import com.buses.webservice.dao.bus.IBusDao;
import com.buses.webservice.domain.bus.BusDomain;
import com.buses.webservice.dto.bus.BusDTO;
import com.buses.webservice.dto.bus.BusResult;
import com.buses.webservice.service.base.BaseServiceImpl;

@Service
public class BusServiceImpl extends
		BaseServiceImpl<BusDTO, BusDomain, BusDaoImpl, BusResult> implements
		IBusService {

	@Autowired
	private IBusDao busDao;

	@Override
	@Transactional
	public BusDTO save(BusDTO dto) {
		final BusDomain domain = convertDtoToDomain(dto);
		final BusDomain busDomain = busDao.save(domain);
		return convertDomainToDto(busDomain);
	}

	@Override
	@Transactional
	public BusDTO getById(Integer id) {
		final BusDomain domain = busDao.getById(id);
		return convertDomainToDto(domain);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		return busDao.delete(id);
	}

	@Override
	@Transactional
	public BusResult getAll() {
		final List<BusDTO> buses = new ArrayList<>();
		for (BusDomain domain : busDao.findAll()) {
			final BusDTO dto = convertDomainToDto(domain);
			buses.add(dto);
		}

		final BusResult busResult = new BusResult();
		busResult.setBuss(buses);
		return busResult;
	}

	@Override
	protected BusDTO convertDomainToDto(BusDomain domain) {
		final BusDTO dto = new BusDTO();
		dto.setId(domain.getId());
		dto.setNumber(domain.getNumber());
		dto.setCapacty(domain.getCapacty());
		dto.setModel(domain.getModel());
		dto.setFuel(domain.getFuel());
		dto.setBrand(domain.getBrand());
		dto.setYear(domain.getYear());
		dto.setMotor(domain.getMotor());
		dto.setDeletedAt(domain.getDeletedAt());
		return dto;
	}

	@Override
	protected BusDomain convertDtoToDomain(BusDTO dto) {
		final BusDomain domain = new BusDomain();
		domain.setId(dto.getId());
		domain.setNumber(dto.getNumber());
		domain.setCapacty(dto.getCapacty());
		domain.setModel(dto.getModel());
		domain.setFuel(dto.getFuel());
		domain.setBrand(dto.getBrand());
		domain.setYear(dto.getYear());
		domain.setMotor(dto.getMotor());
		domain.setDeletedAt(dto.getDeletedAt());
		return domain;
	}

	@Override
	@Transactional
	public BusResult searchForAnyProperty(String key) {
		final List<BusDTO> buses = new ArrayList<>();
		for (BusDomain domain : busDao.getByAny(key)) {
			final BusDTO dto = convertDomainToDto(domain);
			buses.add(dto);
		}

		final BusResult busResult = new BusResult();
		busResult.setBuss(buses);
		return busResult;
	}

	@Override
	@Transactional
	public Integer count(String key) {
		return busDao.count(key);
	}
}
