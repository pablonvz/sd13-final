package com.buses.webservice.service.bus;

import com.buses.webservice.dao.bus.BusDaoImpl;
import com.buses.webservice.domain.bus.BusDomain;
import com.buses.webservice.dto.bus.BusDTO;
import com.buses.webservice.dto.bus.BusResult;
import com.buses.webservice.service.base.IBaseService;

public interface IBusService extends
		IBaseService<BusDTO, BusDomain, BusDaoImpl, BusResult> {

}
