package com.buses.webservice.dto.country;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseDTO;

@XmlRootElement(name = "country")
public class CountryDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private String code;

	private String name;

	private Date deletedAt;

	@XmlElement
	public String getCode() {
		return code;
	}

	@XmlElement
	public String getName() {
		return name;
	}

	@XmlElement
	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setCode(String codeP) {
		code = codeP;
	}

	public void setName(String nameP) {
		name = nameP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
