package com.buses.webservice.dto.country;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseResult;

@XmlRootElement(name = "countryResult")
public class CountryResult extends BaseResult<CountryDTO> {

	private static final long serialVersionUID = 1L;

	@XmlElement
	public List<CountryDTO> getCountrys() {
		return getList();
	}

	public void setCountrys(List<CountryDTO> dtos) {
		super.setList(dtos);
	}
}
