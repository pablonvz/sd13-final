package com.buses.webservice.dto.city;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseResult;

@XmlRootElement(name = "cityResult")
public class CityResult extends BaseResult<CityDTO> {

	private static final long serialVersionUID = 1L;

	@XmlElement
	public List<CityDTO> getCitys() {
		return getList();
	}

	public void setCitys(List<CityDTO> dtos) {
		super.setList(dtos);
	}
}
