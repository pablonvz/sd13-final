package com.buses.webservice.dto.city;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseDTO;

@XmlRootElement(name = "city")
public class CityDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private Integer stateId;

	private String name;

	private Date deletedAt;

	@XmlElement
	public Integer getStateId() {
		return stateId;
	}

	@XmlElement
	public String getName() {
		return name;
	}

	@XmlElement
	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setStateId(Integer stateIdP) {
		stateId = stateIdP;
	}

	public void setName(String nameP) {
		name = nameP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
