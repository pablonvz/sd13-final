package com.buses.webservice.dto.ticket;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseDTO;

@XmlRootElement(name = "ticket")
public class TicketDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private Integer clientId;

	private Integer journeyId;

	private Integer seat;

	private String status;

	private Date deletedAt;

	@XmlElement
	public Integer getClientId() {
		return clientId;
	}

	@XmlElement
	public Integer getJourneyId() {
		return journeyId;
	}

	@XmlElement
	public Integer getSeat() {
		return seat;
	}

	@XmlElement
	public String getStatus() {
		return status;
	}

	@XmlElement
	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setClientId(Integer clientIdP) {
		clientId = clientIdP;
	}

	public void setJourneyId(Integer journeyIdP) {
		journeyId = journeyIdP;
	}

	public void setSeat(Integer seatP) {
		seat = seatP;
	}

	public void setStatus(String statusP) {
		status = statusP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
