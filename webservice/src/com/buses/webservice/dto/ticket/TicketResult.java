package com.buses.webservice.dto.ticket;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseResult;

@XmlRootElement(name = "ticketResult")
public class TicketResult extends BaseResult<TicketDTO> {

	private static final long serialVersionUID = 1L;

	@XmlElement
	public List<TicketDTO> getTickets() {
		return getList();
	}

	public void setTickets(List<TicketDTO> dtos) {
		super.setList(dtos);
	}
}
