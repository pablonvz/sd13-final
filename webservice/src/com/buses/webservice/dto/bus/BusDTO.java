package com.buses.webservice.dto.bus;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseDTO;

@XmlRootElement(name = "bus")
public class BusDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private Integer capacty;

	private Integer number;

	private String model;

	private String fuel;

	private String brand;

	private Date year;

	private String motor;

	private Date deletedAt;

	@XmlElement
	public Integer getNumber() {
		return number;
	}

	@XmlElement
	public Integer getCapacty() {
		return capacty;
	}

	@XmlElement
	public String getModel() {
		return model;
	}

	@XmlElement
	public String getFuel() {
		return fuel;
	}

	@XmlElement
	public String getBrand() {
		return brand;
	}

	@XmlElement
	public Date getYear() {
		return year;
	}

	@XmlElement
	public String getMotor() {
		return motor;
	}

	@XmlElement
	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setNumber(Integer numberP) {
		number = numberP;
	}

	public void setCapacty(Integer capactyP) {
		capacty = capactyP;
	}

	public void setModel(String modelP) {
		model = modelP;
	}

	public void setFuel(String fuelP) {
		fuel = fuelP;
	}

	public void setBrand(String brandP) {
		brand = brandP;
	}

	public void setYear(Date yearP) {
		year = yearP;
	}

	public void setMotor(String motorP) {
		motor = motorP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
