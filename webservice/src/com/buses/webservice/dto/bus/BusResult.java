package com.buses.webservice.dto.bus;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseResult;

@XmlRootElement(name = "busResult")
public class BusResult extends BaseResult<BusDTO> {

	private static final long serialVersionUID = 1L;

	@XmlElement
	public List<BusDTO> getBuss() {
		return getList();
	}

	public void setBuss(List<BusDTO> dtos) {
		super.setList(dtos);
	}
}
