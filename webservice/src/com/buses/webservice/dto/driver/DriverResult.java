package com.buses.webservice.dto.driver;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseResult;

@XmlRootElement(name = "driverResult")
public class DriverResult extends BaseResult<DriverDTO> {

	private static final long serialVersionUID = 1L;

	@XmlElement
	public List<DriverDTO> getDrivers() {
		return getList();
	}

	public void setDrivers(List<DriverDTO> dtos) {
		super.setList(dtos);
	}
}
