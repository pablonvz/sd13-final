package com.buses.webservice.dto.driver;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseDTO;

@XmlRootElement(name = "driver")
public class DriverDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private Integer personId;

	private Date deletedAt;

	private Integer agencyId;

	@XmlElement
	public Integer getPersonId() {
		return personId;
	}

	@XmlElement
	public Date getDeletedAt() {
		return deletedAt;
	}

	@XmlElement
	public Integer getAgencyId() {
		return agencyId;
	}

	public void setPersonId(Integer personIdP) {
		personId = personIdP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}

	public void setAgencyId(Integer agencyIdP) {
		agencyId = agencyIdP;
	}
}
