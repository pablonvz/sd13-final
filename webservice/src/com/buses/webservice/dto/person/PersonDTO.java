package com.buses.webservice.dto.person;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseDTO;

@XmlRootElement(name = "person")
public class PersonDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private String firstName;

	private String lastName;

	private Date birthDate;

	private String sex;

	private String document;

	private String email;

	private Date deletedAt;

	@XmlElement
	public String getFirstName() {
		return firstName;
	}

	@XmlElement
	public String getLastName() {
		return lastName;
	}

	@XmlElement
	public Date getBirthDate() {
		return birthDate;
	}

	@XmlElement
	public String getSex() {
		return sex;
	}

	@XmlElement
	public String getDocument() {
		return document;
	}

	@XmlElement
	public String getEmail() {
		return email;
	}

	@XmlElement
	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setFirstName(String firstNameP) {
		firstName = firstNameP;
	}

	public void setLastName(String lastNameP) {
		lastName = lastNameP;
	}

	public void setBirthDate(Date birthDateP) {
		birthDate = birthDateP;
	}

	public void setSex(String sexP) {
		sex = sexP;
	}

	public void setDocument(String documentP) {
		document = documentP;
	}

	public void setEmail(String emailP) {
		email = emailP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
