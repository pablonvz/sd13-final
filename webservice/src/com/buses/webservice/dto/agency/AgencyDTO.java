package com.buses.webservice.dto.agency;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseDTO;

@XmlRootElement(name = "agency")
public class AgencyDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private Integer cityId;

	private String description;

	private Date deletedAt;

	@XmlElement
	public Integer getCityId() {
		return cityId;
	}

	@XmlElement
	public String getDescription() {
		return description;
	}

	@XmlElement
	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setCityId(Integer cityIdP) {
		cityId = cityIdP;
	}

	public void setDescription(String descriptionP) {
		description = descriptionP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
