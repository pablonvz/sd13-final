package com.buses.webservice.dto.agency;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseResult;

@XmlRootElement(name = "agencyResult")
public class AgencyResult extends BaseResult<AgencyDTO> {

	private static final long serialVersionUID = 1L;

	@XmlElement
	public List<AgencyDTO> getAgencys() {
		return getList();
	}

	public void setAgencys(List<AgencyDTO> dtos) {
		super.setList(dtos);
	}
}
