package com.buses.webservice.dto.zone;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseResult;

@XmlRootElement(name = "zoneResult")
public class ZoneResult extends BaseResult<ZoneDTO> {

	private static final long serialVersionUID = 1L;

	@XmlElement
	public List<ZoneDTO> getZones() {
		return getList();
	}

	public void setZones(List<ZoneDTO> dtos) {
		super.setList(dtos);
	}
}
