package com.buses.webservice.dto.zone;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseDTO;

@XmlRootElement(name = "zone")
public class ZoneDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private String name;

	private String comment;

	private Date deletedAt;

	@XmlElement
	public String getName() {
		return name;
	}

	@XmlElement
	public String getComment() {
		return comment;
	}

	@XmlElement
	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setName(String nameP) {
		name = nameP;
	}

	public void setComment(String commentP) {
		comment = commentP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
