package com.buses.webservice.dto.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseResult<DTO extends BaseDTO> implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<DTO> _dtos = new ArrayList<DTO>();

	protected List<DTO> getList() {
		return _dtos;
	}

	protected void setList(List<DTO> dtos) {
		_dtos = dtos == null ? new ArrayList<DTO>() : dtos;
	}

	public Integer getTotal() {
		return null == _dtos ? 0 : _dtos.size();
	}
}
