package com.buses.webservice.dto.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "counts")
public class CountResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Integer> _counts = new ArrayList<Integer>();

	@XmlElement
	public List<Integer> getCounts() {
		return _counts;
	}

	public void setCounts(List<Integer> counts) {
		_counts = counts == null ? new ArrayList<Integer>() : counts;
	}

	public Integer getTotal() {
		return null == _counts ? 0 : _counts.size();
	}
}
