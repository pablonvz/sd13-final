package com.buses.webservice.dto.journey;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseResult;

@XmlRootElement(name = "journeyResult")
public class JourneyResult extends BaseResult<JourneyDTO> {

	private static final long serialVersionUID = 1L;

	@XmlElement
	public List<JourneyDTO> getJourneys() {
		return getList();
	}

	public void setJourneys(List<JourneyDTO> dtos) {
		super.setList(dtos);
	}
}
