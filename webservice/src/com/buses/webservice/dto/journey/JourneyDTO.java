package com.buses.webservice.dto.journey;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseDTO;

@XmlRootElement(name = "journey")
public class JourneyDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private Integer itineraryId;

	private Integer busId;

	private Integer driverId;

	private Integer price;

	private Double initialKm;

	private Double endKm;

	private String incident;

	private Date deletedAt;

	@XmlElement
	public Integer getItineraryId() {
		return itineraryId;
	}

	@XmlElement
	public Integer getBusId() {
		return busId;
	}

	@XmlElement
	public Integer getDriverId() {
		return driverId;
	}

	@XmlElement
	public Integer getPrice() {
		return price;
	}

	@XmlElement
	public Double getInitialKm() {
		return initialKm;
	}

	@XmlElement
	public Double getEndKm() {
		return endKm;
	}

	@XmlElement
	public String getIncident() {
		return incident;
	}

	@XmlElement
	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setItineraryId(Integer itineraryIdP) {
		itineraryId = itineraryIdP;
	}

	public void setBusId(Integer busIdP) {
		busId = busIdP;
	}

	public void setDriverId(Integer driverIdP) {
		driverId = driverIdP;
	}

	public void setPrice(Integer priceP) {
		price = priceP;
	}

	public void setInitialKm(Double initialKmP) {
		initialKm = initialKmP;
	}

	public void setEndKm(Double endKmP) {
		endKm = endKmP;
	}

	public void setIncident(String incidentP) {
		incident = incidentP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}

	private Date date;

	@XmlElement
	public Date getDate() {
		return date;
	}

	public void setDate(Date dateP) {
		date = dateP;
	}
}
