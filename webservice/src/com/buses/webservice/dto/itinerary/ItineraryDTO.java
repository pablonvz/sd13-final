package com.buses.webservice.dto.itinerary;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseDTO;

@XmlRootElement(name = "itinerary")
public class ItineraryDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private Integer origin;

	private Integer destination;

	private Date deletedAt;

	@XmlElement
	public Integer getOrigin() {
		return origin;
	}

	@XmlElement
	public Integer getDestination() {
		return destination;
	}

	@XmlElement
	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setOrigin(Integer originP) {
		origin = originP;
	}

	public void setDestination(Integer destinationP) {
		destination = destinationP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
