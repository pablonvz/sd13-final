package com.buses.webservice.dto.itinerary;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseResult;

@XmlRootElement(name = "itineraryResult")
public class ItineraryResult extends BaseResult<ItineraryDTO> {

	private static final long serialVersionUID = 1L;

	@XmlElement
	public List<ItineraryDTO> getItinerarys() {
		return getList();
	}

	public void setItinerarys(List<ItineraryDTO> dtos) {
		super.setList(dtos);
	}
}
