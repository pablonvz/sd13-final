package com.buses.webservice.dto.client;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseDTO;

@XmlRootElement(name = "client")
public class ClientDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private Integer personId;

	private Date deletedAt;

	@XmlElement
	public Integer getPersonId() {
		return personId;
	}

	@XmlElement
	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setPersonId(Integer personIdP) {
		personId = personIdP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}

}
