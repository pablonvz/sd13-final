package com.buses.webservice.dto.state;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.buses.webservice.dto.base.BaseDTO;

@XmlRootElement(name = "state")
public class StateDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private Integer countryId;

	private String name;

	private Date deletedAt;

	@XmlElement
	public Integer getCountryId() {
		return countryId;
	}

	@XmlElement
	public String getName() {
		return name;
	}

	@XmlElement
	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setCountryId(Integer countryIdP) {
		countryId = countryIdP;
	}

	public void setName(String nameP) {
		name = nameP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
