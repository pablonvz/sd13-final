package com.buses.webservice.domain.city;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.buses.webservice.domain.base.BaseDomain;

@Entity
@Table(name = "city")
public class CityDomain extends BaseDomain {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "stateId")
	private Integer stateId;
	@Column(name = "name")
	private String name;
	@Column(name = "deletedAt")
	private Date deletedAt;

	public Integer getStateId() {
		return stateId;
	}

	public String getName() {
		return name;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setStateId(Integer stateIdP) {
		stateId = stateIdP;
	}

	public void setName(String nameP) {
		name = nameP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
