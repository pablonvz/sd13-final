package com.buses.webservice.domain.person;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.buses.webservice.domain.base.BaseDomain;

@Entity
@Table(name = "person")
public class PersonDomain extends BaseDomain {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "firstName")
	private String firstName;
	@Column(name = "lastName")
	private String lastName;
	@Column(name = "birthDate")
	private Date birthDate;
	@Column(name = "sex")
	private String sex;
	@Column(name = "document")
	private String document;
	@Column(name = "email")
	private String email;
	@Column(name = "deletedAt")
	private Date deletedAt;

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public String getSex() {
		return sex;
	}

	public String getDocument() {
		return document;
	}

	public String getEmail() {
		return email;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setFirstName(String firstNameP) {
		firstName = firstNameP;
	}

	public void setLastName(String lastNameP) {
		lastName = lastNameP;
	}

	public void setBirthDate(Date birthDateP) {
		birthDate = birthDateP;
	}

	public void setSex(String sexP) {
		sex = sexP;
	}

	public void setDocument(String documentP) {
		document = documentP;
	}

	public void setEmail(String emailP) {
		email = emailP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
