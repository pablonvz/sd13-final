package com.buses.webservice.domain.itinerary;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.buses.webservice.domain.base.BaseDomain;

@Entity
@Table(name = "itinerary")
public class ItineraryDomain extends BaseDomain {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "origin")
	private Integer origin;
	@Column(name = "destination")
	private Integer destination;
	@Column(name = "deletedAt")
	private Date deletedAt;

	public Integer getOrigin() {
		return origin;
	}

	public Integer getDestination() {
		return destination;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setOrigin(Integer originP) {
		origin = originP;
	}

	public void setDestination(Integer destinationP) {
		destination = destinationP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
