package com.buses.webservice.domain.journey;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.buses.webservice.domain.base.BaseDomain;

@Entity
@Table(name = "journey")
public class JourneyDomain extends BaseDomain {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "itineraryId")
	private Integer itineraryId;
	@Column(name = "busId")
	private Integer busId;
	@Column(name = "driverId")
	private Integer driverId;
	@Column(name = "price")
	private Integer price;
	@Column(name = "initialKm")
	private Double initialKm;
	@Column(name = "endKm")
	private Double endKm;
	@Column(name = "incident")
	private String incident;
	@Column(name = "deletedAt")
	private Date deletedAt;

	public Integer getItineraryId() {
		return itineraryId;
	}

	public Integer getBusId() {
		return busId;
	}

	public Integer getDriverId() {
		return driverId;
	}

	public Integer getPrice() {
		return price;
	}

	public Double getInitialKm() {
		return initialKm;
	}

	public Double getEndKm() {
		return endKm;
	}

	public String getIncident() {
		return incident;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setItineraryId(Integer itineraryIdP) {
		itineraryId = itineraryIdP;
	}

	public void setBusId(Integer busIdP) {
		busId = busIdP;
	}

	public void setDriverId(Integer driverIdP) {
		driverId = driverIdP;
	}

	public void setPrice(Integer priceP) {
		price = priceP;
	}

	public void setInitialKm(Double initialKmP) {
		initialKm = initialKmP;
	}

	public void setEndKm(Double endKmP) {
		endKm = endKmP;
	}

	public void setIncident(String incidentP) {
		incident = incidentP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}

	@Column(name = "date")
	private Date date;

	public Date getDate() {
		return date;
	}

	public void setDate(Date dateP) {
		date = dateP;
	}
}
