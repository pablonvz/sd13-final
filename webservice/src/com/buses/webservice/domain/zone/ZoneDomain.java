package com.buses.webservice.domain.zone;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.buses.webservice.domain.base.BaseDomain;

@Entity
@Table(name = "zone")
public class ZoneDomain extends BaseDomain {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name")
	private String name;
	@Column(name = "comment")
	private String comment;
	@Column(name = "deletedAt")
	private Date deletedAt;

	public String getName() {
		return name;
	}

	public String getComment() {
		return comment;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setName(String nameP) {
		name = nameP;
	}

	public void setComment(String commentP) {
		comment = commentP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
