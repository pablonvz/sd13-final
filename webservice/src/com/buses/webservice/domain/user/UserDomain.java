package com.buses.webservice.domain.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.buses.webservice.domain.base.BaseDomain;

@Entity
@Table(name = "user")
public class UserDomain extends BaseDomain {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "personId")
	private Integer personId;
	@Column(name = "deletedAt")
	private Date deletedAt;
	@Column(name = "agencyId")
	private Integer agencyId;

	public Integer getPersonId() {
		return personId;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public Integer getAgencyId() {
		return agencyId;
	}

	public void setPersonId(Integer personIdP) {
		personId = personIdP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}

	public void setAgencyId(Integer agencyIdP) {
		agencyId = agencyIdP;
	}

	@Column(name = "passwordHash")
	private String passwordHash;

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHashP) {
		passwordHash = passwordHashP;
	}
}
