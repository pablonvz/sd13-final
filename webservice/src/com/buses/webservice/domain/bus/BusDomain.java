package com.buses.webservice.domain.bus;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.buses.webservice.domain.base.BaseDomain;

@Entity
@Table(name = "bus")
public class BusDomain extends BaseDomain {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "capacty")
	private Integer capacty;
	@Column(name = "number")
	private Integer number;
	@Column(name = "model")
	private String model;
	@Column(name = "fuel")
	private String fuel;
	@Column(name = "brand")
	private String brand;
	@Column(name = "year")
	private Date year;
	@Column(name = "motor")
	private String motor;
	@Column(name = "deletedAt")
	private Date deletedAt;

	public Integer getNumber() {
		return number;
	}

	public Integer getCapacty() {
		return capacty;
	}

	public String getModel() {
		return model;
	}

	public String getFuel() {
		return fuel;
	}

	public String getBrand() {
		return brand;
	}

	public Date getYear() {
		return year;
	}

	public String getMotor() {
		return motor;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setNumber(Integer numberP) {
		number = numberP;
	}

	public void setCapacty(Integer capactyP) {
		capacty = capactyP;
	}

	public void setModel(String modelP) {
		model = modelP;
	}

	public void setFuel(String fuelP) {
		fuel = fuelP;
	}

	public void setBrand(String brandP) {
		brand = brandP;
	}

	public void setYear(Date yearP) {
		year = yearP;
	}

	public void setMotor(String motorP) {
		motor = motorP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
