package com.buses.webservice.domain.country;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.buses.webservice.domain.base.BaseDomain;

@Entity
@Table(name = "country")
public class CountryDomain extends BaseDomain {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "code")
	private String code;
	@Column(name = "name")
	private String name;
	@Column(name = "deletedAt")
	private Date deletedAt;

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setCode(String codeP) {
		code = codeP;
	}

	public void setName(String nameP) {
		name = nameP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
