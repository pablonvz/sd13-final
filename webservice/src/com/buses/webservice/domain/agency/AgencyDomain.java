package com.buses.webservice.domain.agency;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.buses.webservice.domain.base.BaseDomain;

@Entity
@Table(name = "agency")
public class AgencyDomain extends BaseDomain {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "cityId")
	private Integer cityId;
	@Column(name = "description")
	private String description;
	@Column(name = "deletedAt")
	private Date deletedAt;

	public Integer getCityId() {
		return cityId;
	}

	public String getDescription() {
		return description;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setCityId(Integer cityIdP) {
		cityId = cityIdP;
	}

	public void setDescription(String descriptionP) {
		description = descriptionP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
