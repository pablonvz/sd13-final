package com.buses.webservice.domain.ticket;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.buses.webservice.domain.base.BaseDomain;

@Entity
@Table(name = "ticket")
public class TicketDomain extends BaseDomain {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "clientId")
	private Integer clientId;
	@Column(name = "journeyId")
	private Integer journeyId;
	@Column(name = "seat")
	private Integer seat;
	@Column(name = "status")
	private String status;
	@Column(name = "deletedAt")
	private Date deletedAt;

	public Integer getClientId() {
		return clientId;
	}

	public Integer getJourneyId() {
		return journeyId;
	}

	public Integer getSeat() {
		return seat;
	}

	public String getStatus() {
		return status;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setClientId(Integer clientIdP) {
		clientId = clientIdP;
	}

	public void setJourneyId(Integer journeyIdP) {
		journeyId = journeyIdP;
	}

	public void setSeat(Integer seatP) {
		seat = seatP;
	}

	public void setStatus(String statusP) {
		status = statusP;
	}

	public void setDeletedAt(Date deletedAtP) {
		deletedAt = deletedAtP;
	}
}
