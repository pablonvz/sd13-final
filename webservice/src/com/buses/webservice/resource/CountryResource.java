package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.country.CountryResult;
import com.buses.webservice.service.country.ICountryService;

@Path("/country")
@Component
public class CountryResource {

	@Autowired
	private ICountryService countryService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public CountryDTO getById(@PathParam("id") Integer countryId) {
		return countryService.getById(countryId);
	}

	@GET
	@Produces("application/json")
	public CountryResult getAll() {
		return countryService.getAll();
	}

	@POST
	public CountryDTO save(CountryDTO country) {
		return countryService.save(country);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer countryId) {
		return countryService.delete(countryId) ? "true" : "false";
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public CountryResult search(@PathParam("key") String keyWord) {
		return countryService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return countryService.count(keyWord).toString();
	}
}
