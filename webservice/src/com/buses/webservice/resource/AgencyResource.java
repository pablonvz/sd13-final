package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.agency.AgencyDTO;
import com.buses.webservice.dto.agency.AgencyResult;
import com.buses.webservice.service.agency.IAgencyService;

@Path("/agency")
@Component
public class AgencyResource {

	@Autowired
	private IAgencyService agencyService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public AgencyDTO getById(@PathParam("id") Integer agencyId) {
		return agencyService.getById(agencyId);
	}

	@GET
	@Produces("application/json")
	public AgencyResult getAll() {
		return agencyService.getAll();
	}

	@POST
	public AgencyDTO save(AgencyDTO agency) {
		return agencyService.save(agency);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer agencyId) {
		return agencyService.delete(agencyId) ? "true" : "false";
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public AgencyResult search(@PathParam("key") String keyWord) {
		return agencyService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return agencyService.count(keyWord).toString();
	}
}
