package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.itinerary.ItineraryDTO;
import com.buses.webservice.dto.itinerary.ItineraryResult;
import com.buses.webservice.service.itinerary.IItineraryService;

@Path("/itinerary")
@Component
public class ItineraryResource {

	@Autowired
	private IItineraryService itineraryService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public ItineraryDTO getById(@PathParam("id") Integer itineraryId) {
		return itineraryService.getById(itineraryId);
	}

	@GET
	@Produces("application/json")
	public ItineraryResult getAll() {
		return itineraryService.getAll();
	}

	@POST
	public ItineraryDTO save(ItineraryDTO itinerary) {
		return itineraryService.save(itinerary);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer itineraryId) {
		return itineraryService.delete(itineraryId) ? "true" : "false";
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public ItineraryResult search(@PathParam("key") String keyWord) {
		return itineraryService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return itineraryService.count(keyWord).toString();
	}
}
