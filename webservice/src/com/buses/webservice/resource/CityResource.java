package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.base.CountResult;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.city.CityResult;
import com.buses.webservice.service.city.ICityService;

@Path("/city")
@Component
public class CityResource {

	@Autowired
	private ICityService cityService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public CityDTO getById(@PathParam("id") Integer cityId) {
		return cityService.getById(cityId);
	}

	@GET
	@Produces("application/json")
	public CityResult getAll() {
		return cityService.getAll();
	}

	@POST
	public CityDTO save(CityDTO city) {
		return cityService.save(city);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer cityId) {
		return cityService.delete(cityId) ? "true" : "false";
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public CityResult search(@PathParam("key") String keyWord) {
		return cityService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return cityService.count(keyWord).toString();
	}

	@GET
	@Path("most-popular/{arguments}")
	@Produces("application/json")
	public CityResult getMostPopular(@PathParam("arguments") String params) {
		return cityService.getMostPopular(params);
	}

	@GET
	@Path("most-popular-count/{arguments}")
	@Produces("application/json")
	public CountResult getMostPopularCount(@PathParam("arguments") String params) {
		CountResult cr = new CountResult();
		cr.setCounts(cityService.getMostPopularCount(params));
		return cr;
	}

	@GET
	@Path("with-more-tickets/{arguments}")
	@Produces("application/json")
	public CityDTO getWithMoreTickets(@PathParam("arguments") String params) {
		return cityService.getCityWithMoreTickets(params);
	}

	@GET
	@Path("with-more-tickets-count/{arguments}")
	@Produces("application/json")
	public String getWithMoreTicketsCount(@PathParam("arguments") String params) {
		return cityService.getCityWithMoreTicketsCount(params).toString();
	}
}
