package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.client.ClientDTO;
import com.buses.webservice.dto.client.ClientResult;
import com.buses.webservice.service.client.IClientService;

@Path("/client")
@Component
public class ClientResource {

	@Autowired
	private IClientService clientService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public ClientDTO getById(@PathParam("id") Integer clientId) {
		return clientService.getById(clientId);
	}

	@GET
	@Produces("application/json")
	public ClientResult getAll() {
		return clientService.getAll();
	}

	@POST
	public ClientDTO save(ClientDTO client) {
		return clientService.save(client);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer clientId) {
		return clientService.delete(clientId) ? "true" : "false";
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public ClientResult search(@PathParam("key") String keyWord) {
		return clientService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return clientService.count(keyWord).toString();
	}
}
