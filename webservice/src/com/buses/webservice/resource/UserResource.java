package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.user.UserDTO;
import com.buses.webservice.dto.user.UserResult;
import com.buses.webservice.service.user.IUserService;

@Path("/user")
@Component
public class UserResource {

	@Autowired
	private IUserService userService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public UserDTO getById(@PathParam("id") Integer userId) {
		return userService.getById(userId);
	}

	@GET
	@Produces("application/json")
	public UserResult getAll() {
		return userService.getAll();
	}

	@POST
	public UserDTO save(UserDTO user) {
		return userService.save(user);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer userId) {
		return userService.delete(userId) ? "true" : "false";
	}

	@GET
	@Path("/person/{id}")
	@Produces("application/json")
	public UserResult getByPersonId(@PathParam("id") Integer personId) {
		return userService.getByPersonId(personId);
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public UserResult search(@PathParam("key") String keyWord) {
		return userService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return userService.count(keyWord).toString();
	}

}
