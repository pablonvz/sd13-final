package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.driver.DriverDTO;
import com.buses.webservice.dto.driver.DriverResult;
import com.buses.webservice.service.driver.IDriverService;

@Path("/driver")
@Component
public class DriverResource {

	@Autowired
	private IDriverService driverService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public DriverDTO getById(@PathParam("id") Integer driverId) {
		return driverService.getById(driverId);
	}

	@GET
	@Produces("application/json")
	public DriverResult getAll() {
		return driverService.getAll();
	}

	@POST
	public DriverDTO save(DriverDTO driver) {
		return driverService.save(driver);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer driverId) {
		return driverService.delete(driverId) ? "true" : "false";
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public DriverResult search(@PathParam("key") String keyWord) {
		return driverService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return driverService.count(keyWord).toString();
	}
}
