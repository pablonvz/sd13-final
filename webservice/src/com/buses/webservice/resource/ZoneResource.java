package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.zone.ZoneDTO;
import com.buses.webservice.dto.zone.ZoneResult;
import com.buses.webservice.service.zone.IZoneService;

@Path("/zone")
@Component
public class ZoneResource {

	@Autowired
	private IZoneService zoneService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public ZoneDTO getById(@PathParam("id") Integer zoneId) {
		return zoneService.getById(zoneId);
	}

	@GET
	@Produces("application/json")
	public ZoneResult getAll() {
		return zoneService.getAll();
	}

	@POST
	public ZoneDTO save(ZoneDTO zone) {
		return zoneService.save(zone);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer zoneId) {
		return zoneService.delete(zoneId) ? "true" : "false";
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public ZoneResult search(@PathParam("key") String keyWord) {
		return zoneService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return zoneService.count(keyWord).toString();
	}
}
