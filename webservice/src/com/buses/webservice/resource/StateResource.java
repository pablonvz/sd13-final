package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.state.StateDTO;
import com.buses.webservice.dto.state.StateResult;
import com.buses.webservice.service.state.IStateService;

@Path("/state")
@Component
public class StateResource {

	@Autowired
	private IStateService stateService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public StateDTO getById(@PathParam("id") Integer stateId) {
		return stateService.getById(stateId);
	}

	@GET
	@Produces("application/json")
	public StateResult getAll() {
		return stateService.getAll();
	}

	@POST
	public StateDTO save(StateDTO state) {
		return stateService.save(state);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer stateId) {
		return stateService.delete(stateId) ? "true" : "false";
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public StateResult search(@PathParam("key") String keyWord) {
		return stateService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return stateService.count(keyWord).toString();
	}
}
