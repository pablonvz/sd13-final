package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.ticket.TicketDTO;
import com.buses.webservice.dto.ticket.TicketResult;
import com.buses.webservice.service.ticket.ITicketService;

@Path("/ticket")
@Component
public class TicketResource {

	@Autowired
	private ITicketService ticketService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public TicketDTO getById(@PathParam("id") Integer ticketId) {
		return ticketService.getById(ticketId);
	}

	@GET
	@Produces("application/json")
	public TicketResult getAll() {
		return ticketService.getAll();
	}

	@POST
	public TicketDTO save(TicketDTO ticket) {
		return ticketService.save(ticket);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer ticketId) {
		return ticketService.delete(ticketId) ? "true" : "false";
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public TicketResult search(@PathParam("key") String keyWord) {
		return ticketService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return ticketService.count(keyWord).toString();
	}
}
