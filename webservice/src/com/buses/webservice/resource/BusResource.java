package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.bus.BusDTO;
import com.buses.webservice.dto.bus.BusResult;
import com.buses.webservice.service.bus.IBusService;

@Path("/bus")
@Component
public class BusResource {

	@Autowired
	private IBusService busService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public BusDTO getById(@PathParam("id") Integer busId) {
		return busService.getById(busId);
	}

	@GET
	@Produces("application/json")
	public BusResult getAll() {
		return busService.getAll();
	}

	@POST
	public BusDTO save(BusDTO bus) {
		return busService.save(bus);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer busId) {
		return busService.delete(busId) ? "true" : "false";
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public BusResult search(@PathParam("key") String keyWord) {
		return busService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return busService.count(keyWord).toString();
	}
}
