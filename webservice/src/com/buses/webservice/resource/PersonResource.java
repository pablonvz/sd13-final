package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.person.PersonDTO;
import com.buses.webservice.dto.person.PersonResult;
import com.buses.webservice.service.person.IPersonService;

@Path("/person")
@Component
public class PersonResource {

	@Autowired
	private IPersonService personService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public PersonDTO getById(@PathParam("id") Integer personId) {
		return personService.getById(personId);
	}

	@GET
	@Produces("application/json")
	public PersonResult getAll() {
		return personService.getAll();
	}

	@POST
	public PersonDTO save(PersonDTO person) {
		return personService.save(person);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer personId) {
		return personService.delete(personId) ? "true" : "false";
	}

	@GET
	@Path("/email/{email}")
	@Produces("application/json")
	public PersonResult getById(@PathParam("email") String email) {
		return personService.getByEmail(email);
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public PersonResult search(@PathParam("key") String keyWord) {
		return personService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return personService.count(keyWord).toString();
	}
}
