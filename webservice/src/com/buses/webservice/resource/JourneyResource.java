package com.buses.webservice.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.buses.webservice.dto.journey.JourneyDTO;
import com.buses.webservice.dto.journey.JourneyResult;
import com.buses.webservice.service.journey.IJourneyService;

@Path("/journey")
@Component
public class JourneyResource {

	@Autowired
	private IJourneyService journeyService;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public JourneyDTO getById(@PathParam("id") Integer journeyId) {
		return journeyService.getById(journeyId);
	}

	@GET
	@Produces("application/json")
	public JourneyResult getAll() {
		return journeyService.getAll();
	}

	@POST
	public JourneyDTO save(JourneyDTO journey) {
		return journeyService.save(journey);
	}

	@DELETE
	@Path("{id}")
	@Produces("application/json")
	public String delete(@PathParam("id") Integer journeyId) {
		return journeyService.delete(journeyId) ? "true" : "false";
	}

	@GET
	@Path("search/{key}")
	@Produces("application/json")
	public JourneyResult search(@PathParam("key") String keyWord) {
		return journeyService.searchForAnyProperty(keyWord);
	}

	@GET
	@Path("count/{key}")
	@Produces("application/json")
	public String count(@PathParam("key") String keyWord) {
		return journeyService.count(keyWord).toString();
	}
}
