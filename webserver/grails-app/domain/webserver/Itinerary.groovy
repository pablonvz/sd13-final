package webserver

/**
 * Itinerary
 * A domain class describes the data object and it's mapping to the database
 */
class Itinerary {
      Integer origin
      Integer destination
      Date deletedAt
  static mapping = {
  }
    
  static constraints = {
  }
}
