package webserver

/**
 * Driver
 * A domain class describes the data object and it's mapping to the database
 */
class Driver {
      Integer personId
      String firstName
      String lastName
      Date birthDate
      String sex
      String document
      String email
      Date deletedAt
      Integer agencyId
  static mapping = {
  }
    
  static constraints = {
  }
}
