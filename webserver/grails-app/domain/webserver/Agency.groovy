package webserver

/**
 * Agency
 * A domain class describes the data object and it's mapping to the database
 */
class Agency {
      Integer cityId
      String description
      Date deletedAt
  static mapping = {
  }
    
  static constraints = {
  }
}
