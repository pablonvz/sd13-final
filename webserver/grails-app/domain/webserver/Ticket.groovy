package webserver

/**
 * Ticket
 * A domain class describes the data object and it's mapping to the database
 */
class Ticket {
      Integer clientId
      Integer journeyId
      Integer seat
      String status
      Date deletedAt
  static mapping = {
  }
    
  static constraints = {
  }
}
