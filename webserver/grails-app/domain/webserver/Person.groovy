package webserver

/**
 * Person
 * A domain class describes the data object and it's mapping to the database
 */
class Person {
      String firstName
      String lastName
      Date birthDate
      String sex
      String document
      String email
      Date deletedAt
  static mapping = {
  }
    
  static constraints = {
  }
}
