package webserver

/**
 * City
 * A domain class describes the data object and it's mapping to the database
 */
class City {
      Integer stateId
      String name
      Date deletedAt
  static mapping = {
  }
    
  static constraints = {
  }
}
