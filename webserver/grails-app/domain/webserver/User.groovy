package webserver

/**
 * User
 * A domain class describes the data object and it's mapping to the database
 */
class User {
      Integer personId
      String firstName
      String lastName
      Date birthDate
      String sex
      String document
      String email
      Date deletedAt
      Integer agencyId
      String passwordHash
  static mapping = {
  }
    
  static constraints = {
  }
}
