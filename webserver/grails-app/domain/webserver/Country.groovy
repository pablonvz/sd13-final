package webserver

/**
 * Country
 * A domain class describes the data object and it's mapping to the database
 */
class Country {
      String code
      String name
      Date deletedAt
  static mapping = {
  }
    
  static constraints = {
	  name blank:false
	  code blank:false
  }
}
