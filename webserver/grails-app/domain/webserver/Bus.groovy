package webserver

/**
 * Bus
 * A domain class describes the data object and it's mapping to the database
 */
class Bus {
      Integer capacty
      String model
      String fuel
      String brand
      Date year
      String motor
	  Integer Number
      Date deletedAt
  static mapping = {
  }
    
  static constraints = {
  }
}
