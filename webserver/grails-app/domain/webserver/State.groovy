package webserver

/**
 * State
 * A domain class describes the data object and it's mapping to the database
 */
class State {
      Integer countryId
      String name
      Date deletedAt
  static mapping = {
  }
    
  static constraints = {
  }
}
