package webserver

import java.util.Date;

/**
 * Journey
 * A domain class describes the data object and it's mapping to the database
 */
class Journey {
      Integer itineraryId
      Integer busId
	  Date date
      Integer driverId
      Integer price
      Double initialKm
      Double endKm
      String incident
      Date deletedAt
  static mapping = {
  }
    
  static constraints = {
  }
}
