class SecurityFilters {
	def filters = {
		loginCheck(controller: '*', action: '*') {
			before = {
				if (!session.user && !controllerName.equals('authentication')) {
					redirect(action: 'login', controller: "authentication")
					return false
				}
			}
		}
	}
}

