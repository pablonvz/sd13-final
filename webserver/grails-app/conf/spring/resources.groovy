import org.springframework.cache.concurrent.ConcurrentMapCacheFactoryBean
import org.springframework.cache.support.SimpleCacheManager

import com.buses.app.resources.agency.AgencyResourceImpl
import com.buses.app.resources.agency.IAgencyResource;
import com.buses.app.resources.bus.BusResourceImpl
import com.buses.app.resources.bus.IBusResource;
import com.buses.app.resources.city.CityResourceImpl
import com.buses.app.resources.city.ICityResource;
import com.buses.app.resources.client.ClientResourceImpl
import com.buses.app.resources.client.IClientResource;
import com.buses.app.resources.country.CountryResourceImpl
import com.buses.app.resources.country.ICountryResource;
import com.buses.app.resources.driver.DriverResourceImpl
import com.buses.app.resources.driver.IDriverResource;
import com.buses.app.resources.itinerary.IItineraryResource;
import com.buses.app.resources.itinerary.ItineraryResourceImpl
import com.buses.app.resources.journey.IJourneyResource;
import com.buses.app.resources.journey.JourneyResourceImpl
import com.buses.app.resources.person.PersonResourceImpl
import com.buses.app.resources.state.IStateResource;
import com.buses.app.resources.state.StateResourceImpl
import com.buses.app.resources.ticket.ITicketResource;
import com.buses.app.resources.ticket.TicketResourceImpl
import com.buses.app.resources.user.UserResourceImpl
import com.buses.app.services.agency.AgencyServiceImpl
import com.buses.app.services.bus.BusServiceImpl
import com.buses.app.services.city.CityServiceImpl
import com.buses.app.services.client.ClientServiceImpl
import com.buses.app.services.country.CountryServiceImpl
import com.buses.app.services.driver.DriverServiceImpl
import com.buses.app.services.itinerary.ItineraryServiceImpl
import com.buses.app.services.journey.JourneyServiceImpl
import com.buses.app.services.person.PersonServiceImpl
import com.buses.app.services.state.StateServiceImpl
import com.buses.app.services.ticket.TicketServiceImpl
import com.buses.app.services.user.UserServiceImpl

// Place your Spring DSL code here
beans = {
	agencyResource(AgencyResourceImpl)
	busResource(BusResourceImpl)
	cityResource(CityResourceImpl)
	clientResource(ClientResourceImpl)
	countryResource(CountryResourceImpl)
	driverResource(DriverResourceImpl)
	itineraryResource(ItineraryResourceImpl)
	journeyResource(JourneyResourceImpl)
	personResource(PersonResourceImpl)
	stateResource(StateResourceImpl)
	ticketResource(TicketResourceImpl)
	userResource(UserResourceImpl)

	agencyService(AgencyServiceImpl, ref("agencyResource"), ref("cityResource"), ref("stateResource"), ref("countryResource"))
	personService(PersonServiceImpl, ref("personResource"))
	busService(BusServiceImpl, ref("busResource"))
	cityService(CityServiceImpl,ref("cityResource"), ref("stateResource"), ref("countryResource"))
	clientService(ClientServiceImpl, ref("clientResource"), ref("personResource"))
	countryService(CountryServiceImpl, ref("countryResource"))
	driverService(DriverServiceImpl, ref("driverResource"), ref("personResource"), 
		ref("agencyResource"), ref("countryResource"), ref("cityResource"), ref("stateResource"), ref("personService"))
	
	itineraryService(ItineraryServiceImpl, ref("itineraryResource"), ref("cityResource"), ref("stateResource"), ref("countryResource"))
	journeyService(JourneyServiceImpl,
			ref("journeyResource"), ref("itineraryResource"), ref("busResource"), ref("driverResource"),
			ref("personResource"), ref("agencyResource"), ref("cityResource"), ref("stateResource"), ref("countryResource"))

	stateService(StateServiceImpl,ref("stateResource"), ref("countryResource"))
	ticketService(TicketServiceImpl, ref("ticketResource"),
			ref("clientResource"), ref("journeyResource"),
			ref("itineraryResource"), ref("busResource"), ref("driverResource"),
			ref("personResource"), ref("agencyResource"), ref("cityResource"),
			ref("stateResource"), ref("countryResource"))

	userService(UserServiceImpl,
			ref("userResource"), ref("personResource"), ref("agencyResource"),
			ref("cityResource"), ref("stateResource"), ref("countryResource"), ref("personService")
			)

	//cache
	concurrentMapCache(ConcurrentMapCacheFactoryBean){ name="buses-cache" }
	cacheManager(SimpleCacheManager){
		caches=[ref("concurrentMapCache")]
	}
}
