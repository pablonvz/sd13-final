
<%@ page import="webserver.Journey" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'journey.label', default: 'Journey')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-journey" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="journey.bus.label" default="Bus" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: journeyInstance, field: "bus.number")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="journey.driver.label" default="Driver" /></td>
				
				<td valign="top" class="value">${journeyInstance?.driver?.person?.firstName+" "+journeyInstance?.driver?.person?.lastName+", doc "+journeyInstance?.driver?.person?.document}</td>
				
			</tr>

			<tr class="prop">
				<td valign="top" class="name"><g:message code="journey.date.label" default="Date" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${journeyInstance?.date}" /></td>
				
			</tr>
				
			<tr class="prop">
				<td valign="top" class="name"><g:message code="journey.endKm.label" default="End Km" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: journeyInstance, field: "endKm")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="journey.incident.label" default="Incident" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: journeyInstance, field: "incident")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="journey.initialKm.label" default="Initial Km" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: journeyInstance, field: "initialKm")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="journey.itinerary.label" default="Itinerary" /></td>
				
				<td valign="top" class="value">${journeyInstance?.itinerary?.origin?.name+" - "+journeyInstance?.itinerary?.destination?.name}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="journey.price.label" default="Price" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: journeyInstance, field: "price")}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
