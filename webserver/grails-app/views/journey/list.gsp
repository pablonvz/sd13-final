
<%@ page import="webserver.Journey"%>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="kickstart" />
<g:set var="entityName"
	value="${message(code: 'journey.label', default: 'Journey')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>

	<section id="list-journey" class="first">

		<table class="table table-bordered margin-top-medium">
			<thead>
				<tr>

					<th>Bus</th>

					<g:sortableColumn property="date"
						title="${message(code: 'journey.date.label', default: 'Date')}" />

					<th>Itinerary</th>					
						
					<th>Driver</th>

					<g:sortableColumn property="endKm"
						title="${message(code: 'journey.endKm.label', default: 'End Km')}" />

					<g:sortableColumn property="incident"
						title="${message(code: 'journey.incident.label', default: 'Incident')}" />

					<g:sortableColumn property="initialKm"
						title="${message(code: 'journey.initialKm.label', default: 'Initial Km')}" />

				</tr>
			</thead>
			<tbody>
				<g:each in="${journeyInstanceList}" status="i" var="journeyInstance">
					<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

						<td><g:link action="show" id="${journeyInstance.id}">
								${fieldValue(bean: journeyInstance, field: "bus.number")}
							</g:link></td>
							
						<td><g:formatDate date="${journeyInstance.date}" /></td>
				
						<td>
							${" "+journeyInstance?.itinerary?.origin?.name+" - "+journeyInstance?.itinerary?.destination?.name}
						</td>
				
						<td>
							${journeyInstance?.driver?.person?.firstName+" "+journeyInstance?.driver?.person?.lastName+", doc "+journeyInstance?.driver?.person?.document}
						</td>

						<td>
							${fieldValue(bean: journeyInstance, field: "endKm")}
						</td>

						<td>
							${fieldValue(bean: journeyInstance, field: "incident")}
						</td>

						<td>
							${fieldValue(bean: journeyInstance, field: "initialKm")}
						</td>

					</tr>
				</g:each>
			</tbody>
		</table>
		<div class="container">
			<bs:paginate total="${journeyInstanceTotal}" />
		</div>
	</section>

</body>

</html>
