<%@ page import="webserver.Journey" %>


			<div
				class="control-group fieldcontain ${hasErrors(bean: journeyInstance, field: 'bus', 'error')} required">
				<label for="bus" class="control-label"><g:message
						code="claim.bus.label" default="Bus number" /><span
					class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="bus" name="bus_id"
						from="${buses}" optionKey="id" optionValue="number" required=""
						value="${journeyInstance?.bus?.id}" class="many-to-one" />
					<span class="help-inline">
						${hasErrors(bean: journeyInstance, field: 'bus', 'error')}
					</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: journeyInstance, field: 'date', 'error')} required">
				<label for="date" class="control-label"><g:message code="journey.date.label" default="Date" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<bs:datePicker name="date" precision="day"  value="${journeyInstance?.date}"  />
					<span class="help-inline">${hasErrors(bean: journeyInstance, field: 'date', 'error')}</span>
				</div>
			</div>
			
			<div
			  class="control-group fieldcontain ${hasErrors(bean: journeyInstance, field: 'driver?.id', 'error')} required">
			  <label for="driver?.id" class="control-label"><g:message
			      code="ticket.driver?.id.label" default="Driver" /><span
			    class="required-indicator">*</span></label>
			  <div class="controls">
			    <g:select id="driver?.id" name="driver_id"
			      from="${drivers}" optionKey="id" optionValue="${{it.person.firstName+" "+it.person.lastName+" - "+it.person.document}}" required=""
			      value="${journeyInstance?.driver?.id}" class="many-to-one" />
			    <span class="help-inline">
			      ${hasErrors(bean: journeyInstance, field: 'driver?.id', 'error')}
			    </span>
			  </div>
			</div>
		
			<div class="control-group fieldcontain ${hasErrors(bean: journeyInstance, field: 'initialKm', 'error')} required">
				<label for="initialKm" class="control-label"><g:message code="journey.initialKm.label" default="Initial Km" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:field type="number" id="initialKm" name="initialKm" step="any" required="" value="${journeyInstance.initialKm}" min="0" />
					<span class="help-inline">${hasErrors(bean: journeyInstance, field: 'initialKm', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: journeyInstance, field: 'endKm', 'error')} required">
				<label for="endKm" class="control-label"><g:message code="journey.endKm.label" default="End Km" /></label>
				<div class="controls">
					<g:field type="number" name="endKm"  id="endKm" step="any" value="${journeyInstance.endKm}" min="0"/>
					<span class="help-inline">${hasErrors(bean: journeyInstance, field: 'endKm', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: journeyInstance, field: 'incident', 'error')} ">
				<label for="incident" class="control-label"><g:message code="journey.incident.label" default="Incident" /></label>
				<div class="controls">
					<g:textField name="incident" value="${journeyInstance?.incident}"/>
					<span class="help-inline">${hasErrors(bean: journeyInstance, field: 'incident', 'error')}</span>
				</div>
			</div>

		<div
				class="control-group fieldcontain ${hasErrors(bean: journeyInstance, field: 'itinerary', 'error')} required">
				<label for="itinerary" class="control-label"><g:message
						code="claim.itinerary.label" default="Itinerary" /><span
					class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="itinerary" name="itinerary_id"
						from="${itineraries}" optionKey="id" optionValue="${{it?.origin?.name +' - '+it?.destination?.name}}" required=""
						value="${journeyInstance?.itinerary?.id}" class="many-to-one" />
					<span class="help-inline">
						${hasErrors(bean: journeyInstance, field: 'itinerary', 'error')}
					</span>
				</div>
			</div>


			<div class="control-group fieldcontain ${hasErrors(bean: journeyInstance, field: 'price', 'error')} required">
				<label for="price" class="control-label"><g:message code="journey.price.label" default="Price" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:field type="number" name="price" required="" value="${journeyInstance.price}" min="1" />
					<span class="help-inline">${hasErrors(bean: journeyInstance, field: 'price', 'error')}</span>
				</div>
			</div>
			
	<g:javascript>
		/*var lostFocus = function(){
			var $this = $(this);
			var inintKmVal = $('[name="initialKm"]').val();
			if($this.val() <= inintKmVal){
				//$('span[for="endKm"][generated="true"].alert-danger').remove();
				$this.after($('<span for="endKm" generated="true" class="alert-danger">Please enter a value greater than or equal to '+inintKmVal+'.</span>'));
			}
		};
		$('[name="endKm"]').blur(lostFocus);*/
		$.validator.addMethod(
		    "greaterThan",
		    function(value, element, params) {
		        var target = $(params).val();
		        var isValueNumeric = !isNaN(parseFloat(value)) && isFinite(value);
		        var isTargetNumeric = !isNaN(parseFloat(target)) && isFinite(target);
		        if (isValueNumeric && isTargetNumeric) {
		            return Number(value) > Number(target);
		        }
		
		        if (!/Invalid|NaN/.test(new Date(value))) {
		            return new Date(value) > new Date(target);
		        }
		
		        return false;
		    },
		    function(tg){
				var iVal = $(tg).val();
		    	return $('<span style="font-weight:normal" for="endKm" generated="true" class="alert-danger">Please enter a value greater than or equal to '+iVal+'.</span>')//);
		    });
		//$('#endKm').rules('add', { greaterThan: '#initialKm' });
		/*$("#endKm").parents("form").validate({
		    rules: {
		        endKm: { greaterThan: "#initialKm" }
		    }
		});*/
		
	</g:javascript>	
	

