<%@ page import="webserver.Ticket" %>

<span class="ticket-form">
	      <div
            class="control-group fieldcontain ${hasErrors(bean: ticketInstance, field: 'status', 'error')} ">
            <label for="status" class="control-label"><g:message
                code="ticket.status.label" default="Status" /><span
		class="required-indicator">*</span></label>
            <div class="controls">
              <g:select id="status" name="status"
                from="${statuses}" required=""
                value="${ticketInstance?.status}" />
              <span class="help-inline">
                ${hasErrors(bean: ticketInstance, field: 'status', 'error')}
              </span>
            </div>
          </div>


			<div
			  class="control-group fieldcontain ${hasErrors(bean: ticketInstance, field: 'client?.id', 'error')} required">
			  <label for="client?.id" class="control-label"><g:message
			      code="ticket.client?.id.label" default="Client" /><span
			    class="required-indicator">*</span></label>
			  <div class="controls">
			    <g:select id="clientId" name="clientId"
			      from="${clients}" optionKey="id" optionValue="${{it.person.firstName+" "+it.person.lastName+" - "+it.person.document}}" required=""
			      value="${ticketInstance?.client?.id}" class="many-to-one" />
			    <span class="help-inline">
			      ${hasErrors(bean: ticketInstance, field: 'client?.id', 'error')}
			    </span>
			  </div>
			</div>

			<div
				class="control-group fieldcontain ${hasErrors(bean: ticketInstance, field: 'journey', 'error')} required">
				<label for="journey" class="control-label"><g:message
						code="claim.journey.label" default="Journey" /><span
					class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="journey" name="journeyId"
						from="${journeys}" optionKey="id" optionValue="${{"From "+it.itinerary?.origin.name+" to "+it.itinerary?.destination.name+" - "+it.date?.toString()+" - Bus #"+it.bus?.number}}" required=""
						value="${ticketInstance?.journey?.id}" class="many-to-one" />
					<span class="help-inline">
						${hasErrors(bean: ticketInstance, field: 'journey', 'error')}
					</span>
				</div>
			</div>


			<div class="control-group fieldcontain ${hasErrors(bean: ticketInstance, field: 'seat', 'error')} required">
				<label for="seat" class="control-label"><g:message code="ticket.seat.label" default="Seat" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<span id="current-seat" style="display:none">${ticketInstance.seat}</span>
					<g:select id="seats" name="seat"
						from="${[]}" optionKey="id" optionValue="" required=""
						value="" class="many-to-one" />
					<span class="help-inline">${hasErrors(bean: ticketInstance, field: 'seat', 'error')}</span>
				</div>
			</div>

</span>
<g:javascript>
	$(document).ready(function(){
		$("#journey").on("change", function(){
			var url = "http://localhost:8080/webserver/ticket/freeSeats.json?journeyId="+$(this).val();
			$.ajax({
			  dataType: "json",
			  url: url,
			  success: function(data){
			  	var current = $("#current-seat").text();
			  	if(current.length > 0) data.unshift(current);
			  	var seats = $("#seats");
			  	seats.empty();
			  	for(var i = 0; i < data.length; i++)
				  	seats.append($('<option>', {value: data[i], text: data[i]}));
			  }
			});
		}).change();
	});
</g:javascript>