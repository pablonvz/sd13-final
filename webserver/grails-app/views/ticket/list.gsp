
<%@ page import="webserver.Ticket"%>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="kickstart" />
<g:set var="entityName"
	value="${message(code: 'ticket.label', default: 'Ticket')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>

	<section id="list-ticket" class="first">

		<table class="table table-bordered margin-top-medium">
			<thead>
				<tr>

					<th>${message(code: 'ticket.clientId.label', default: 'Client')}</th>

					<th>${message(code: 'ticket.journeyId.label', default: 'Journey')}</th>

					<g:sortableColumn property="seat"
						title="${message(code: 'ticket.seat.label', default: 'Seat')}" />

					<g:sortableColumn property="status"
						title="${message(code: 'ticket.status.label', default: 'Status')}" />

				</tr>
			</thead>
			<tbody>
				<g:each in="${ticketInstanceList}" status="i" var="ticketInstance">
					<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

						<td><g:link action="show" id="${ticketInstance.id}">
								${ticketInstance?.client?.person?.firstName+" "+ticketInstance?.client?.person?.lastName+", doc "+ticketInstance?.client?.person?.document}
							</g:link></td>

						<td>
							${"From "+ticketInstance?.journey?.itinerary?.origin.name+" to "+ticketInstance?.journey?.itinerary?.destination.name+" - "+ticketInstance?.journey?.date?.toString()+" - Bus #"+ticketInstance?.journey?.bus?.number}
						</td>

						<td>
							${fieldValue(bean: ticketInstance, field: "seat")}
						</td>

						<td>
							${fieldValue(bean: ticketInstance, field: "status")}
						</td>

					</tr>
				</g:each>
			</tbody>
		</table>
		<div class="container">
			<bs:paginate total="${ticketInstanceTotal}" />
		</div>
	</section>

</body>

</html>
