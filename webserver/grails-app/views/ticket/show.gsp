
<%@ page import="webserver.Ticket" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'ticket.label', default: 'Ticket')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-ticket" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="ticket.clientId.label" default="Client" /></td>
				
				<td valign="top" class="value">${ticketInstance?.client?.person?.firstName+" "+ticketInstance?.client?.person?.lastName+", doc "+ticketInstance?.client?.person?.document}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="ticket.journeyId.label" default="Journey" /></td>
				
				<td valign="top" class="value">${"From "+ticketInstance?.journey?.itinerary?.origin.name+" to "+ticketInstance?.journey?.itinerary?.destination.name+" - "+ticketInstance?.journey?.date?.toString()+" - Bus #"+ticketInstance?.journey?.bus?.number}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="ticket.seat.label" default="Seat" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: ticketInstance, field: "seat")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="ticket.status.label" default="Status" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: ticketInstance, field: "status")}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
