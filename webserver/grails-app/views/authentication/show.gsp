
<%@ page import="webserver.Authentication" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'authentication.label', default: 'Authentication')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-authentication" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="authentication.email.label" default="Email" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: authenticationInstance, field: "email")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="authentication.password.label" default="Password" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: authenticationInstance, field: "password")}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
