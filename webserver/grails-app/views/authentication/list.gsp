
<%@ page import="webserver.Authentication" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'authentication.label', default: 'Authentication')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-authentication" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="email" title="${message(code: 'authentication.email.label', default: 'Email')}" />
			
				<g:sortableColumn property="password" title="${message(code: 'authentication.password.label', default: 'Password')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${authenticationInstanceList}" status="i" var="authenticationInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${authenticationInstance.id}">${fieldValue(bean: authenticationInstance, field: "email")}</g:link></td>
			
				<td>${fieldValue(bean: authenticationInstance, field: "password")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${authenticationInstanceTotal}" />
	</div>
</section>

</body>

</html>
