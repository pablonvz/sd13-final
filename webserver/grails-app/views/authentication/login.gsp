<%@ page import="webserver.Authentication" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'authentication.label', default: 'Authentication')}" />
	<%--<title><g:message code="default.create.label" args="[entityName]" /></title>
--%>
	<title>Authentication</title>
</head>

<body>

<section id="create-authentication" class="first">
	<g:if test="${errorMessage}" >
		<div class="alert alert-danger alert-dismissable" style="width:40%" >
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  ${errorMessage}
		</div>
	</g:if>
	<g:hasErrors bean="${authenticationInstance}">
	<div class="alert alert-error">
		<g:renderErrors bean="${authenticationInstance}" as="list" />
	</div>
	</g:hasErrors>
	
	<g:form action="save" class="form-horizontal" >
		<fieldset class="form">
			<g:render template="form"/>
		</fieldset>
		<div class="form-actions">
			<g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.login.label', default: 'Login')}" /><%--
            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
		--%></div>
	</g:form>
	
</section>
		
</body>

</html>
