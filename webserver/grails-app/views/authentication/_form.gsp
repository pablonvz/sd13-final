<%@ page import="webserver.Authentication" %>


			<div class="control-group fieldcontain ${hasErrors(bean: authenticationInstance, field: 'email', 'error')} ">
				<label for="email" class="control-label"><g:message code="authentication.email.label" default="Email" /><span
		class="required-indicator">*</span></label>
				<div class="controls">
					<g:field type="email" required="" name="email" value="${authenticationInstance?.email}"/>
					<span class="help-inline">${hasErrors(bean: authenticationInstance, field: 'email', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: authenticationInstance, field: 'password', 'error')} ">
				<label for="password" class="control-label"><g:message code="authentication.password.label" default="Password" /><span
		class="required-indicator">*</span></label>
				<div class="controls">
					<g:passwordField required="" name="password" value="${authenticationInstance?.password}"/>
					<span class="help-inline">${hasErrors(bean: authenticationInstance, field: 'password', 'error')}</span>
				</div>
			</div>

