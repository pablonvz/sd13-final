
<%@ page import="webserver.Itinerary" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'itinerary.label', default: 'Itinerary')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-itinerary" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="destination" title="${message(code: 'itinerary.destination.label', default: 'Destination')}" />
			
				<g:sortableColumn property="origin" title="${message(code: 'itinerary.origin.label', default: 'Origin')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${itineraryInstanceList}" status="i" var="itineraryInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${itineraryInstance.id}">${itineraryInstance.destination?.name+' - '+itineraryInstance.destination?.state?.name+' - '+itineraryInstance.destination?.state?.country?.name}</g:link></td>
					
				<td>${itineraryInstance.origin?.name+' - '+itineraryInstance.origin?.state?.name+' - '+itineraryInstance.origin?.state?.country?.name}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${itineraryInstanceTotal}" />
	</div>
</section>

</body>

</html>
