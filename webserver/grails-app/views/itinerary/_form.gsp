<%@ page import="webserver.Itinerary" %>




			<div class="control-group fieldcontain ${hasErrors(bean: itineraryInstance, field: 'destination', 'error')} required">
				<label for="destination" class="control-label"><g:message code="itinerary.destination.label" default="Destination" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="destination" name="destination.id" from="${cities}" optionValue="${{it.name +' - '+it.state.name+' - '+it.state.country.name}}"
			optionKey="id" required="" value="${itineraryInstance?.destination?.id}"
			class="many-to-one" />
					<span class="help-inline">${hasErrors(bean: itineraryInstance, field: 'destination', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: itineraryInstance, field: 'origin', 'error')} required">
				<label for="origin" class="control-label"><g:message code="itinerary.origin.label" default="Origin" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="origin" name="origin.id" from="${cities}" optionValue="${{it.name +' - '+it.state.name+' - '+it.state.country.name}}"
			optionKey="id" required="" value="${itineraryInstance?.origin?.id}"
			class="many-to-one" />
					<span class="help-inline">${hasErrors(bean: itineraryInstance, field: 'origin', 'error')}</span>
				</div>
			</div>

