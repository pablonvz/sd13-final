
<%@ page import="webserver.Itinerary" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'itinerary.label', default: 'Itinerary')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-itinerary" class="first">

	<table class="table">
		<tbody>
		
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="itinerary.destination.label" default="Destination" /></td>
				
				<td valign="top" class="value">${itineraryInstance.destination?.name+' - '+itineraryInstance.destination?.state?.name+' - '+itineraryInstance.destination?.state?.country?.name}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="itinerary.origin.label" default="Origin" /></td>
				
				<td valign="top" class="value">${itineraryInstance.origin?.name+' - '+itineraryInstance.origin?.state?.name+' - '+itineraryInstance.origin?.state?.country?.name}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
