
<%@ page import="webserver.City" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'city.label', default: 'City')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-city" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
				<g:sortableColumn property="name" title="${message(code: 'city.name.label', default: 'Name')}" />
			
				<th>${message(code: 'city.stateId.label', default: 'State')}</th>
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${cityInstanceList}" status="i" var="cityInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${cityInstance.id}">${fieldValue(bean: cityInstance, field: "name")}</g:link></td>
				<td>${fieldValue(bean: cityInstance, field: "state.name")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${cityInstanceTotal}" />
	</div>
</section>

</body>

</html>
