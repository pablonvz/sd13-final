
<%@ page import="webserver.City" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'city.label', default: 'City')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-city" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="city.name.label" default="Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: cityInstance, field: "name")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="city.state.label" default="State" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: cityInstance, field: "state.name")}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
