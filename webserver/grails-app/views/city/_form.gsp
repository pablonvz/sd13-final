<%@ page import="webserver.City" %>




			<div class="control-group fieldcontain ${hasErrors(bean: cityInstance, field: 'name', 'error')} ">
				<label for="name" class="control-label"><g:message code="city.name.label" default="Name" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="name" value="${cityInstance?.name}" required="" minlength="2" type="text"/>
					<span class="help-inline">${hasErrors(bean: cityInstance, field: 'name', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: cityInstance, field: 'stateId', 'error')} required">
				<label for="state" class="control-label"><g:message code="city.state.label" default="State" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="state" name="state.id"
						from="${states}" optionKey="id" required="" optionValue="${{it.name +' - '+it.country.name}}"
						value="${cityInstance?.state?.id}" class="many-to-one" />
					<span class="help-inline">${hasErrors(bean: cityInstance, field: 'state', 'error')}</span>
				</div>
			</div>

