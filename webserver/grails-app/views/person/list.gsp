
<%@ page import="webserver.Person" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'person.label', default: 'Person')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-person" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="birthDate" title="${message(code: 'person.birthDate.label', default: 'Birth Date')}" />
			
				<g:sortableColumn property="document" title="${message(code: 'person.document.label', default: 'Document')}" />
			
				<g:sortableColumn property="email" title="${message(code: 'person.email.label', default: 'Email')}" />
			
				<g:sortableColumn property="firstName" title="${message(code: 'person.firstName.label', default: 'First Name')}" />
			
				<g:sortableColumn property="lastName" title="${message(code: 'person.lastName.label', default: 'Last Name')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${personInstanceList}" status="i" var="personInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${personInstance.id}">${fieldValue(bean: personInstance, field: "birthDate")}</g:link></td>
			
				<td>${fieldValue(bean: personInstance, field: "document")}</td>
			
				<td>${fieldValue(bean: personInstance, field: "email")}</td>
			
				<td>${fieldValue(bean: personInstance, field: "firstName")}</td>
			
				<td>${fieldValue(bean: personInstance, field: "lastName")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${personInstanceTotal}" />
	</div>
</section>

</body>

</html>
