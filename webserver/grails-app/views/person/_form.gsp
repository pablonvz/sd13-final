<%@ page import="webserver.Person"%>



<div
	class="control-group fieldcontain ${hasErrors(bean: personInstance, field: 'birthDate', 'error')} required">
	<label for="birthDate" class="control-label"><g:message
			code="person.birthDate.label" default="Birth Date" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<bs:datePicker name="birthDate" precision="day"
			value="${personInstance?.birthDate}" />
		<span class="help-inline">
			${hasErrors(bean: personInstance, field: 'birthDate', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: personInstance, field: 'document', 'error')} ">
	<label for="document" class="control-label"><g:message
			code="person.document.label" default="Document" /></label>
	<div class="controls">
		<g:textField name="document" value="${personInstance?.document}" />
		<span class="help-inline">
			${hasErrors(bean: personInstance, field: 'document', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: personInstance, field: 'email', 'error')} ">
	<label for="email" class="control-label"><g:message
			code="person.email.label" default="Email" /></label>
	<div class="controls">
		<g:textField name="email" value="${personInstance?.email}" />
		<span class="help-inline">
			${hasErrors(bean: personInstance, field: 'email', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: personInstance, field: 'firstName', 'error')} ">
	<label for="firstName" class="control-label"><g:message
			code="person.firstName.label" default="First Name" /></label>
	<div class="controls">
		<g:textField name="firstName" value="${personInstance?.firstName}" />
		<span class="help-inline">
			${hasErrors(bean: personInstance, field: 'firstName', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: personInstance, field: 'lastName', 'error')} ">
	<label for="lastName" class="control-label"><g:message
			code="person.lastName.label" default="Last Name" /></label>
	<div class="controls">
		<g:textField name="lastName" value="${personInstance?.lastName}" />
		<span class="help-inline">
			${hasErrors(bean: personInstance, field: 'lastName', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: personInstance, field: 'sex', 'error')} ">
	<label for="sex" class="control-label"><g:message
			code="person.sex.label" default="Sex" /></label>
	<div class="controls">
		<g:select id="sex" name="sex" from="${availableSex}" required=""
			value="${personInstance?.sex}" />
		<span class="help-inline"> ${hasErrors(bean: personInstance, field: 'sex', 'error')}
		</span>
	</div>
</div>

