<%@ page import="webserver.Country" %>



			<div class="control-group fieldcontain ${hasErrors(bean: countryInstance, field: 'code', 'error')} ">
				<label for="code" class="control-label"><g:message code="country.code.label" default="Code" /><span
		class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField required="" name="code" value="${countryInstance?.code}"/>
					<span class="help-inline">${hasErrors(bean: countryInstance, field: 'code', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: countryInstance, field: 'name', 'error')} ">
				<label for="name" class="control-label"><g:message code="country.name.label" default="Name" /><span
		class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField required="" name="name" value="${countryInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: countryInstance, field: 'name', 'error')}</span>
				</div>
			</div>

