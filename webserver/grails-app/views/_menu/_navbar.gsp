<nav id="Navbar" class="navbar navbar-fixed-top navbar-inverse" role="navigation">
	<div class="container">
	
	    <div class="navbar-header">
	    
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        		<span class="sr-only">Toggle navigation</span>
        		<span class="icon-bar"></span>
	           	<span class="icon-bar"></span>
	           	<span class="icon-bar"></span>
			</button>
	
			<a class="navbar-brand" href="${createLink(uri: '/')}">
				<img class="logo" src="${resource(plugin: 'kickstart-with-bootstrap', dir:'kickstart/img', file:'grails.png')}" alt="${meta(name:'app.name')}" width="16px" height="16px"/> 
				${meta(name:'app.name')}
				<small> v${meta(name:'app.version')}</small>
			</a>
		</div>

		<div class="collapse navbar-collapse navbar-ex1-collapse" role="navigation">
		<g:if test="${session.user}">
			<ul class="nav navbar-nav">
			
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Browse <b class="caret"></b></a>
					<ul class="dropdown-menu">
	                    <g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
	                    <li style="display: ${ ( (c.logicalPropertyName.equalsIgnoreCase('Home') || 
					c.logicalPropertyName.equalsIgnoreCase('Dbdoc')) ||
					c.logicalPropertyName.equalsIgnoreCase('Person') ||
					c.logicalPropertyName.equalsIgnoreCase('Authentication') ||
					c.logicalPropertyName.equalsIgnoreCase('_DemoPage')? 'none' : '') }"
					class="controller${params.controller == c.logicalPropertyName ? " active" : ""}">
					<g:link controller="${c.logicalPropertyName}" action="index">
						<g:message code="${c.logicalPropertyName}.label"
							default="${c.logicalPropertyName.capitalize()}" />
					</g:link>
				</li>
	                    </g:each>
					</ul>
				</li>
			</ul>
		</g:if>
    	<ul class="nav navbar-nav navbar-right">
 			<%--<g:render template="/_menu/search"/> 
			<g:render template="/_menu/admin"/>														
			<g:render template="/_menu/info"/>														
			--%>
			<g:if test="${!session.user}">
				<g:render template="/_menu/user"/><!-- NOTE: the renderDialog for the "Register" modal dialog MUST be placed outside the NavBar (at least for Bootstrap 2.1.1): see bottom of main.gsp -->
			</g:if>
			<g:else>
			
			
				
				<form class="navbar-form navbar-left" action="/webserver/authentication/logout" method="post" accept-charset="UTF-8">

					<input class="btn btn-default" type="submit" id="logout" value="Logout">
				</form>
			<%--<a class=""  href="/webserver/authentication/logout">Logout</a>
			--%></g:else>
			<g:render template="/_menu/language"/>														
	    </ul>			

		</div>
	</div>
</nav>
