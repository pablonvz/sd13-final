<r:use module="jquery-validate"></r:use>

<style type="text/css">
	input.alert-danger{
		border-color: #DD7188;
		-webkit-box-shadow: none;
		box-shadow: none;
		background:none;
	}
	input.alert-danger:focus{
		border-color: #DD7188;
		-webkit-box-shadow: 1px 1px 8px 0 #D97182;
		box-shadow: 1px 1px 8px 0 #D97182;
		background:none;
	}
	.form-control, span.alert-danger{	
		width: 300px;
		margin: 10px;
		margin-left:0;
	}
	span.alert-danger{
		padding:8px;
		-webkit-border-radius: 6px 6px 6px 6px;
		border-radius: 6px 6px 6px 6px;
		display:block;
		/*-webkit-box-shadow: 1px 1px 8px 0 #D9D9D9;
		box-shadow: 1px 1px 8px 0 #D9D9D9;*/
		margin-bottom: 20px;
	}
	.nav-pills > li + li, .nav-pills > li {
		margin-top: 15px;
		margin-bottom:15px;
	}
	ul#Menu li form{
		padding:0;
	}
	.form-actions{
		margin-top: 25px;
		text-align: right;
		width: 300px;
	}
	input#create, [name="_action_update"]{
		float: right;
		margin-left: 9px;
	}
	.ticket-form input, .ticket-form select{
		width:550px;
	}
</style>

<r:script>
	$("form.form-horizontal:eq(0)").validate({errorElement: 'span', errorClass: "alert-danger"});
	$(document).ready(function(){
		$('input, textarea, select').not(':input[type=button], :input[type=submit], :input[type=reset]').addClass("form-control");
		var value = getValueFromUrl("value");
		//$("input#search_value").val(value);
		if(value && value.length > 0){
			$("ul.pagination li a").each(function() {
			   var _href = $(this).attr("href"); 
			   $(this).attr("href", _href + '&value='+value);
			});
		}
		$("ul.pagination li.disabled a").each(function(){
			$(this).attr("href", "#");
		});
	});
	
	function getValueFromUrl(name){
	   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
	      return decodeURIComponent(name[1]);
	}
	
	
</r:script>
<g:if test="${session.user}">
	<div class="">
		<ul class="nav nav-tabs" data-role="listview" data-split-icon="gear"
			data-filter="true">

			<g:each status="i" var="c"
				in="${grailsApplication.controllerClasses[1..(grailsApplication.controllerClasses.size()-1)].sort { it.logicalPropertyName } }">
				<li
					style="display: ${ ( (c.logicalPropertyName.equalsIgnoreCase('Home') || 
					c.logicalPropertyName.equalsIgnoreCase('Dbdoc')) ||
					c.logicalPropertyName.equalsIgnoreCase('Person') ||
					c.logicalPropertyName.equalsIgnoreCase('Base') ||
					c.logicalPropertyName.equalsIgnoreCase('Authentication') ||
					c.logicalPropertyName.equalsIgnoreCase('_DemoPage')? 'none' : '') }"
					class="controller${params.controller == c.logicalPropertyName ? " active" : ""}">
					<g:link controller="${c.logicalPropertyName}" action="index">
						<g:message code="${c.logicalPropertyName}.label"
							default="${c.logicalPropertyName.capitalize()}" />
					</g:link>
				</li>
			</g:each>

		</ul>
	</div>
</g:if>