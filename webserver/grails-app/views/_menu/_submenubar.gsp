<!-- 
This menu is used to show function that can be triggered on the content (an object or list of objects).
-->

<%-- Only show the "Pills" navigation menu if a controller exists (but not for home) --%>
<g:if test="${	params.controller != null
			&&	params.controller != ''
			&&	params.controller != 'home'
			&&	params.controller != 'report'
}">
	<ul id="Menu" class="nav nav-pills">
		<g:if test="${params.action.toString().equalsIgnoreCase("list")}">
			 <li class="pull-right">
				 <form class="navbar-form navbar-left" role="search"  method="get">
				    <div class="form-group">
				        <input id="search_value" type="text" class="form-control" placeholder="Search"  name="value" value="${params.value}" />
						<input type="hidden" name="offset" value="0"/>
						<input type="hidden" name="max" value="10"/>
				    </div>
				    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
				</form>
	        </li>
        </g:if>
		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		<g:if test="${!entityName.equals("Authentication")}">		
			<li class="${ params.action == "list" ? 'active' : '' }">
				<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
			</li>
			<li class="${ params.action == "create" ? 'active' : '' }">
				<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
			</li>
			
			<g:if test="${ params.action == 'show' || params.action == 'edit' }">
				<!-- the item is an object (not a list) -->
				<li class="${ params.action == "edit" ? 'active' : '' }">
					<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
				</li>
				<li class="">
					<g:render template="/_common/modals/deleteTextLink"/>
				</li>
			</g:if>
		</g:if>
	</ul>
</g:if>
