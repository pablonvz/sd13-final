<%@ page import="webserver.State"%>



<div
	class="control-group fieldcontain ${hasErrors(bean: stateInstance, field: 'countryId', 'error')} required row">
	<label for="countryId" class="control-label"><g:message
			code="state.country.label" default="Country" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:select class="many-to-one" id="country" name="country.id"
			required="true" from="${countries}" optionKey="id" optionValue="name" value="${state?.country?.id}"/>
		<span class="help-inline">
			${hasErrors(bean: stateInstance, field: 'countryId', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: stateInstance, field: 'name', 'error')} row">
	<label for="name" class="control-label"><g:message
			code="state.name.label" default="Name" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField required="" class="form-control" name="name"
			value="${stateInstance?.name}" />
		<span class="help-inline">
			${hasErrors(bean: stateInstance, field: 'name', 'error')}
		</span>
	</div>
</div>
<br/>
