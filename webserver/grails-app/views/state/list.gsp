
<%@ page import="webserver.State" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'state.label', default: 'State')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-state" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<th>${message(code: 'state.country.label', default: 'Country')}</th>
			
				<g:sortableColumn property="name" title="${message(code: 'state.name.label', default: 'Name')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${stateInstanceList}" status="i" var="stateInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td>${fieldValue(bean: stateInstance, field: "country.name")}</td>
			
				<td><g:link action="show" id="${stateInstance.id}">${fieldValue(bean: stateInstance, field: "name")}</g:link></td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${stateInstanceTotal}" />
	</div>
</section>

</body>

</html>
