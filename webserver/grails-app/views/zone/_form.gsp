<%@ page import="webserver.Zone"%>



<div
	class="fieldcontain ${hasErrors(bean: zoneInstance, field: 'comment', 'error')} ">
	<label for="comment"> <g:message code="zone.comment.label"
			default="Comment" />

	</label>
	<g:textField name="comment" value="${zoneInstance?.comment}" />
</div>

<div
	class="fieldcontain ${hasErrors(bean: zoneInstance, field: 'name', 'error')} ">
	<label for="name"> <g:message code="zone.name.label"
			default="Name" />

	</label>
	<g:textField name="name" value="${zoneInstance?.name}" />
</div>

