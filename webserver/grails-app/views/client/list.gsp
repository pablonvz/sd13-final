
<%@ page import="webserver.Client" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'client.label', default: 'Client')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-client" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<th>${message(code: 'client.birthDate.label', default: 'Birth Date')}</th>
			
				<th>${message(code: 'client.document.label', default: 'Document')}</th>
			
				<th>${message(code: 'client.email.label', default: 'Email')}</th>
			
				<th>${message(code: 'client.firstName.label', default: 'First Name')}</th>
			
				<th>${message(code: 'client.lastName.label', default: 'Last Name')}</th>
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${clientInstanceList}" status="i" var="clientInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${clientInstance.id}">${fieldValue(bean: clientInstance, field: "person.birthDate")}</g:link></td>
			
				<td>${fieldValue(bean: clientInstance, field: "person.document")}</td>
			
				<td>${fieldValue(bean: clientInstance, field: "person.email")}</td>
			
				<td>${fieldValue(bean: clientInstance, field: "person.firstName")}</td>
			
				<td>${fieldValue(bean: clientInstance, field: "person.lastName")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${clientInstanceTotal}" />
	</div>
</section>

</body>

</html>
