<%@ page import="webserver.Client"%>


<div
	class="control-group fieldcontain ${hasErrors(bean: clientInstance, field: 'birthDate', 'error')} required">
	<label for="birthDate" class="control-label"><g:message
			code="client.birthDate.label" default="Birth Date" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<bs:datePicker name="birthDate" precision="day"
			value="${clientInstance?.person?.birthDate}" />
		<span class="help-inline">
			${hasErrors(bean: clientInstance, field: 'birthDate', 'error')}
		</span>
	</div>
</div>


<div
	class="control-group fieldcontain ${hasErrors(bean: clientInstance, field: 'document', 'error')} ">
	<label for="document" class="control-label"><g:message
			code="client.document.label" default="Document" /><span class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField name="document"
			value="${clientInstance?.person?.document}" required="" minlength="2" type="text"/>
		<span class="help-inline">
			${hasErrors(bean: clientInstance, field: 'document', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: clientInstance, field: 'email', 'error')} ">
	<label for="email" class="control-label"><g:message
			code="client.email.label" default="Email" /></label>
	<div class="controls">
		<g:field name="email" value="${clientInstance?.person?.email}" type="email"/>
		<span class="help-inline">
			${hasErrors(bean: clientInstance, field: 'email', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: clientInstance, field: 'firstName', 'error')} ">
	<label for="firstName" class="control-label"><g:message
			code="client.firstName.label" default="First Name" /><span class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField name="firstName"
			value="${clientInstance?.person?.firstName}"  required="" minlength="2" type="text"/>
		<span class="help-inline">
			${hasErrors(bean: clientInstance, field: 'firstName', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: clientInstance, field: 'lastName', 'error')} ">
	<label for="lastName" class="control-label"><g:message
			code="client.lastName.label" default="Last Name" /><span class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField name="lastName"
			value="${clientInstance?.person?.lastName}"  required="" minlength="2" type="text"/>
		<span class="help-inline">
			${hasErrors(bean: clientInstance, field: 'lastName', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: clientInstance, field: 'sex', 'error')} ">
	<label for="sex" class="control-label"><g:message
			code="client.sex.label" default="Sex" /><span class="required-indicator">*</span></label>
	<div class="controls">
		<g:select id="sex" name="sex" from="${availableSex}" required=""
			value="${clientInstance?.person?.sex}" />
		<span class="help-inline"> ${hasErrors(bean: clientInstance, field: 'sex', 'error')}
		</span>
	</div>
</div>


