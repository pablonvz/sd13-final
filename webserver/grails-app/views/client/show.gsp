
<%@ page import="webserver.Client" %>
<!doctype html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="kickstart" />
  <g:set var="entityName" value="${message(code: 'client?.person?.label', default: 'Client')}" />
  <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-client" class="first">

  <table class="table">
    <tbody>
    
      <tr class="prop">
        <td valign="top" class="name"><g:message code="client?.person?.birthDate.label" default="Birth Date" /></td>
        
        <td valign="top" class="value"><g:formatDate date="${clientInstance?.person?.birthDate}" /></td>
        
      </tr>
    
    
      <tr class="prop">
        <td valign="top" class="name"><g:message code="client?.person?.document.label" default="Document" /></td>
        
        <td valign="top" class="value">${fieldValue(bean: clientInstance, field: "person.document")}</td>
        
      </tr>
    
      <tr class="prop">
        <td valign="top" class="name"><g:message code="client?.person?.email.label" default="Email" /></td>
        
        <td valign="top" class="value">${fieldValue(bean: clientInstance, field: "person.email")}</td>
        
      </tr>
    
      <tr class="prop">
        <td valign="top" class="name"><g:message code="client?.person?.firstName.label" default="First Name" /></td>
        
        <td valign="top" class="value">${fieldValue(bean: clientInstance, field: "person.firstName")}</td>
        
      </tr>
    
      <tr class="prop">
        <td valign="top" class="name"><g:message code="client?.person?.lastName.label" default="Last Name" /></td>
        
        <td valign="top" class="value">${fieldValue(bean: clientInstance, field: "person.lastName")}</td>
        
      </tr>
    
      <tr class="prop">
        <td valign="top" class="name"><g:message code="client?.person?.personId.label" default="Person Id" /></td>
        
        <td valign="top" class="value">${fieldValue(bean: clientInstance, field: "person.id")}</td>
        
      </tr>
    
      <tr class="prop">
        <td valign="top" class="name"><g:message code="client?.person?.sex.label" default="Sex" /></td>
        
        <td valign="top" class="value">${fieldValue(bean: clientInstance, field: "person.sex")}</td>
        
      </tr>
    
    </tbody>
  </table>
</section>

</body>

</html>
