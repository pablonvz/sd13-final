
<%@ page import="webserver.Driver" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'driver.label', default: 'Driver')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-driver" class="first">

  <table class="table table-bordered margin-top-medium">
    <thead>
      <tr>
      
        <th>${message(code: 'driver.person.document.label', default: 'Document')}</th>
      
        <th>${message(code: 'driver.person.firstName.label', default: 'Full Name')}</th>
      
        <th>${message(code: 'driver.person.birthDate.label', default: 'Birth Date')}</th>
      
        <th>${message(code: 'driver.person.email.label', default: 'Email')}</th>
      
        <th>${message(code: 'driver.agency.label', default: 'Agency')}</th>
      
      </tr>
    </thead>
    <tbody>
    <g:each in="${driverInstanceList}" status="i" var="driverInstance">
      <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
      
        <td><g:link action="show" id="${driverInstance.id}">${fieldValue(bean: driverInstance, field: "person.document")}</g:link></td>
      
        <td>${fieldValue(bean: driverInstance, field: "person.firstName")} ${fieldValue(bean: driverInstance, field: "person.lastName")}</td>
      
        <td><g:formatDate date="${driverInstance.person.birthDate}" /></td>
      
        <td>${fieldValue(bean: driverInstance, field: "person.email")}</td>
      
        <td>${fieldValue(bean: driverInstance, field: "agency.description")}</td>
      
      </tr>
    </g:each>
    </tbody>
  </table>
  <div class="container">
    <bs:paginate total="${driverInstanceTotal}" />
  </div>
</section>

</body>

</html>
