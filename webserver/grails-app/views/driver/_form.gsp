<%@ page import="webserver.Driver"%>


<div
	class="control-group fieldcontain ${hasErrors(bean: driverInstance, field: 'agency', 'error')} required">
	<label for="agency" class="control-label"><g:message
			code="claim.agency.label" default="agency" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:select id="agency" name="agency.id" from="${agencies}"
			optionKey="id" optionValue="description" required=""
			value="${driverInstance?.agency?.id}" class="many-to-one" />
		<span class="help-inline"> ${hasErrors(bean: driverInstance, field: 'agency', 'error')}
		</span>
	</div>
</div>



<div
	class="control-group fieldcontain ${hasErrors(bean: driverInstance, field: 'birthDate', 'error')} required">
	<label for="birthDate" class="control-label"><g:message
			code="client.birthDate.label" default="Birth Date" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<bs:datePicker required="" name="birthDate" precision="day"
			value="${driverInstance?.person?.birthDate}" />
		<span class="help-inline">
			${hasErrors(bean: driverInstance, field: 'birthDate', 'error')}
		</span>
	</div>
</div>


<div
	class="control-group fieldcontain ${hasErrors(bean: driverInstance, field: 'document', 'error')} ">
	<label for="document" class="control-label"><g:message
			code="client.document.label" default="Document" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField required="" name="document"
			value="${driverInstance?.person?.document}" />
		<span class="help-inline">
			${hasErrors(bean: driverInstance, field: 'document', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: driverInstance, field: 'email', 'error')} ">
	<label for="email" class="control-label"><g:message
			code="client.email.label" default="Email" /></label>
	<div class="controls">
		<g:field type="email" name="email" value="${driverInstance?.person?.email}" />
		<span class="help-inline">
			${hasErrors(bean: driverInstance, field: 'email', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: driverInstance, field: 'firstName', 'error')} ">
	<label for="firstName" class="control-label"><g:message
			code="client.firstName.label" default="First Name" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField required="" name="firstName"
			value="${driverInstance?.person?.firstName}" />
		<span class="help-inline">
			${hasErrors(bean: driverInstance, field: 'firstName', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: driverInstance, field: 'lastName', 'error')} ">
	<label for="lastName" class="control-label"><g:message
			code="client.lastName.label" default="Last Name" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField required="" name="lastName"
			value="${driverInstance?.person?.lastName}" />
		<span class="help-inline">
			${hasErrors(bean: driverInstance, field: 'lastName', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: driverInstance, field: 'sex', 'error')} ">
	<label for="sex" class="control-label"><g:message
			code="driver.sex.label" default="Sex" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:select id="sex" name="sex" from="${availableSex}" required=""
			value="${driverInstance?.person?.sex}" />
		<span class="help-inline"> ${hasErrors(bean: driverInstance, field: 'sex', 'error')}
		</span>
	</div>
</div>

