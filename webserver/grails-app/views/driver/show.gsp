
<%@ page import="webserver.Driver"%>
<!doctype html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="kickstart" />
<g:set var="entityName"
	value="${message(code: 'driver.label', default: 'Driver')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

	<section id="show-driver" class="first">

		<table class="table">
			<tbody>


				<tr class="prop">
					<td valign="top" class="name"><g:message
							code="driver.agency.label" default="Agency" /></td>

					<td valign="top" class="value">
						${fieldValue(bean: driverInstance, field: "agency.description")}
					</td>

				</tr>

				<tr class="prop">
					<td valign="top" class="name"><g:message
							code="driver.person.birthDate.label" default="Birth Date" /></td>

					<td valign="top" class="value"><g:formatDate
							date="${driverInstance?.person.birthDate}" /></td>

				</tr>


				<tr class="prop">
					<td valign="top" class="name"><g:message
							code="driver.person.document.label" default="Document" /></td>

					<td valign="top" class="value">
						${fieldValue(bean: driverInstance, field: "person.document")}
					</td>

				</tr>

				<tr class="prop">
					<td valign="top" class="name"><g:message
							code="driver.person.email.label" default="Email" /></td>

					<td valign="top" class="value">
						${fieldValue(bean: driverInstance, field: "person.email")}
					</td>

				</tr>

				<tr class="prop">
					<td valign="top" class="name"><g:message
							code="driver.personfirstName.label" default="First Name" /></td>

					<td valign="top" class="value">
						${fieldValue(bean: driverInstance, field: "person.firstName")}
					</td>

				</tr>

				<tr class="prop">
					<td valign="top" class="name"><g:message
							code="driver.person.lastName.label" default="Last Name" /></td>

					<td valign="top" class="value">
						${fieldValue(bean: driverInstance, field: "person.lastName")}
					</td>

				</tr>


				<tr class="prop">
					<td valign="top" class="name"><g:message
							code="driver.person.sex.label" default="Sex" /></td>

					<td valign="top" class="value">
						${fieldValue(bean: driverInstance, field: "person.sex")}
					</td>

				</tr>

			</tbody>
		</table>
	</section>

</body>

</html>
