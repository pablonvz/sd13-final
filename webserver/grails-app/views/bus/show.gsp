
<%@ page import="webserver.Bus" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'bus.label', default: 'Bus')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-bus" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="bus.id.label" default="Number" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: busInstance, field: "number")}</td>
				
			</tr>
			
			<tr class="prop">
				<td valign="top" class="name"><g:message code="bus.brand.label" default="Brand" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: busInstance, field: "brand")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="bus.capacty.label" default="Capacity" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: busInstance, field: "capacty")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="bus.fuel.label" default="Fuel" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: busInstance, field: "fuel")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="bus.model.label" default="Model" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: busInstance, field: "model")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="bus.motor.label" default="Motor" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: busInstance, field: "motor")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="bus.year.label" default="Year" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${busInstance?.year}" format="yyyy" /></td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
