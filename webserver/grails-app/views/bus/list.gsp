
<%@ page import="webserver.Bus" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'bus.label', default: 'Bus')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-bus" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
				<g:sortableColumn property="number" title="${message(code: 'bus.number.label', default: 'Number')}" />
			
				<g:sortableColumn property="brand" title="${message(code: 'bus.brand.label', default: 'Brand')}" />
			
				<g:sortableColumn property="capacty" title="${message(code: 'bus.capacty.label', default: 'Capacity')}" />
			
				<g:sortableColumn property="fuel" title="${message(code: 'bus.fuel.label', default: 'Fuel')}" />
			
				<g:sortableColumn property="model" title="${message(code: 'bus.model.label', default: 'Model')}" />
			
				<g:sortableColumn property="motor" title="${message(code: 'bus.motor.label', default: 'Motor')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${busInstanceList}" status="i" var="busInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${busInstance.id}">${fieldValue(bean: busInstance, field: "number")}</g:link></td>
				
				<td>${fieldValue(bean: busInstance, field: "brand")}</td>
			
				<td>${fieldValue(bean: busInstance, field: "capacty")}</td>
			
				<td>${fieldValue(bean: busInstance, field: "fuel")}</td>
			
				<td>${fieldValue(bean: busInstance, field: "model")}</td>
			
				<td>${fieldValue(bean: busInstance, field: "motor")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${busInstanceTotal}" />
	</div>
</section>

</body>

</html>
