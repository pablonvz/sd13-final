<%@ page import="webserver.Bus"%>

<div
	class="control-group fieldcontain ${hasErrors(bean: busInstance, field: 'number', 'error')} ">
	<label for="brand" class="control-label"><g:message
			code="bus.brand.label" default="Number" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField name="number" value="${busInstance?.number}" required="" min="1" type="number"/>
		<span class="help-inline">
			${hasErrors(bean: busInstance, field: 'number', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: busInstance, field: 'brand', 'error')} ">
	<label for="brand" class="control-label"><g:message
			code="bus.brand.label" default="Brand" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField name="brand" value="${busInstance?.brand}" required="" minlength="2" type="text"/>
		<span class="help-inline">
			${hasErrors(bean: busInstance, field: 'brand', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: busInstance, field: 'capacty', 'error')} required">
	<label for="capacty" class="control-label"><g:message
			code="bus.capacty.label" default="Capacity" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:field type="number" name="capacty" required="" min="2"
			value="${busInstance.capacty}" />
		<span class="help-inline">
			${hasErrors(bean: busInstance, field: 'capacty', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: busInstance, field: 'fuel', 'error')} ">
	<label for="fuel" class="control-label"><g:message
			code="bus.fuel.label" default="Fuel" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:select id="fuel" name="fuel" from="${availableFuel}" required=""
			value="${busInstance?.fuel}" />
		<span class="help-inline"> ${hasErrors(bean: busInstance, field: 'fuel', 'error')}
		</span>
	</div>
</div>


<div
	class="control-group fieldcontain ${hasErrors(bean: busInstance, field: 'model', 'error')} ">
	<label for="model" class="control-label"><g:message
			code="bus.model.label" default="Model" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField name="model" value="${busInstance?.model}"  required="" minlength="1" type="text" />
		<span class="help-inline">
			${hasErrors(bean: busInstance, field: 'model', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: busInstance, field: 'motor', 'error')} ">
	<label for="motor" class="control-label"><g:message
			code="bus.motor.label" default="Motor" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField name="motor" value="${busInstance?.motor}"  required="" minlength="2" type="text"/>
		<span class="help-inline">
			${hasErrors(bean: busInstance, field: 'motor', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: busInstance, field: 'year', 'error')} required">
	<label for="year" class="control-label"><g:message
			code="bus.year.label" default="Year" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<bs:datePicker name="year" precision="day"
			value="${busInstance?.year}" />
		<span class="help-inline">
			${hasErrors(bean: busInstance, field: 'year', 'error')}
		</span>
	</div>
</div>

