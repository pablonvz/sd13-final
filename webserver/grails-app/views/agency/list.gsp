
<%@ page import="webserver.Agency" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'agency.label', default: 'Agency')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-agency" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="description" title="${message(code: 'agency.description.label', default: 'Description')}" />
			
				<th>${message(code: 'agency.city.label', default: 'City')}</th>
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${agencyInstanceList}" status="i" var="agencyInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${agencyInstance.id}">${fieldValue(bean: agencyInstance, field: "description")}</g:link></td>
			
				<td>${agencyInstance.city?.name+' - '+agencyInstance.city?.state?.name+' - '+agencyInstance.city?.state?.country?.name}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${agencyInstanceTotal}" />
	</div>
</section>

</body>

</html>
