<%@ page import="webserver.Agency" %>



			<div class="control-group fieldcontain ${hasErrors(bean: agencyInstance, field: 'city', 'error')} required">
				<label for="city" class="control-label"><g:message code="agency.city.label" default="City" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="city" name="city.id"
						from="${cities}" optionKey="id" required="" optionValue="name"
							value="${agencyInstance?.city?.id}" class="many-to-one" />
					<span class="help-inline">${hasErrors(bean: agencyInstance, field: 'city', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: agencyInstance, field: 'description', 'error')} required">
				<label for="description" class="control-label"><g:message code="agency.description.label" default="Description" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="description" value="${agencyInstance?.description}"  required="" minlength="2" type="text" />
					<span class="help-inline">${hasErrors(bean: agencyInstance, field: 'description', 'error')}</span>
				</div>
			</div>

