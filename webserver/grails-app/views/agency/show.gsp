
<%@ page import="webserver.Agency" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'agency.label', default: 'Agency')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-agency" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="agency.city.label" default="City" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: agencyInstance, field: "city.name")}</td>
				
			</tr>
		
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="agency.description.label" default="Description" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: agencyInstance, field: "description")}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
