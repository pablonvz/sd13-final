
<%@ page import="webserver.Report" %>


<g:javascript>
$("#printBtn").on('click', function() {
   
	    var openWindow = window.open("", "title", "attributes");
	    openWindow.document.write("<br/>" + $('#list-report')[0].innerHTML);
	    openWindow.document.close();
	    openWindow.focus();
	    openWindow.print();
	    openWindow.close();

});

	

</g:javascript>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'report.label', default: 'Report')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-report" class="first">
	<h3>${option}</h3>
	<table  class="table table-bordered margin-top-medium">
		<thead>
			<tr>
				<th>City</th>
				<th>Quantity</th>			
			</tr>
		</thead>

			<tbody >
			<g:each in="${cities}" status="i" var="city">
				<g:if test="${city != null &&  counts.size() > i && counts[i] != -1}">
					<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					
						<td>${city.name }</td>
					
						<td>${counts[i]}</td>
					
					</tr>
				</g:if>
			</g:each>
			</tbody>
	</table>
	
</section>
<g:link  class="btn btn-primary" action="create" >Report</g:link>
<button id="printBtn" class="btn btn-primary" type="button" > <g:message code="default.button.print.label" default="Print" /></button>
</body>

</html>
