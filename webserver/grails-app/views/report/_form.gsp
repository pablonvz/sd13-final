<%@ page import="webserver.Report" %>

<g:javascript>
	$("#option").on('change', function() {
			if($(this).val()!="2"){
	  			$("#cityDiv").slideUp();
			}else{
				$("#cityDiv").slideDown();
			}
	});

	$('[type="reset"].btn').click(function() {
			$("#cityDiv").slideUp();
			
	});
	
	$.validator.addMethod(
	    "greaterOrEqualThan",
	    function(value, element, params) {
	        var target = $(params).val();
	        var isValueNumeric = !isNaN(parseFloat(value)) && isFinite(value);
	        var isTargetNumeric = !isNaN(parseFloat(target)) && isFinite(target);
	        if (isValueNumeric && isTargetNumeric) {
	            return Number(value) >= Number(target);
	        }
	
	        if (!/Invalid|NaN/.test(new Date(value))) {
	            return new Date(value) >= new Date(target);
	        }
	
	        return false;
	    },
	    function(tg){
			var iVal = $(tg).val();
	    	return $('<span style="font-weight:normal" for="endKm" generated="true" class="alert-danger">Please enter a value greater than or equal to '+iVal+'.</span>')//);
	    });
	$("#finalDate").parents("form").validate({
	    rules: {
	        finalDate: { greaterOrEqualThan: "#initialDate" }
	    }
	});
		
</g:javascript>


			<div class="control-group fieldcontain ${hasErrors(bean: reportInstance, field: 'initialDate', 'error')} required">
				<label for="initialDate" class="control-label"><g:message code="report.initialDate.label" default="Initial Date" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<bs:datePicker name="initialDate" id="initialDate" precision="day"  value="${reportInstance?.initialDate}"  />
					<span class="help-inline">${hasErrors(bean: reportInstance, field: 'initialDate', 'error')}</span>
				</div>
			</div>
			
			<div class="control-group fieldcontain ${hasErrors(bean: reportInstance, field: 'finalDate', 'error')} required">
				<label for="finalDate" class="control-label"><g:message code="report.finalDate.label" default="Final Date" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<bs:datePicker name="finalDate" id="finalDate" precision="day"  value="${reportInstance?.finalDate}"  />
					<span class="help-inline">${hasErrors(bean: reportInstance, field: 'finalDate', 'error')}</span>
				</div>
			</div>
			<div
				class="control-group fieldcontain ">
				<label for="reports" class="control-label"><g:message
						code="report.option.label" default="Option" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="option" name="option" from="${availableOptions}" required=""
						value="_value" optionKey="_key" optionValue="_value"/>
				</div>
			</div>
			
			<div id="cityDiv" style="display: none" class="control-group fieldcontain ${hasErrors(bean: reportInstance, field: 'cityId', 'error')} required">
				<label for="cityId" class="control-label"><g:message code="report.cityId.label" default="City" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="city" name="city.id"
						from="${cities}" optionKey="id" required="" 
						optionValue="name" value="${reportInstance?.cityId}"/>
					<span class="help-inline">${hasErrors(bean: reportInstance, field: 'cityId', 'error')}</span>
				</div>
			</div>

