
<%@ page import="webserver.Report" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'report.label', default: 'Report')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-report" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="report.cityId.label" default="City Id" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: reportInstance, field: "cityId")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="report.finalDate.label" default="Final Date" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${reportInstance?.finalDate}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="report.initialDate.label" default="Initial Date" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${reportInstance?.initialDate}" /></td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
