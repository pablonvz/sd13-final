
<%@ page import="webserver.User" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-user" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<th>${message(code: 'user.agency.label', default: 'Agency')}</th>
			
				<th>${message(code: 'user.person.birthDate.label', default: 'Birth Date')}</th>
			
				<th>${message(code: 'user.person.document.label', default: 'Document')}</th>
			
				<th>${message(code: 'user.person.email.label', default: 'Email')}</th>
			
				<th>${message(code: 'user.person.firstName.label', default: 'First Name')}</th>
				
				<th>${message(code: 'user.person.lastName.label', default: 'Last Name')}</th>
			
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${userInstanceList}" status="i" var="userInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${userInstance.id}">${fieldValue(bean: userInstance, field: "agency.description")}</g:link></td>
			
				<td><g:formatDate date="${userInstance.person.birthDate}" /></td>
			
				<%--<td><g:formatDate date="${userInstance.deletedAt}" /></td>
			
				--%><td>${fieldValue(bean: userInstance, field: "person.document")}</td>
			
				<td>${fieldValue(bean: userInstance, field: "person.email")}</td>
			
				<td>${fieldValue(bean: userInstance, field: "person.firstName")}</td>
				
				<td>${fieldValue(bean: userInstance, field: "person.lastName")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${userInstanceTotal}" />
	</div>
</section>

</body>

</html>
