<%@ page import="webserver.User"%>
<div
	class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'agency', 'error')} required">
	<label for="agency" class="control-label"><g:message
			code="claim.agency.label" default="agency" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:select id="agency" name="agency.id" from="${agencies}"
			optionKey="id" optionValue="description" required=""
			value="${userInstance?.agency?.id}" class="many-to-one" />
		<span class="help-inline"> ${hasErrors(bean: userInstance, field: 'agency', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'birthDate', 'error')} required">
	<label for="birthDate" class="control-label"><g:message
			code="user.birthDate.label" default="Birth Date" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<bs:datePicker required="" name="birthDate" precision="day"
			value="${userInstance?.person?.birthDate}" />
		<span class="help-inline"> ${hasErrors(bean: userInstance, field: 'birthDate', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'document', 'error')} ">
	<label for="document" class="control-label"><g:message
			code="user.document.label" default="Document" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField required="" name="document" value="${userInstance?.person?.document}" />
		<span class="help-inline"> ${hasErrors(bean: userInstance, field: 'document', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'email', 'error')} ">
	<label for="email" class="control-label"><g:message
			code="user.email.label" default="Email" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:field required="" type="email" name="email" value="${userInstance?.person?.email}" />
		<span class="help-inline"> ${hasErrors(bean: userInstance, field: 'email', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'firstName', 'error')} ">
	<label for="firstName" class="control-label"><g:message
			code="user.firstName.label" default="First Name" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField required="" name="firstName"
			value="${userInstance?.person?.firstName}" />
		<span class="help-inline"> ${hasErrors(bean: userInstance, field: 'firstName', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'lastName', 'error')} ">
	<label for="lastName" class="control-label"><g:message
			code="user.lastName.label" default="Last Name" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:textField name="lastName" required="" value="${userInstance?.person?.lastName}" />
		<span class="help-inline"> ${hasErrors(bean: userInstance, field: 'lastName', 'error')}
		</span>
	</div>
</div>

<div
	class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'passwordHash', 'error')} ">
	<label for="passwordHash" class="control-label"><g:message
			code="user.passwordHash.label" default="Password" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:passwordField name="passwordHash" required="" value="" />
		<span class="help-inline">
			${hasErrors(bean: userInstance, field: 'passwordHash', 'error')}
		</span>
	</div>
</div>


<div
	class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'sex', 'error')} ">
	<label for="sex" class="control-label"><g:message
			code="user.sex.label" default="Sex" /><span
		class="required-indicator">*</span></label>
	<div class="controls">
		<g:select id="sex" name="sex" from="${availableSex}" required=""
			value="${userInstance?.person?.sex}" />
		<span class="help-inline"> ${hasErrors(bean: userInstance, field: 'sex', 'error')}
		</span>
	</div>
</div>

