
<%@ page import="webserver.User" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-user" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="user.agencyId.label" default="Agency" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: userInstance, field: "agency.description")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="user.person.birthDate.label" default="Birth Date" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${userInstance?.person.birthDate}" /></td>
				
			</tr>
		
			<%--<tr class="prop">
				<td valign="top" class="name"><g:message code="user.deletedAt.label" default="Deleted At" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${userInstance?.deletedAt}" /></td>
				
			</tr>
		
			--%><tr class="prop">
				<td valign="top" class="name"><g:message code="user.person.document.label" default="Document" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: userInstance, field: "person.document")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="user.person.email.label" default="Email" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: userInstance, field: "person.email")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="user.person.firstName.label" default="First Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: userInstance, field: "person.firstName")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="user.person.lastName.label" default="Last Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: userInstance, field: "person.lastName")}</td>
				
			</tr>

			<tr class="prop">
				<td valign="top" class="name"><g:message code="user.person.sex.label" default="Sex" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: userInstance, field: "person.sex")}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
