package webserver

import grails.converters.JSON

import java.util.Date;
import java.util.Calendar;

import com.buses.app.beans.ticket.TicketBean
import com.buses.app.services.client.IClientService
import com.buses.app.services.journey.IJourneyService
import com.buses.app.services.ticket.ITicketService

class TicketController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	static allowedStuses = ["reserved","paid","canceled"]
    def ITicketService ticketService
	def IClientService clientService
	def IJourneyService journeyService
	
    def index() {
        redirect(action: "list", params: params)
    }

	def freeSeats(){
		def freeSeats = ticketService.getFreeSeats(Integer.parseInt(params.journeyId));
		println freeSeats; 
		render freeSeats   as JSON 
	}
	
    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def fields = ["seat", "status"]
        def tickets = ticketService.search(BaseController.getParams(params, fields))
        [ticketInstanceList: tickets, ticketInstanceTotal: ticketService.count(BaseController.getCountParams(params, fields))]
    }

    def create() {
//        [ticketInstance: new Ticket(params), statuses: allowedStuses, clients: clientService.list(), journeys: journeyService.list()]
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		System.out.println("value="+cal.getTime().format("yyyy-MM-dd")+"&fields=date&like=false&criteria=>");
		def journeyList = journeyService.search("value="+cal.getTime().format("yyyy-MM-dd")+"&fields=date&like=false&criteria=>");
		[ticketInstance: new TicketBean(null, null, null, null, null, null), statuses: allowedStuses, clients: clientService.list(), journeys: journeyList]
    }

    def save() {
        def id = params.id == null ? null : Integer.parseInt(params.id)
        TicketBean ticket = new TicketBean(id, 
			clientService.getById(Integer.parseInt(params.clientId)),
			journeyService.getById(Integer.parseInt(params.journeyId)), 
			Integer.parseInt(params.seat), params.status,null)
		
        ticket = ticketService.save(ticket)
        redirect(action: "show", id: ticket.getId())
    }

    def show() {
        TicketBean ticket = ticketService.getById(Integer.parseInt(params.id))
        [ticketInstance: ticket]
    }

    def edit() {
        def ticketInstance = ticketService.getById(Integer.parseInt(params.id))
        if (!ticketInstance) {
            flash.message = message(code: 'default.not.found.message', args: [
                message(code: 'ticket.label', default: 'Ticket'),
                params.id
            ])
            redirect(action: "list")
            return
        }

        [ticketInstance: ticketInstance, statuses: allowedStuses, clients: clientService.list(), journeys: journeyService.list()]
    }

    def update() {
        def ticketInstance = ticketService.getById(Integer.parseInt(params.id))
        if (!ticketInstance) {
            flash.message = message(code: 'default.not.found.message', args: [
                message(code: 'ticket.label', default: 'Ticket'),
                params.id
            ])
            redirect(action: "list")
            return
        }
		
		/*
			Integer id, Integer client_id, Date date,
			Integer journey_id, Integer seat, String status, Date deleted_at
		*/
	    def id = params.id == null ? null : Integer.parseInt(params.id)
        TicketBean ticket = new TicketBean(id, 
			clientService.getById(Integer.parseInt(params.clientId)), 
			journeyService.getById(Integer.parseInt(params.journeyId)), 
			Integer.parseInt(params.seat), params.status,null)
		
        ticket = ticketService.save(ticket)
        redirect(action: "show", id: ticket.getId())
    }

    def delete() {
        ticketService.delete(Integer.parseInt(params.id))
        flash.message = message(code: 'default.deleted.message', args: [
            message(code: 'ticket.label', default: 'Ticket'),
            params.id
        ])
        redirect(action: "list")
    }
}

