package webserver

import com.buses.app.beans.country.CountryBean
import com.buses.app.services.country.ICountryService

class CountryController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def ICountryService countryService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def fields = ["code","name"]
		def countries = countryService.search(BaseController.getParams(params, fields));
		[countryInstanceList: countries,  countryInstanceTotal: countryService.count(BaseController.getCountParams(params, fields)) ]
	}

	def create() {
		[countryInstance: new Country(params)]
	}

	def save() {
		def id = params.id == null ? null : Integer.parseInt(params.id)
		CountryBean country = new CountryBean(id,
				, params.code, params.name, params.deleted_at            )
		country = countryService.save(country)
		redirect(action: "show", id: country.getId())
	}

	def show() {
		CountryBean country = countryService.getById(Integer.parseInt(params.id))
		[countryInstance: country]
	}

	def edit() {
		def countryInstance = countryService.getById(Integer.parseInt(params.id))
		if (!countryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'country.label', default: 'Country'),
				params.id
			])
			redirect(action: "list")
			return
		}

		[countryInstance: countryInstance]
	}

	def update() {
		def countryInstance = countryService.getById(Integer.parseInt(params.id))
		if (!countryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'country.label', default: 'Country'),
				params.id
			])
			redirect(action: "list")
			return
		}

		def id = params.id == null ? null : Integer.parseInt(params.id)
		CountryBean country = new CountryBean(id,
				, params.code, params.name, params.deleted_at            )
		country = countryService.save(country)
		redirect(action: "show", id: country.getId())
	}

	def delete() {
		countryService.delete(Integer.parseInt(params.id))
		flash.message = message(code: 'default.deleted.message', args: [
			message(code: 'country.label', default: 'Country'),
			params.id
		])
		redirect(action: "list")
	}
}

