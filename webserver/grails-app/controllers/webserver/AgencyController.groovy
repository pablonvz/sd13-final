package webserver

import com.buses.app.beans.agency.AgencyBean
import com.buses.app.services.agency.IAgencyService
import com.buses.app.services.city.ICityService

class AgencyController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def IAgencyService agencyService
	def ICityService cityService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def fields = ["description"]
		def agencies = agencyService.search(BaseController.getParams(params, fields))
		[agencyInstanceList: agencies, agencyInstanceTotal: agencyService.count(BaseController.getCountParams(params, fields))  ]
	}

	def create() {
		def cities = cityService.list()
		[agencyInstance: new AgencyBean(null, null, null, null), cities: cityService.list()]
	}


	def save() {
		def id = params.id == null ? null : Integer.parseInt(params.id)
		AgencyBean agency = new AgencyBean(id,
				, cityService.getById(Integer.parseInt(params.city.id)), params.description, params.deleted_at            )
		agency = agencyService.save(agency)
		redirect(action: "show", id: agency.getId())
	}


	def show() {
		AgencyBean agency = agencyService.getById(Integer.parseInt(params.id))
		[agencyInstance: agency]
	}

	def edit() {
		def agencyInstance = agencyService.getById(Integer.parseInt(params.id))
		if (!agencyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'agency.label', default: 'Agency'),
				params.id
			])
			redirect(action: "list")
			return
		}

		[agencyInstance: agencyInstance, cities: cityService.list()]
	}

	def update() {
		def agencyInstance = agencyService.getById(Integer.parseInt(params.id))
		if (!agencyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'agency.label', default: 'Agency'),
				params.id
			])
			redirect(action: "list")
			return
		}

		def id = params.id == null ? null : Integer.parseInt(params.id)
		AgencyBean agency = new AgencyBean(id,
				, cityService.getById(Integer.parseInt(params.city.id)), params.description, params.deleted_at            )
		agency = agencyService.save(agency)
		redirect(action: "show", id: agency.getId())
	}
	def delete() {
		agencyService.delete(Integer.parseInt(params.id))
		flash.message = message(code: 'default.deleted.message', args: [
			message(code: 'agency.label', default: 'Agency'),
			params.id
		])
		redirect(action: "list")
	}
}

