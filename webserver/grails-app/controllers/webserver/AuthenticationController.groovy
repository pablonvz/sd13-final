package webserver

import com.buses.app.beans.user.UserBean;
import com.buses.app.services.user.IUserService;


/**
 * AuthenticationController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class AuthenticationController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
    def IUserService userService
	
    def index() {
        redirect(action: "login", params: params)
    }

//    def list() {
//        params.max = Math.min(params.max ? params.int('max') : 10, 100)
//        [authenticationInstanceList: Authentication.list(params), authenticationInstanceTotal: Authentication.count()]
//    }

    def login() {
        [authenticationInstance: new Authentication(params)]
    }

    def save() {
		List<UserBean> usrs = userService.getByEmail(params.email);
		def authenticationInstance = params;
        if( (!usrs.isEmpty()) && userService.isValidPassword(usrs.get(0).getPasswordHash(), params.password)) {
        	session["user"] = params.email;
        	//flash.message = message(code: 'default.created.message', args: [message(code: 'authentication.label', default: 'Authentication'), authenticationInstance.id])
        	redirect(controller: "agency" ,action: "list", id: authenticationInstance.id)
        }else{
        	render(view: "login", model: [authenticationInstance: authenticationInstance, errorMessage: "Invalid email and password combination"])
        	return
        }
    }
	
	def logout() {
		//log.info "User agent: " + request.getHeader("User-Agent")
		session.invalidate()
		redirect(action: "login")
	}

//    def show() {
//        def authenticationInstance = Authentication.get(params.id)
//        if (!authenticationInstance) {
//			flash.message = message(code: 'default.not.found.message', args: [message(code: 'authentication.label', default: 'Authentication'), params.id])
//            redirect(action: "list")
//            return
//        }
//
//        [authenticationInstance: authenticationInstance]
//    }
//
//    def edit() {
//        def authenticationInstance = Authentication.get(params.id)
//        if (!authenticationInstance) {
//            flash.message = message(code: 'default.not.found.message', args: [message(code: 'authentication.label', default: 'Authentication'), params.id])
//            redirect(action: "list")
//            return
//        }
//
//        [authenticationInstance: authenticationInstance]
//    }
//
//    def update() {
//        def authenticationInstance = Authentication.get(params.id)
//        if (!authenticationInstance) {
//            flash.message = message(code: 'default.not.found.message', args: [message(code: 'authentication.label', default: 'Authentication'), params.id])
//            redirect(action: "list")
//            return
//        }
//
//        if (params.version) {
//            def version = params.version.toLong()
//            if (authenticationInstance.version > version) {
//                authenticationInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
//                          [message(code: 'authentication.label', default: 'Authentication')] as Object[],
//                          "Another user has updated this Authentication while you were editing")
//                render(view: "edit", model: [authenticationInstance: authenticationInstance])
//                return
//            }
//        }
//
//        authenticationInstance.properties = params
//
//        if (!authenticationInstance.save(flush: true)) {
//            render(view: "edit", model: [authenticationInstance: authenticationInstance])
//            return
//        }
//
//		flash.message = message(code: 'default.updated.message', args: [message(code: 'authentication.label', default: 'Authentication'), authenticationInstance.id])
//        redirect(action: "show", id: authenticationInstance.id)
//    }
//
//    def delete() {
//        def authenticationInstance = Authentication.get(params.id)
//        if (!authenticationInstance) {
//			flash.message = message(code: 'default.not.found.message', args: [message(code: 'authentication.label', default: 'Authentication'), params.id])
//            redirect(action: "list")
//            return
//        }
//
//        try {
//            authenticationInstance.delete(flush: true)
//			flash.message = message(code: 'default.deleted.message', args: [message(code: 'authentication.label', default: 'Authentication'), params.id])
//            redirect(action: "list")
//        }
//        catch (DataIntegrityViolationException e) {
//			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'authentication.label', default: 'Authentication'), params.id])
//            redirect(action: "show", id: params.id)
//        }
//    }
}
