package webserver

import com.buses.app.beans.agency.AgencyBean
import com.buses.app.services.agency.IAgencyService
import com.buses.app.services.city.ICityService

class BaseController {
	
	def static getParams(prms, fields){
		prms.remove("controller");
		prms.remove("action");
		def extra = "";
		if(prms.value == null || prms.value.toString().isEmpty() /*|| fields.isEmpty()*/){
			//prms.remove("value");
			prms = prms.findAll{ !it.key.equalsIgnoreCase("value") }
		}else if(!fields.isEmpty()){
			extra += "&fields="+(fields.join(","));
		}
		return prms.collect { k,v -> "$k=$v" }.join('&').toString() + extra;
	}
	
	def static getCountParams(prms, fields){
		prms.remove("controller");
		prms.remove("action");
		def extra = "";
		if(prms.value == null || prms.value.toString().isEmpty() /*|| fields.isEmpty()*/){ 
			prms.remove("value"); 
		}else if(!fields.isEmpty()){
			extra += "&fields="+(fields.join(","));
		}
		def rp = prms.findAll { !(it.key.equalsIgnoreCase("max") || it.key.equalsIgnoreCase("offset")) }.collect { k,v -> "$k=$v" }.join('&').toString() + extra;
		return rp.isEmpty() ? "null" : rp;
	}
	
}

