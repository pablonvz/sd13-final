package webserver

import com.buses.app.beans.bus.BusBean
import com.buses.app.services.bus.IBusService

class BusController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	static availableFuel = ["diesel","gasoil","biodiesel","alcohol","flex alcohol","electric"]
	
	def IBusService busService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def fields = ["number",
			"brand","capacty","fuel", "model","motor","year"];
		def buses = busService.search(BaseController.getParams(params, fields))
		[busInstanceList: buses, busInstanceTotal: busService.count(BaseController.getCountParams(params, fields)) ]
	}

	def create() {
		[availableFuel: availableFuel, busInstance: new Bus(params)]
	}

	def save() {
		def id = params.id == null ? null : Integer.parseInt(params.id)
		BusBean bus = new BusBean(id, Integer.parseInt(params.number)
				, Integer.parseInt(params.capacty), params.model, params.fuel, params.brand, Date.parse("yyyy-mm-dd", params.year), params.motor, params.deleted_at            )
		bus = busService.save(bus)
		redirect(action: "show", id: bus.getId())
	}

	def show() {
		BusBean bus = busService.getById(Integer.parseInt(params.id))
		[busInstance: bus]
	}

	def edit() {
		def busInstance = busService.getById(Integer.parseInt(params.id))
		if (!busInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'bus.label', default: 'Bus'),
				params.id
			])
			redirect(action: "list")
			return
		}

		[availableFuel: availableFuel, busInstance: busInstance]
	}

	def update() {
		def busInstance = busService.getById(Integer.parseInt(params.id))
		if (!busInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'bus.label', default: 'Bus'),
				params.id
			])
			redirect(action: "list")
			return
		}

		BusBean bus = new BusBean(Integer.parseInt(params.id), Integer.parseInt(params.number)
				, Integer.parseInt(params.capacty), params.model, params.fuel, params.brand, Date.parse("yyyy-mm-dd", params.year), params.motor, params.deleted_at            )
		bus = busService.save(bus)
		redirect(action: "show", id: bus.getId())
	}

	def delete() {
		busService.delete(Integer.parseInt(params.id))
		flash.message = message(code: 'default.deleted.message', args: [
			message(code: 'bus.label', default: 'Bus'),
			params.id
		])
		redirect(action: "list")
	}
}

