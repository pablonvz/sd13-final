package webserver

import com.buses.app.beans.state.StateBean
import com.buses.app.services.country.ICountryService
import com.buses.app.services.state.IStateService

class StateController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def IStateService stateService
	def ICountryService countryService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def fields = ["name"]
		def states = stateService.search(BaseController.getParams(params, fields))
		[stateInstanceList: states, stateInstanceTotal: stateService.count(BaseController.getCountParams(params, fields))]
	}

	def create() {
		def countries = countryService.list()
		
		[stateInstance: new State(params), countries: countries]
	}

	def save() {
		def id = params.id == null ? null : Integer.parseInt(params.id)
		StateBean state = new StateBean(id, countryService.getById(Integer.parseInt(params.country.id)), params.name, null)
		
		state = stateService.save(state)
		redirect(action: "list")
	}

	def show() {
		StateBean state = stateService.getById(Integer.parseInt(params.id))
		[stateInstance: state]
	}

	def edit() {
		def stateInstance = stateService.getById(Integer.parseInt(params.id))
		if (!stateInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'state.label', default: 'State'),
				params.id
			])
			redirect(action: "list")
			return
		}
		
		def countries = countryService.list()

		[stateInstance: stateInstance, countries: countries]
	}

	def update() {
		def stateInstance = stateService.getById(Integer.parseInt(params.id))
		if (!stateInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'state.label', default: 'State'),
				params.id
			])
			redirect(action: "list")
			return
		}

		def id = params.id == null ? null : Integer.parseInt(params.id)
		StateBean state = new StateBean(id, countryService.getById(Integer.parseInt(params.country.id)), params.name, null)
		state = stateService.save(state)
		redirect(action: "show", id: state.getId())
	}

	def delete() {
		stateService.delete(Integer.parseInt(params.id))
		flash.message = message(code: 'default.deleted.message', args: [
			message(code: 'state.label', default: 'State'),
			params.id
		])
		redirect(action: "list")
	}
}

