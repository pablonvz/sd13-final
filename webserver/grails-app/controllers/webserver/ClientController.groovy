package webserver

import com.buses.app.beans.client.ClientBean
import com.buses.app.beans.person.PersonBean
import com.buses.app.services.client.IClientService
import com.buses.app.services.person.IPersonService

class ClientController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def IClientService clientService
	def IPersonService personService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def fields = []
		def clients = clientService.search(BaseController.getParams(params, fields))
		[clientInstanceList: clients, clientInstanceTotal: clientService.count(BaseController.getCountParams(params, fields)) ]
	}

	def create() {
		[availableSex: PersonController.availableSex,clientInstance: new ClientBean(null, params.deleted_at, new PersonBean(null, params.first_name, params.last_name, params.birth_date,params.sex, params.document, params.email, params.deleted_at))]
	}

	def save() {
		def id = params.id == null ? null : Integer.parseInt(params.id)
		def ClientBean client = id == null ?  new ClientBean(id, null, null) : clientService.getById(id)
		PersonBean p = personService.save(new PersonBean(
			client.getPerson() == null ? null : client.getPerson().getId(), params.firstName, params.lastName, 
			Date.parse("yyyy-mm-dd", params.birthDate), params.sex, params.document, params.email, params.deletedAt));

		def clientBean = new ClientBean(client.getId(), client.getDeletedAt(), p)
		client = clientService.save(clientBean);
		redirect(action: "show", id: client.getId())
	}

	def show() {
		ClientBean client = clientService.getById(Integer.parseInt(params.id))
		[clientInstance: client]
	}

	def edit() {
		def clientInstance = clientService.getById(Integer.parseInt(params.id))
		if (!clientInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'client.label', default: 'Client'),
				params.id
			])
			redirect(action: "list")
			return
		}

		[availableSex: PersonController.availableSex,clientInstance: clientInstance]
	}

	def update() {
		def clientInstance = clientService.getById(Integer.parseInt(params.id))
		if (!clientInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'client.label', default: 'Client'),
				params.id
			])
			redirect(action: "list")
			return
		}
		def id = params.id == null ? null : Integer.parseInt(params.id)
		def ClientBean client = clientService.getById(id)
		PersonBean p = personService.save(new PersonBean(client.getPerson().getId(), params.firstName, params.lastName,
			Date.parse("yyyy-mm-dd", params.birthDate), params.sex, params.document, params.email, params.deletedAt));
		client = clientService.save(new ClientBean(client.getId(), client.getDeletedAt(), p));
		redirect(action: "show", id: client.getId())
	}


	def delete() {
		clientService.delete(Integer.parseInt(params.id))
		flash.message = message(code: 'default.deleted.message', args: [
			message(code: 'client.label', default: 'Client'),
			params.id
		])
		redirect(action: "list")
	}
}

