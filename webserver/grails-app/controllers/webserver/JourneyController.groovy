package webserver

import com.buses.app.beans.journey.JourneyBean
import com.buses.app.services.bus.IBusService
import com.buses.app.services.driver.IDriverService
import com.buses.app.services.itinerary.IItineraryService
import com.buses.app.services.journey.IJourneyService

class JourneyController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def IJourneyService journeyService
	def IBusService busService
	def IItineraryService itineraryService
	def IDriverService driverService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def fields = ["endKm","incident","initialKm","price"];
		def journeys = journeyService.search(BaseController.getParams(params, fields))
		[journeyInstanceList: journeys, journeyInstanceTotal: journeyService.count(BaseController.getCountParams(params, fields))]
	}

	def create() {
		[journeyInstance: new JourneyBean(null, null, null, null, null, null, null, null, null, null), buses: busService.list(), drivers: driverService.list(), itineraries: itineraryService.list()]
	}

	def save() {
		def id = params.id == null ? null : Integer.parseInt(params.id)
		def endKmAux = params.endKm == null || params.endKm.isEmpty() ? 0 : Double.parseDouble(params.endKm)
		JourneyBean journey = new JourneyBean(id,
				itineraryService.getById(Integer.parseInt(params.itinerary_id)), 
				Date.parse("yyyy-MM-dd", params.date),
				busService.getById(Integer.parseInt(params.bus_id)),
				driverService.getById(Integer.parseInt(params.driver_id)),
				Integer.parseInt(params.price), 
				Double.parseDouble(params.initialKm), 
				endKmAux, params.incident, null )
		journey = journeyService.save(journey)
		redirect(action: "show", id: journey.getId())
	}

	def show() {
		JourneyBean journey = journeyService.getById(Integer.parseInt(params.id))
		[journeyInstance: journey]
	}

	def edit() {
		def journeyInstance = journeyService.getById(Integer.parseInt(params.id))
		if (!journeyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'journey.label', default: 'Journey'),
				params.id
			])
			redirect(action: "list")
			return
		}

		[journeyInstance: journeyInstance, buses: busService.list(), drivers: driverService.list(), itineraries: itineraryService.list()]
	}

	def update() {
		def journeyInstance = journeyService.getById(Integer.parseInt(params.id))
		if (!journeyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'journey.label', default: 'Journey'),
				params.id
			])
			redirect(action: "list")
			return
		}
		def id = params.id == null ? null : Integer.parseInt(params.id)
		def endKmAux = params.endKm == null || params.endKm.isEmpty() ? 0 : Double.parseDouble(params.endKm)

			JourneyBean journey = new JourneyBean(id,
				itineraryService.getById(Integer.parseInt(params.itinerary_id)), 
				Date.parse("yyyy-MM-dd", params.date),
				busService.getById(Integer.parseInt(params.bus_id)),
				driverService.getById(Integer.parseInt(params.driver_id)),
				Integer.parseInt(params.price), 
				Double.parseDouble(params.initialKm), 
				endKmAux, params.incident, null )
		journey = journeyService.save(journey)
		redirect(action: "show", id: journey.getId())
	}

	def delete() {
		journeyService.delete(Integer.parseInt(params.id))
		flash.message = message(code: 'default.deleted.message', args: [
			message(code: 'journey.label', default: 'Journey'),
			params.id
		])
		redirect(action: "list")
	}
}

