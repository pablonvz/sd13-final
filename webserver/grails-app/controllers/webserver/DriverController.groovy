package webserver

import com.buses.app.beans.agency.AgencyBean
import com.buses.app.beans.driver.DriverBean
import com.buses.app.beans.person.PersonBean
import com.buses.app.services.agency.IAgencyService
import com.buses.app.services.driver.IDriverService
import com.buses.app.services.person.IPersonService

class DriverController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def IDriverService driverService
	def IAgencyService agencyService
	def IPersonService personService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def fields = []
		def drivers = driverService.search(BaseController.getParams(params, fields))
		[driverInstanceList: drivers, driverInstanceTotal: driverService.count(BaseController.getCountParams(params, fields))]
	}

	def create() {
		[availableSex: PersonController.availableSex,driverInstance: new DriverBean(null, null, null, null), agencies: agencyService.list()]
	}

	def save() {
		def id = params.id == null ? null : Integer.parseInt(params.id)
		def DriverBean oldDriver = null == id ? null : driverService.getById(id)
		def PersonBean personBean = new PersonBean(id  == null ? null : oldDriver.getPerson().getId(),
			params.firstName, params.lastName, 
			Date.parse("yyyy-mm-dd", params.birthDate), params.sex,
			 params.document, params.email, params.deletedAt) 
		personBean = personService.save(personBean)

		def AgencyBean agencyBean = agencyService.getById(Integer.parseInt(params.agency.id))
		def DriverBean driver = new DriverBean(id,
				, agencyBean, personBean, params.deleted_at            )
		driver = driverService.save(driver)
		redirect(action: "show", id: driver.getId())
	}

	def show() {
		DriverBean driver = driverService.getById(Integer.parseInt(params.id))
		[driverInstance: driver]
	}

	def edit() {
		def driverInstance = driverService.getById(Integer.parseInt(params.id))
		if (!driverInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'driver.label', default: 'Driver'),
				params.id
			])
			redirect(action: "list")
			return
		}

		[availableSex: PersonController.availableSex,driverInstance: driverInstance, agencies: agencyService.list()]
	}

	def update() {
		def driverInstance = driverService.getById(Integer.parseInt(params.id))
		if (!driverInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'driver.label', default: 'Driver'),
				params.id
			])
			redirect(action: "list")
			return
		}

		
		def id = params.id == null ? null : Integer.parseInt(params.id)
		def DriverBean oldDriver = null == id ? null : driverService.getById(id)
		def PersonBean personBean = new PersonBean(id  == null ? null : oldDriver.getPerson().getId(),
			params.firstName, params.lastName, 
			Date.parse("yyyy-mm-dd", params.birthDate), params.sex,
			 params.document, params.email, params.deletedAt) 
		personService.save(personBean)
		def AgencyBean agencyBean = agencyService.getById(Integer.parseInt(params.agency.id))
		def DriverBean driver = new DriverBean(id,
				, agencyBean, personBean, params.deleted_at            )
		driver = driverService.save(driver)
		redirect(action: "show", id: driver.getId())
	}

	def delete() {
		driverService.delete(Integer.parseInt(params.id))
		flash.message = message(code: 'default.deleted.message', args: [
			message(code: 'driver.label', default: 'Driver'),
			params.id
		])
		redirect(action: "list")
	}
}

