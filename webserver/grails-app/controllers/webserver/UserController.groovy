package webserver

import com.buses.app.beans.agency.AgencyBean
import com.buses.app.beans.person.PersonBean
import com.buses.app.beans.user.UserBean
import com.buses.app.services.agency.IAgencyService;
import com.buses.app.services.person.IPersonService;
import com.buses.app.services.user.IUserService

class UserController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
    def IUserService userService
	def IAgencyService agencyService
	def IPersonService personService

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def fields = []
        def users = userService.search(BaseController.getParams(params, fields))
        [userInstanceList: users, userInstanceTotal: userService.count(BaseController.getCountParams(params, fields))]
    }

    def create() {
        [availableSex: PersonController.availableSex, userInstance: new UserBean(null, null, null, null, null),agencies: agencyService.list()]
    }

    def save() {
        def id = params.id == null ? null : Integer.parseInt(params.id)
        UserBean oldUser = null == id ? null : userService.getById(id)
		def PersonBean personBean = new PersonBean(id  == null ? null : oldUser.getPerson().getId(),
			params.firstName, params.lastName,
			Date.parse("yyyy-mm-dd", params.birthDate), params.sex,
			 params.document, params.email, params.deletedAt)
		personBean = personService.save(personBean)
		def AgencyBean agencyBean = agencyService.getById(Integer.parseInt(params.agency.id))
        def UserBean user = new UserBean(id,
				, agencyBean, personBean, params.passwordHash, params.deleted_at            )
		user = userService.save(user)
		redirect(action: "show", id: user.getId())
    }

    def show() {
        UserBean user = userService.getById(Integer.parseInt(params.id))
        [userInstance: user]
    }

    def edit() {
        def userInstance = userService.getById(Integer.parseInt(params.id))
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [
                message(code: 'user.label', default: 'User'),
                params.id
            ])
            redirect(action: "list")
            return
        }

        [availableSex: PersonController.availableSex, userInstance: userInstance, agencies: agencyService.list()]
    }

    def update() {
        def userInstance = userService.getById(Integer.parseInt(params.id))
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [
                message(code: 'user.label', default: 'User'),
                params.id
            ])
            redirect(action: "list")
            return
        }
		def id = params.id == null ? null : Integer.parseInt(params.id)
        def UserBean oldUser = null == id ? null : userService.getById(id)
		def PersonBean personBean = new PersonBean(id  == null ? null : oldUser.getPerson().getId(),
			params.firstName, params.lastName, 
			Date.parse("yyyy-mm-dd", params.birthDate), params.sex,
			 params.document, params.email, params.deletedAt) 
		personService.save(personBean)
		def AgencyBean agencyBean = agencyService.getById(Integer.parseInt(params.agency.id))
		def UserBean user = new UserBean(id,
				, agencyBean, personBean, params.passwordHash, params.deleted_at            )
		user = userService.save(user)
		redirect(action: "show", id: user.getId())
    }

    def delete() {
        userService.delete(Integer.parseInt(params.id))
        flash.message = message(code: 'default.deleted.message', args: [
            message(code: 'user.label', default: 'User'),
            params.id
        ])
        redirect(action: "list")
    }
}


