package webserver

import com.buses.app.beans.person.PersonBean
import com.buses.app.services.person.IPersonService

class PersonController {
	
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	static availableSex = ["male", "female"]
	
	def IPersonService personService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def fields = ["birthDate","document","email","firstName","lastName","sex"];
		def people = personService.search(BaseController.getParams(params, fields))
		[personInstanceList: people, personInstanceTotal: personService.count(BaseController.getCountParams(params, fields))]
	}

	def create() {
		[personInstance: new Person(params), availableSex: availableSex]
	}

	def save() {
		def id = params.id == null ? null : Integer.parseInt(params.id)
		PersonBean person = new PersonBean(id,
				, params.firstName, params.lastName, Date.parse("yyyy-MM-dd", params.birthDate), params.sex, params.document, params.email, params.deleted_at            )
		person = personService.save(person)
		redirect(action: "show", id: person.getId())
	}

	def show() {
		PersonBean person = personService.getById(Integer.parseInt(params.id))
		[personInstance: person]
	}

	def edit() {
		def personInstance = personService.getById(Integer.parseInt(params.id))
		if (!personInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'person.label', default: 'Person'),
				params.id
			])
			redirect(action: "list")
			return
		}

		[personInstance: personInstance, availableSex: availableSex]
	}

	def update() {
		def personInstance = personService.getById(Integer.parseInt(params.id))
		if (!personInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'person.label', default: 'Person'),
				params.id
			])
			redirect(action: "list")
			return
		}

		PersonBean person = new PersonBean(Integer.parseInt(params.id),
								, params.firstName, params.lastName, Date.parse("yyyy-MM-dd", params.birthDate), params.sex, params.document, params.email, params.deleted_at            )
		person = personService.save(person)
		redirect(action: "show", id: person.getId())
	}

	def delete() {
		personService.delete(Integer.parseInt(params.id))
		flash.message = message(code: 'default.deleted.message', args: [
			message(code: 'person.label', default: 'Person'),
			params.id
		])
		redirect(action: "list")
	}
}

