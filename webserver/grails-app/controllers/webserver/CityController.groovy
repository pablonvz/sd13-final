package webserver

import com.buses.app.beans.city.CityBean
import com.buses.app.services.city.ICityService
import com.buses.app.services.state.IStateService

class CityController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def ICityService cityService
	def IStateService stateService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def fields = ["name"]
		def cities = cityService.search(BaseController.getParams(params, fields))
		[cityInstanceList: cities, cityInstanceTotal: cityService.count(BaseController.getCountParams(params, fields)) ]
	}

	def create() {
		def states = stateService.list()
		[cityInstance: new CityBean(0, null, null, null), states: states]
	}

	def save() {
		def id = params.id == null ? null : Integer.parseInt(params.id)
		CityBean city = new CityBean(id, stateService.getById(Integer.parseInt(params.state.id)), params.name, params.deleted_at            )
		city = cityService.save(city)
		redirect(action: "list")
	}

	def show() {
		CityBean city = cityService.getById(Integer.parseInt(params.id))
		[cityInstance: city]
	}

	def edit() {
		def cityInstance = cityService.getById(Integer.parseInt(params.id))
		if (!cityInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'city.label', default: 'City'),
				params.id
			])
			redirect(action: "list")
			return
		}
		
		def states = stateService.list()

		[cityInstance: cityInstance, states: states]
	}

	def update() {
		def cityInstance = cityService.getById(Integer.parseInt(params.id))
		if (!cityInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'city.label', default: 'City'),
				params.id
			])
			redirect(action: "list")
			return
		}

		def id = params.id == null ? null : Integer.parseInt(params.id)
		CityBean city = new CityBean(id, stateService.getById(Integer.parseInt(params.state.id)), params.name, params.deleted_at            )
		city = cityService.save(city)
		redirect(action: "show", id: city.getId())
	}

	def delete() {
		cityService.delete(Integer.parseInt(params.id))
		flash.message = message(code: 'default.deleted.message', args: [
			message(code: 'city.label', default: 'City'),
			params.id
		])
		redirect(action: "list")
	}
}

