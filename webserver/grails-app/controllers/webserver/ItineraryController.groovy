package webserver

import com.buses.app.beans.itinerary.ItineraryBean
import com.buses.app.services.city.ICityService
import com.buses.app.services.itinerary.IItineraryService

class ItineraryController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def IItineraryService itineraryService
	def ICityService cityService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def fields = []
		def itineraries = itineraryService.search(BaseController.getParams(params, fields))
		[itineraryInstanceList: itineraries, itineraryInstanceTotal: itineraryService.count(BaseController.getCountParams(params, fields))]
	}

	def create() {
		def cities = cityService.list()
		[itineraryInstance: new Itinerary(params), cities: cities]
	}

	def save() {
		def id = params.id == null ? null : Integer.parseInt(params.id)
		ItineraryBean itinerary = new ItineraryBean(id,
				, cityService.getById(Integer.parseInt(params.origin.id)),cityService.getById(Integer.parseInt( params.destination.id)), params.deleted_at            )
		itinerary = itineraryService.save(itinerary)
		redirect(action: "show", id: itinerary.getId())
	}

	def show() {
		ItineraryBean itinerary = itineraryService.getById(Integer.parseInt(params.id))
		[itineraryInstance: itinerary]
	}

	def edit() {
		def itineraryInstance = itineraryService.getById(Integer.parseInt(params.id))
		def cities = cityService.list()
		if (!itineraryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'itinerary.label', default: 'Itinerary'),
				params.id
			])
			redirect(action: "list")
			return
		}

		[itineraryInstance: itineraryInstance, cities: cities]
	}

	def update() {
		def itineraryInstance = itineraryService.getById(Integer.parseInt(params.id))
		if (!itineraryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'itinerary.label', default: 'Itinerary'),
				params.id
			])
			redirect(action: "list")
			return
		}

		def id = params.id == null ? null : Integer.parseInt(params.id)
		ItineraryBean itinerary = new ItineraryBean(id,
				, cityService.getById(Integer.parseInt(params.origin.id)),cityService.getById(Integer.parseInt( params.destination.id)), params.deleted_at            )
		itinerary = itineraryService.save(itinerary)
		redirect(action: "show", id: itinerary.getId())
	}

	def delete() {
		itineraryService.delete(Integer.parseInt(params.id))
		flash.message = message(code: 'default.deleted.message', args: [
			message(code: 'itinerary.label', default: 'Itinerary'),
			params.id
		])
		redirect(action: "list")
	}
}

