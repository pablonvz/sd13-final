package webserver

import java.text.SimpleDateFormat

import com.buses.app.services.city.ICityService

/**
 * ReportController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class ReportController {
	
	static public class Options{
		String _value;
		String _key;
		Options(String value,String key){
			_value=value;
			_key=key;
		}
	}

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	static Options a=new Options("Most wanted destinations", "1");
	static Options b=new Options("Total amount of tickets sold", "2");
	static Options c=new Options("City where more tickets were sold", "3");
	static availableOptions = [a,b,c]
	def ICityService cityService

    def index() {
        redirect(action: "create", params: params)
    }

    def list() {
        //params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [cities: params.cities, counts: params.counts]
    }

    def create() {
		def cities = cityService.list()
        [reportInstance: new Report(params), cities: cities, availableOptions: availableOptions]
    }

    def save() {
		SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
		def initialDate = sp.parse(params.initialDate);
		def finalDate = sp.parse(params.finalDate);
		def cities = [];
		def counts = [];
		def op = params.option.toString(); 
		def cityId = params.city.id;
		def kind = "";
		cityId = cityId == null || cityId.isEmpty() || cityId.equalsIgnoreCase("null") ? -1 : Integer.parseInt(cityId);
		if(op.equals(a._key)){
			kind = a._value;
			cities = cityService.getMostPopularDestination(initialDate, finalDate);
			counts = cityService.getMostPopularDestinationCount(initialDate, finalDate);
		}else if(op.equals(b._key)){
			kind = b._value;
			cities = [cityService.getCityWithMoreTickets(initialDate, finalDate, cityId)];
			counts = [cityService.getCityWithMoreTicketsCount(initialDate, finalDate, cityId)]
		}else if(op.equals(c._key)){
			kind = c._value;
			cities = cityService.getMostPopularOrigin(initialDate, finalDate);
			counts = cityService.getMostPopularOriginCount(initialDate, finalDate);
		}
		render(view: "list", model:[cities: cities, counts: counts, option: kind]);
    }


}
