package com.buses.app.services.base;

import java.util.List;

import com.buses.app.beans.base.BaseBean;
import com.buses.webservice.dto.base.BaseDTO;

public interface IBaseService<BEAN extends BaseBean, DTO extends BaseDTO> {
	public BEAN getById(Integer id);

	public BEAN save(BEAN bean);

	public boolean delete(Integer id);

	public List<BEAN> list();

	public List<BEAN> search(String params);

	public Integer count(String params);

}
