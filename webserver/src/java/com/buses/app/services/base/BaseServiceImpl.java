package com.buses.app.services.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.buses.app.beans.base.BaseBean;
import com.buses.webservice.dto.base.BaseDTO;

public abstract class BaseServiceImpl<BEAN extends BaseBean, DTO extends BaseDTO>
		implements IBaseService<BEAN, DTO> {

	protected abstract BEAN dtoToBean(DTO dto);

	protected abstract DTO beanToDTO(BEAN bean);

	protected List<BEAN> resultToList(List<DTO> dtos) {
		if (dtos == null)
			return null;
		List<BEAN> list = new ArrayList<BEAN>();

		for (DTO dto : dtos)
			list.add(dtoToBean(dto));

		return list;
	}

	private Map<String, String> getQueryMap(String query) {
		System.out.println(query);
		if (query.indexOf("=") < 0)
			return null;
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();
		for (String param : params) {
			String name = param.split("=")[0];
			String value = param.split("=")[1];
			map.put(name, value);
		}
		return map;
	}

	protected String getValue(String params) {
		Map<String, String> m = getQueryMap(params);
		return null == m ? null : m.get("value");
	}

}
