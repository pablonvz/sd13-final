package com.buses.app.services.user;

import java.util.List;

import com.buses.app.beans.user.UserBean;
import com.buses.app.services.base.IBaseService;
import com.buses.webservice.dto.user.UserDTO;

public interface IUserService extends IBaseService<UserBean, UserDTO> {

	public List<UserBean> getByEmail(String email);

	public boolean isValidPassword(String hash, String password);
}
