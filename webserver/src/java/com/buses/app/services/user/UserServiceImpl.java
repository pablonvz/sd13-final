package com.buses.app.services.user;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.buses.app.beans.agency.AgencyBean;
import com.buses.app.beans.city.CityBean;
import com.buses.app.beans.country.CountryBean;
import com.buses.app.beans.person.PersonBean;
import com.buses.app.beans.state.StateBean;
import com.buses.app.beans.user.UserBean;
import com.buses.app.resources.agency.IAgencyResource;
import com.buses.app.resources.city.ICityResource;
import com.buses.app.resources.country.ICountryResource;
import com.buses.app.resources.person.IPersonResource;
import com.buses.app.resources.state.IStateResource;
import com.buses.app.resources.user.IUserResource;
import com.buses.app.services.base.BaseServiceImpl;
import com.buses.app.services.person.IPersonService;
import com.buses.webservice.dto.agency.AgencyDTO;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.person.PersonDTO;
import com.buses.webservice.dto.state.StateDTO;
import com.buses.webservice.dto.user.UserDTO;
import com.buses.webservice.dto.user.UserResult;

public class UserServiceImpl extends BaseServiceImpl<UserBean, UserDTO>
		implements IUserService {

	private final IUserResource _userResource;
	private final IPersonResource _personResource;
	private final IAgencyResource _agencyResource;
	private final ICityResource _cityResource;
	private final IStateResource _stateResource;
	private final ICountryResource _countryResource;
	private final IPersonService _personService;

	public UserServiceImpl(IUserResource userResource,
			IPersonResource personResource, IAgencyResource agencyResource,
			ICityResource cityResource, IStateResource stateResouce,
			ICountryResource countryResource, IPersonService personService) {
		_userResource = userResource;
		_personResource = personResource;
		_agencyResource = agencyResource;
		_cityResource = cityResource;
		_stateResource = stateResouce;
		_countryResource = countryResource;
		_personService = personService;
	}

	@Override
	public UserBean getById(Integer id) {
		return dtoToBean(_userResource.getById(id));
	}

	@Override
	public UserBean save(UserBean bean) {
		bean = new UserBean(bean.getId(), bean.getAgency(),
				_personService.save(bean.getPerson()),
				getPasswordHash(bean.getPasswordHash()), bean.getDeletedAt());

		return dtoToBean(_userResource.save(beanToDTO(bean)));
	}

	@Override
	public boolean delete(Integer id) {
		return _userResource.delete(id);
	}

	@Override
	public List<UserBean> list() {
		UserResult result = _userResource.getAll();

		List<UserBean> users = new ArrayList<UserBean>();

		for (UserDTO dto : result.getUsers())
			users.add(dtoToBean(dto));

		return users;
	}

	@Override
	protected UserBean dtoToBean(UserDTO dto) {
		if (dto == null || dto.getAgencyId() == null
				|| dto.getPersonId() == null)
			return null;
		AgencyDTO agencyDTO = _agencyResource.getById(dto.getAgencyId());

		CityDTO cityDTO = _cityResource.getById(agencyDTO.getCityId());

		StateDTO stateDTO = _stateResource.getById(cityDTO.getStateId());

		CountryDTO countryDTO = _countryResource.getById(stateDTO
				.getCountryId());
		CountryBean countryBean = new CountryBean(countryDTO.getId(),
				countryDTO.getCode(), countryDTO.getName(),
				countryDTO.getDeletedAt());

		StateBean stateBean = new StateBean(stateDTO.getId(), countryBean,
				stateDTO.getName(), stateDTO.getDeletedAt());

		CityBean cityBean = new CityBean(cityDTO.getId(), stateBean,
				cityDTO.getName(), cityDTO.getDeletedAt());

		AgencyBean agencyBean = new AgencyBean(agencyDTO.getId(), cityBean,
				agencyDTO.getDescription(), agencyDTO.getDeletedAt());

		PersonDTO personDTO = _personResource.getById(dto.getPersonId());

		PersonBean personBean = new PersonBean(personDTO.getId(),
				personDTO.getFirstName(), personDTO.getLastName(),
				personDTO.getBirthDate(), personDTO.getSex(),
				personDTO.getDocument(), personDTO.getEmail(),
				personDTO.getDeletedAt());

		UserBean ub = new UserBean(dto.getId(), agencyBean, personBean,
				dto.getPasswordHash(), dto.getDeletedAt());

		return ub;
	}

	@Override
	protected UserDTO beanToDTO(UserBean bean) {
		UserDTO dto = new UserDTO();
		dto.setPersonId(bean.getPerson().getId());
		dto.setId(bean.getId());
		dto.setAgencyId(bean.getAgency().getId());
		dto.setPasswordHash(bean.getPasswordHash());
		dto.setDeletedAt(bean.getDeletedAt());
		return dto;
	}

	@Override
	public List<UserBean> getByEmail(String email) {
		List<PersonDTO> pr = _personResource.getByEmail(email).getPersons();
		List<UserBean> users = new ArrayList<UserBean>();
		if (pr.isEmpty())
			return users;

		List<UserDTO> urs = _userResource.getByPersonId(pr.get(0).getId())
				.getUsers();
		if (urs.isEmpty())
			return users;
		for (UserDTO dto : urs)
			users.add(dtoToBean(dto));

		return users;
	}

	@Override
	public boolean isValidPassword(String hash, String password) {
		return hash.equals(getPasswordHash(password));
	}

	private String getPasswordHash(String password) {
		System.out.println("password:" + password);
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		md.update(password.getBytes());
		byte[] digest = md.digest();
		StringBuffer sb = new StringBuffer();
		for (byte b : digest) {
			sb.append(Integer.toHexString((int) (b & 0xff)));
		}
		System.out.println("hash:" + sb.toString());
		return sb.toString();
	}

	@Override
	public List<UserBean> search(String params) {
		String value = this.getValue(params);
		List<UserDTO> dtos = new ArrayList<UserDTO>();
		Map<Integer, UserDTO> hs = new LinkedHashMap<Integer, UserDTO>();
		if (value != null) {
			List<PersonDTO> people = _personResource
					.search("value="
							+ value
							+ "&fields=birthDate,document,email,firstName,lastName,sex")
					.getPersons();
			for (PersonDTO c : people) {
				List<UserDTO> usrs = _userResource.search(
						"value=" + c.getId() + "&fields=personId&like=false")
						.getUsers();
				for (UserDTO a : usrs) {
					hs.put(a.getId(), a);
				}
			}
			dtos.addAll(hs.values());
		} else {
			dtos = _userResource.search(params).getUsers();
		}
		return resultToList(dtos);
	}

	@Override
	public Integer count(String params) {
		return search(params).size();
	}

}
