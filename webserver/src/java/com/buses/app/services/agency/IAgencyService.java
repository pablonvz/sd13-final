package com.buses.app.services.agency;

import com.buses.app.beans.agency.AgencyBean;
import com.buses.app.services.base.IBaseService;
import com.buses.webservice.dto.agency.AgencyDTO;

public interface IAgencyService extends IBaseService<AgencyBean, AgencyDTO> {

}
