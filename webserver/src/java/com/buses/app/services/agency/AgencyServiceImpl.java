package com.buses.app.services.agency;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.buses.app.beans.agency.AgencyBean;
import com.buses.app.beans.city.CityBean;
import com.buses.app.beans.country.CountryBean;
import com.buses.app.beans.state.StateBean;
import com.buses.app.resources.agency.IAgencyResource;
import com.buses.app.resources.city.ICityResource;
import com.buses.app.resources.country.ICountryResource;
import com.buses.app.resources.state.IStateResource;
import com.buses.app.services.base.BaseServiceImpl;
import com.buses.webservice.dto.agency.AgencyDTO;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.state.StateDTO;

public class AgencyServiceImpl extends BaseServiceImpl<AgencyBean, AgencyDTO>
		implements IAgencyService {

	private final IAgencyResource _agencyResource;
	private final ICityResource _cityResource;
	private final IStateResource _stateResource;
	private final ICountryResource _countryResource;

	public AgencyServiceImpl(IAgencyResource agencyResource,
			ICityResource cityResource, IStateResource stateResource,
			ICountryResource countryResource) {
		_agencyResource = agencyResource;
		_cityResource = cityResource;
		_stateResource = stateResource;
		_countryResource = countryResource;
	}

	@Override
	public AgencyBean getById(Integer id) {
		return dtoToBean(_agencyResource.getById(id));
	}

	@Override
	public AgencyBean save(AgencyBean bean) {
		return dtoToBean(_agencyResource.save(beanToDTO(bean)));
	}

	@Override
	public boolean delete(Integer id) {
		return _agencyResource.delete(id);
	}

	@Override
	public List<AgencyBean> list() {
		return resultToList(_agencyResource.getAll().getAgencys());
	}

	@Override
	protected AgencyBean dtoToBean(AgencyDTO agencyDTO) {
		if (agencyDTO == null || agencyDTO.getCityId() == null)
			return null;
		CityDTO cityDTO = _cityResource.getById(agencyDTO.getCityId());
		if (cityDTO.getStateId() == null)
			return null;
		StateDTO stateDTO = _stateResource.getById(cityDTO.getStateId());
		if (stateDTO.getCountryId() == null)
			return null;
		CountryDTO countryDTO = _countryResource.getById(stateDTO
				.getCountryId());
		CountryBean countryBean = new CountryBean(countryDTO.getId(),
				countryDTO.getCode(), countryDTO.getName(),
				countryDTO.getDeletedAt());

		StateBean stateBean = new StateBean(stateDTO.getId(), countryBean,
				stateDTO.getName(), stateDTO.getDeletedAt());

		CityBean cityBean = new CityBean(cityDTO.getId(), stateBean,
				cityDTO.getName(), cityDTO.getDeletedAt());

		AgencyBean agencyBean = new AgencyBean(agencyDTO.getId(), cityBean,
				agencyDTO.getDescription(), agencyDTO.getDeletedAt());

		return agencyBean;
	}

	@Override
	protected AgencyDTO beanToDTO(AgencyBean bean) {
		AgencyDTO dto = new AgencyDTO();
		dto.setId(bean.getId());
		dto.setCityId(bean.getCity().getId());
		dto.setDescription(bean.getDescription());
		dto.setDeletedAt(bean.getDeletedAt());
		return dto;
	}

	@Override
	public List<AgencyBean> search(String params) {
		String value = this.getValue(params);
		List<AgencyDTO> dtos = new ArrayList<AgencyDTO>();
		Map<Integer, AgencyDTO> hs = new LinkedHashMap<Integer, AgencyDTO>();
		if (value != null) {
			List<CityDTO> cityDtos = new ArrayList<CityDTO>();
			Map<Integer, CityDTO> cityM = new LinkedHashMap<Integer, CityDTO>();
			if (value != null) {
				List<StateDTO> states = _stateResource.search(
						"value=" + value + "&fields=name").getStates();
				for (StateDTO c : states) {
					List<CityDTO> cts = _cityResource
							.search("value=" + c.getId()
									+ "&fields=stateId&like=false").getCitys();
					for (CityDTO a : cts) {
						cityM.put(a.getId(), a);
					}
				}

				List<CountryDTO> countries = _countryResource.search(
						"value=" + value + "&fields=name").getCountrys();
				for (CountryDTO c : countries) {
					List<StateDTO> statesC = _stateResource.search(
							"value=" + c.getId()
									+ "&fields=countryId&like=false")
							.getStates();
					for (StateDTO s : statesC) {
						List<CityDTO> cts = _cityResource.search(
								"value=" + s.getId()
										+ "&fields=stateId&like=false")
								.getCitys();
						for (CityDTO a : cts) {
							cityM.put(a.getId(), a);
						}
					}
				}

			}
			List<CityDTO> cts = _cityResource.search(
					"value=" + value + "&fields=name").getCitys();
			for (CityDTO a : cts) {
				cityM.put(a.getId(), a);
			}
			cityDtos.addAll(cityM.values());
			for (CityDTO c : cityDtos) {
				List<AgencyDTO> ags = _agencyResource.search(
						"value=" + c.getId() + "&fields=cityId&like=false")
						.getAgencys();
				for (AgencyDTO a : ags) {
					hs.put(a.getId(), a);
				}
			}
		}
		List<AgencyDTO> ags = _agencyResource.search(params).getAgencys();
		for (AgencyDTO a : ags) {
			hs.put(a.getId(), a);
		}
		dtos.addAll(hs.values());
		return resultToList(dtos);
	}

	@Override
	public Integer count(String params) {
		return search(params).size();
	}

}
