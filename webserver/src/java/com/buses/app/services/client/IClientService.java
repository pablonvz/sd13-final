package com.buses.app.services.client;

import com.buses.app.beans.client.ClientBean;
import com.buses.app.services.base.IBaseService;
import com.buses.webservice.dto.client.ClientDTO;

public interface IClientService extends IBaseService<ClientBean, ClientDTO> {

}
