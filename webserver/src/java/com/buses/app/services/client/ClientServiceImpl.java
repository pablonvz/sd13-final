package com.buses.app.services.client;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.buses.app.beans.client.ClientBean;
import com.buses.app.beans.person.PersonBean;
import com.buses.app.resources.client.IClientResource;
import com.buses.app.resources.person.IPersonResource;
import com.buses.app.services.base.BaseServiceImpl;
import com.buses.webservice.dto.client.ClientDTO;
import com.buses.webservice.dto.client.ClientResult;
import com.buses.webservice.dto.person.PersonDTO;

public class ClientServiceImpl extends BaseServiceImpl<ClientBean, ClientDTO>
		implements IClientService {

	private final IClientResource _clientResource;
	private final IPersonResource _personResource;

	public ClientServiceImpl(IClientResource clientResource,
			IPersonResource personResource) {
		_clientResource = clientResource;
		_personResource = personResource;
	}

	@Override
	public ClientBean getById(Integer id) {
		return dtoToBean(_clientResource.getById(id));
	}

	@Override
	public ClientBean save(ClientBean bean) {
		return dtoToBean(_clientResource.save(beanToDTO(bean)));
	}

	@Override
	public boolean delete(Integer id) {
		return _clientResource.delete(id);
	}

	@Override
	public List<ClientBean> list() {
		ClientResult result = _clientResource.getAll();

		List<ClientBean> clients = new ArrayList<ClientBean>();

		for (ClientDTO dto : result.getClients())
			clients.add(dtoToBean(dto));

		return clients;
	}

	@Override
	protected ClientBean dtoToBean(ClientDTO dto) {
		// PersonDTO pd = null == dto.getPersonId() ? new PersonDTO() :
		// _personResource.getById(dto.getPersonId());
		if (dto == null || dto.getPersonId() == null)
			return null;
		PersonDTO pd = _personResource.getById(dto.getPersonId());
		return new ClientBean(dto.getId(), dto.getDeletedAt(), new PersonBean(
				pd.getId(), pd.getFirstName(), pd.getLastName(),
				pd.getBirthDate(), pd.getSex(), pd.getDocument(),
				pd.getEmail(), pd.getDeletedAt()));
	}

	@Override
	protected ClientDTO beanToDTO(ClientBean bean) {
		ClientDTO dto = new ClientDTO();
		dto.setId(bean.getId());
		dto.setPersonId(bean.getPerson().getId());
		dto.setDeletedAt(bean.getDeletedAt());
		return dto;
	}

	@Override
	public List<ClientBean> search(String params) {
		String value = this.getValue(params);
		List<ClientDTO> dtos = new ArrayList<ClientDTO>();
		Map<Integer, ClientDTO> hs = new LinkedHashMap<Integer, ClientDTO>();
		if (value != null) {
			List<PersonDTO> people = _personResource
					.search("value="
							+ value
							+ "&fields=birthDate,document,email,firstName,lastName,sex")
					.getPersons();
			for (PersonDTO c : people) {
				List<ClientDTO> clis = _clientResource.search(
						"value=" + c.getId() + "&fields=personId&like=false")
						.getClients();
				for (ClientDTO a : clis) {
					hs.put(a.getId(), a);
				}
			}
			dtos.addAll(hs.values());
		} else {
			dtos = _clientResource.search(params).getClients();
		}
		return resultToList(dtos);
	}

	@Override
	public Integer count(String p) {
		return search(p).size();
	}

}
