package com.buses.app.services.state;

import com.buses.app.beans.state.StateBean;
import com.buses.app.services.base.IBaseService;
import com.buses.webservice.dto.state.StateDTO;

public interface IStateService extends IBaseService<StateBean, StateDTO> {

}
