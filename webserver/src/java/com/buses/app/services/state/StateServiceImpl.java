package com.buses.app.services.state;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.buses.app.beans.country.CountryBean;
import com.buses.app.beans.state.StateBean;
import com.buses.app.resources.country.ICountryResource;
import com.buses.app.resources.state.IStateResource;
import com.buses.app.services.base.BaseServiceImpl;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.state.StateDTO;
import com.buses.webservice.dto.state.StateResult;

public class StateServiceImpl extends BaseServiceImpl<StateBean, StateDTO>
		implements IStateService {

	private final IStateResource _stateResource;
	private final ICountryResource _countryResource;

	public StateServiceImpl(IStateResource stateResource,
			ICountryResource countryResource) {
		_stateResource = stateResource;
		_countryResource = countryResource;
	}

	@Override
	public StateBean getById(Integer id) {
		return dtoToBean(_stateResource.getById(id));
	}

	@Override
	public StateBean save(StateBean bean) {
		return dtoToBean(_stateResource.save(beanToDTO(bean)));
	}

	@Override
	public boolean delete(Integer id) {
		return _stateResource.delete(id);
	}

	@Override
	public List<StateBean> list() {
		StateResult result = _stateResource.getAll();

		List<StateBean> states = new ArrayList<StateBean>();

		for (StateDTO dto : result.getStates())
			states.add(dtoToBean(dto));

		return states;
	}

	@Override
	protected StateBean dtoToBean(StateDTO stateDTO) {
		if (stateDTO == null || stateDTO.getCountryId() == null)
			return null;
		CountryDTO countryDTO = _countryResource.getById(stateDTO
				.getCountryId());
		CountryBean countryBean = new CountryBean(countryDTO.getId(),
				countryDTO.getCode(), countryDTO.getName(),
				countryDTO.getDeletedAt());

		StateBean stateBean = new StateBean(stateDTO.getId(), countryBean,
				stateDTO.getName(), stateDTO.getDeletedAt());

		return stateBean;
	}

	@Override
	protected StateDTO beanToDTO(StateBean bean) {
		StateDTO dto = new StateDTO();
		dto.setId(bean.getId());
		dto.setCountryId(bean.getCountry().getId());
		dto.setName(bean.getName());
		dto.setDeletedAt(bean.getDeletedAt());
		return dto;
	}

	@Override
	public List<StateBean> search(String params) {
		String value = this.getValue(params);
		List<StateDTO> dtos = new ArrayList<StateDTO>();
		Map<Integer, StateDTO> hs = new LinkedHashMap<Integer, StateDTO>();
		if (value != null) {
			List<CountryDTO> countries = _countryResource.search(
					"value=" + value + "&fields=name").getCountrys();
			for (CountryDTO c : countries) {
				List<StateDTO> statesC = _stateResource.search(
						"value=" + c.getId() + "&fields=countryId&like=false")
						.getStates();
				for (StateDTO a : statesC) {
					hs.put(a.getId(), a);
				}
			}
		}
		List<StateDTO> sts = _stateResource.search(params).getStates();
		for (StateDTO a : sts) {
			hs.put(a.getId(), a);
		}
		dtos.addAll(hs.values());
		return resultToList(dtos);
	}

	@Override
	public Integer count(String params) {
		return search(params).size();
	}

}
