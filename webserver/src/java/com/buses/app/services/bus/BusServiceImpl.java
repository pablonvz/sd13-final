package com.buses.app.services.bus;

import java.util.ArrayList;
import java.util.List;

import com.buses.app.beans.bus.BusBean;
import com.buses.app.resources.bus.IBusResource;
import com.buses.app.services.base.BaseServiceImpl;
import com.buses.webservice.dto.bus.BusDTO;
import com.buses.webservice.dto.bus.BusResult;

public class BusServiceImpl extends BaseServiceImpl<BusBean, BusDTO> implements
		IBusService {

	private final IBusResource _busResource;

	public BusServiceImpl(IBusResource busResource) {
		_busResource = busResource;
	}

	@Override
	public BusBean getById(Integer id) {
		return dtoToBean(_busResource.getById(id));
	}

	@Override
	public BusBean save(BusBean bean) {
		return dtoToBean(_busResource.save(beanToDTO(bean)));
	}

	@Override
	public boolean delete(Integer id) {
		return _busResource.delete(id);
	}

	@Override
	public List<BusBean> list() {
		BusResult result = _busResource.getAll();

		List<BusBean> buss = new ArrayList<BusBean>();

		for (BusDTO dto : result.getBuss())
			buss.add(dtoToBean(dto));

		return buss;
	}

	@Override
	protected BusBean dtoToBean(BusDTO dto) {
		if (dto == null)
			return null;
		return new BusBean(dto.getId(), dto.getNumber(), dto.getCapacty(),
				dto.getModel(), dto.getFuel(), dto.getBrand(), dto.getYear(),
				dto.getMotor(), dto.getDeletedAt());
	}

	@Override
	protected BusDTO beanToDTO(BusBean bean) {
		BusDTO dto = new BusDTO();
		dto.setId(bean.getId());
		dto.setNumber(bean.getNumber());
		dto.setCapacty(bean.getCapacty());
		dto.setModel(bean.getModel());
		dto.setFuel(bean.getFuel());
		dto.setBrand(bean.getBrand());
		dto.setYear(bean.getYear());
		dto.setMotor(bean.getMotor());
		dto.setDeletedAt(bean.getDeletedAt());
		return dto;
	}

	@Override
	public List<BusBean> search(String params) {
		return resultToList(_busResource.search(params).getBuss());
	}

	public Integer count(String p) {
		return _busResource.count(p);
	}
}
