package com.buses.app.services.bus;

import com.buses.app.beans.bus.BusBean;
import com.buses.app.services.base.IBaseService;
import com.buses.webservice.dto.bus.BusDTO;

public interface IBusService extends IBaseService<BusBean, BusDTO> {

}
