package com.buses.app.services.person;

import java.util.List;

import com.buses.app.beans.person.PersonBean;
import com.buses.app.services.base.IBaseService;
import com.buses.webservice.dto.person.PersonDTO;

public interface IPersonService extends IBaseService<PersonBean, PersonDTO> {

	public List<PersonBean> getByEmail(String email);
}
