package com.buses.app.services.person;

import java.util.ArrayList;
import java.util.List;

import com.buses.app.beans.person.PersonBean;
import com.buses.app.resources.person.IPersonResource;
import com.buses.app.services.base.BaseServiceImpl;
import com.buses.webservice.dto.person.PersonDTO;
import com.buses.webservice.dto.person.PersonResult;

public class PersonServiceImpl extends BaseServiceImpl<PersonBean, PersonDTO>
		implements IPersonService {

	private final IPersonResource _personResource;

	public PersonServiceImpl(IPersonResource personResource) {
		_personResource = personResource;
	}

	@Override
	public PersonBean getById(Integer id) {
		return dtoToBean(_personResource.getById(id));
	}

	@Override
	public PersonBean save(PersonBean bean) {
		return dtoToBean(_personResource.save(beanToDTO(bean)));
	}

	@Override
	public boolean delete(Integer id) {
		return _personResource.delete(id);
	}

	@Override
	public List<PersonBean> list() {
		PersonResult result = _personResource.getAll();

		List<PersonBean> persons = new ArrayList<PersonBean>();

		for (PersonDTO dto : result.getPersons())
			persons.add(dtoToBean(dto));

		return persons;
	}

	@Override
	protected PersonBean dtoToBean(PersonDTO dto) {
		if (dto == null)
			return null;
		return new PersonBean(dto.getId(), dto.getFirstName(),
				dto.getLastName(), dto.getBirthDate(), dto.getSex(),
				dto.getDocument(), dto.getEmail(), dto.getDeletedAt());
	}

	@Override
	protected PersonDTO beanToDTO(PersonBean bean) {
		PersonDTO dto = new PersonDTO();
		dto.setId(bean.getId());
		dto.setFirstName(bean.getFirstName());
		dto.setLastName(bean.getLastName());
		dto.setBirthDate(bean.getBirthDate());
		dto.setSex(bean.getSex());
		dto.setDocument(bean.getDocument());
		dto.setEmail(bean.getEmail());
		dto.setDeletedAt(bean.getDeletedAt());
		return dto;
	}

	@Override
	public List<PersonBean> getByEmail(String email) {
		PersonResult result = _personResource.getByEmail(email);

		List<PersonBean> persons = new ArrayList<PersonBean>();

		for (PersonDTO dto : result.getPersons())
			persons.add(dtoToBean(dto));

		return persons;
	}

	@Override
	public List<PersonBean> search(String params) {
		return resultToList(_personResource.search(params).getPersons());
	}

	@Override
	public Integer count(String p) {
		return _personResource.count(p);
	}
}
