package com.buses.app.services.driver;

import com.buses.app.beans.driver.DriverBean;
import com.buses.app.services.base.IBaseService;
import com.buses.webservice.dto.driver.DriverDTO;

public interface IDriverService extends IBaseService<DriverBean, DriverDTO> {

}
