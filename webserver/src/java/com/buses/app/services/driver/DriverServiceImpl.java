package com.buses.app.services.driver;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.buses.app.beans.agency.AgencyBean;
import com.buses.app.beans.city.CityBean;
import com.buses.app.beans.country.CountryBean;
import com.buses.app.beans.driver.DriverBean;
import com.buses.app.beans.person.PersonBean;
import com.buses.app.beans.state.StateBean;
import com.buses.app.resources.agency.IAgencyResource;
import com.buses.app.resources.city.ICityResource;
import com.buses.app.resources.country.ICountryResource;
import com.buses.app.resources.driver.IDriverResource;
import com.buses.app.resources.person.IPersonResource;
import com.buses.app.resources.state.IStateResource;
import com.buses.app.services.base.BaseServiceImpl;
import com.buses.app.services.person.IPersonService;
import com.buses.webservice.dto.agency.AgencyDTO;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.driver.DriverDTO;
import com.buses.webservice.dto.driver.DriverResult;
import com.buses.webservice.dto.person.PersonDTO;
import com.buses.webservice.dto.state.StateDTO;

public class DriverServiceImpl extends BaseServiceImpl<DriverBean, DriverDTO>
		implements IDriverService {

	private final IDriverResource _driverResource;
	private final IAgencyResource _agencyResource;
	private final ICountryResource _countryResource;
	private final ICityResource _cityResource;
	private final IStateResource _stateResource;
	private final IPersonResource _personResource;
	private final IPersonService _personService;

	public DriverServiceImpl(IDriverResource driverResource,
			IPersonResource personResource, IAgencyResource agencyResource,
			ICountryResource countryResource, ICityResource cityResource,
			IStateResource stateResource, IPersonService personService) {
		_driverResource = driverResource;
		_agencyResource = agencyResource;
		_countryResource = countryResource;
		_cityResource = cityResource;
		_stateResource = stateResource;
		_personResource = personResource;
		_personService = personService;
	}

	@Override
	public DriverBean getById(Integer id) {
		return dtoToBean(_driverResource.getById(id));
	}

	@Override
	public DriverBean save(DriverBean bean) {
		bean = new DriverBean(bean.getId(), bean.getAgency(),
				_personService.save(bean.getPerson()), bean.getDeletedAt());
		return dtoToBean(_driverResource.save(beanToDTO(bean)));
	}

	@Override
	public boolean delete(Integer id) {
		return _driverResource.delete(id);
	}

	@Override
	public List<DriverBean> list() {
		DriverResult result = _driverResource.getAll();

		List<DriverBean> drivers = new ArrayList<DriverBean>();

		for (DriverDTO dto : result.getDrivers())
			drivers.add(dtoToBean(dto));

		return drivers;
	}

	@Override
	protected DriverBean dtoToBean(DriverDTO dd) {
		if (dd == null || dd.getPersonId() == null || dd.getAgencyId() == null)
			return null;
		AgencyDTO agencyDTO = _agencyResource.getById(dd.getAgencyId());

		CityDTO cityDTO = _cityResource.getById(agencyDTO.getCityId());

		StateDTO stateDTO = _stateResource.getById(cityDTO.getStateId());

		CountryDTO countryDTO = _countryResource.getById(stateDTO
				.getCountryId());
		CountryBean countryBean = new CountryBean(countryDTO.getId(),
				countryDTO.getCode(), countryDTO.getName(),
				countryDTO.getDeletedAt());

		StateBean stateBean = new StateBean(stateDTO.getId(), countryBean,
				stateDTO.getName(), stateDTO.getDeletedAt());

		CityBean cityBean = new CityBean(cityDTO.getId(), stateBean,
				cityDTO.getName(), cityDTO.getDeletedAt());

		AgencyBean agencyBean = new AgencyBean(agencyDTO.getId(), cityBean,
				agencyDTO.getDescription(), agencyDTO.getDeletedAt());

		PersonDTO personDTO = _personResource.getById(dd.getPersonId());

		PersonBean personBean = new PersonBean(personDTO.getId(),
				personDTO.getFirstName(), personDTO.getLastName(),
				personDTO.getBirthDate(), personDTO.getSex(),
				personDTO.getDocument(), personDTO.getEmail(),
				personDTO.getDeletedAt());

		DriverBean db = new DriverBean(dd.getId(), agencyBean, personBean,
				dd.getDeletedAt());

		return db;
	}

	@Override
	protected DriverDTO beanToDTO(DriverBean bean) {
		DriverDTO dto = new DriverDTO();
		dto.setPersonId(bean.getPerson().getId());
		dto.setId(bean.getId());
		dto.setAgencyId(bean.getAgency().getId());
		dto.setDeletedAt(bean.getDeletedAt());
		return dto;
	}

	@Override
	public List<DriverBean> search(String params) {
		String value = this.getValue(params);
		List<DriverDTO> dtos = new ArrayList<DriverDTO>();
		Map<Integer, DriverDTO> hs = new LinkedHashMap<Integer, DriverDTO>();
		if (value != null) {
			List<PersonDTO> people = _personResource
					.search("value="
							+ value
							+ "&fields=birthDate,document,email,firstName,lastName,sex")
					.getPersons();
			for (PersonDTO c : people) {
				List<DriverDTO> drvs = _driverResource.search(
						"value=" + c.getId() + "&fields=personId&like=false")
						.getDrivers();
				for (DriverDTO a : drvs) {
					hs.put(a.getId(), a);
				}
			}
			List<AgencyDTO> ags = _agencyResource.search(
					"value=" + value + "&fields=description").getAgencys();
			for (AgencyDTO c : ags) {
				List<DriverDTO> drvs = _driverResource.search(
						"value=" + c.getId() + "&fields=agencyId&like=false")
						.getDrivers();
				for (DriverDTO a : drvs) {
					hs.put(a.getId(), a);
				}
			}
			dtos.addAll(hs.values());
		} else {
			dtos = _driverResource.search(params).getDrivers();
		}
		return resultToList(dtos);
	}

	@Override
	public Integer count(String p) {
		return search(p).size();
	}

}
