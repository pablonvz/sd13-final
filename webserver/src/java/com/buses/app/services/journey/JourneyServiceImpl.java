package com.buses.app.services.journey;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.buses.app.beans.agency.AgencyBean;
import com.buses.app.beans.bus.BusBean;
import com.buses.app.beans.city.CityBean;
import com.buses.app.beans.country.CountryBean;
import com.buses.app.beans.driver.DriverBean;
import com.buses.app.beans.itinerary.ItineraryBean;
import com.buses.app.beans.journey.JourneyBean;
import com.buses.app.beans.person.PersonBean;
import com.buses.app.beans.state.StateBean;
import com.buses.app.resources.agency.IAgencyResource;
import com.buses.app.resources.bus.IBusResource;
import com.buses.app.resources.city.ICityResource;
import com.buses.app.resources.country.ICountryResource;
import com.buses.app.resources.driver.IDriverResource;
import com.buses.app.resources.itinerary.IItineraryResource;
import com.buses.app.resources.journey.IJourneyResource;
import com.buses.app.resources.person.IPersonResource;
import com.buses.app.resources.state.IStateResource;
import com.buses.app.services.base.BaseServiceImpl;
import com.buses.webservice.dto.agency.AgencyDTO;
import com.buses.webservice.dto.bus.BusDTO;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.driver.DriverDTO;
import com.buses.webservice.dto.itinerary.ItineraryDTO;
import com.buses.webservice.dto.journey.JourneyDTO;
import com.buses.webservice.dto.journey.JourneyResult;
import com.buses.webservice.dto.person.PersonDTO;
import com.buses.webservice.dto.state.StateDTO;

public class JourneyServiceImpl extends
		BaseServiceImpl<JourneyBean, JourneyDTO> implements IJourneyService {

	private final IJourneyResource _journeyResource;
	private final IItineraryResource _itineraryResource;
	private final IBusResource _busResource;
	private final IDriverResource _driverResource;
	private final IPersonResource _personResource;
	private final IAgencyResource _agencyResource;
	private final ICityResource _cityResource;
	private final IStateResource _stateResource;
	private final ICountryResource _countryResource;

	public JourneyServiceImpl(IJourneyResource journeyResource,
			IItineraryResource itResource, IBusResource busResource,
			IDriverResource driverResource, IPersonResource personResource,
			IAgencyResource agencyResource, ICityResource cityResource,
			IStateResource stateResouce, ICountryResource countryResource) {
		_journeyResource = journeyResource;
		_itineraryResource = itResource;
		_busResource = busResource;
		_driverResource = driverResource;
		_agencyResource = agencyResource;
		_cityResource = cityResource;
		_stateResource = stateResouce;
		_countryResource = countryResource;
		_personResource = personResource;
	}

	@Override
	public JourneyBean getById(Integer id) {
		return dtoToBean(_journeyResource.getById(id));
	}

	@Override
	public JourneyBean save(JourneyBean bean) {
		return dtoToBean(_journeyResource.save(beanToDTO(bean)));
	}

	@Override
	public boolean delete(Integer id) {
		return _journeyResource.delete(id);
	}

	@Override
	public List<JourneyBean> list() {
		JourneyResult result = _journeyResource.getAll();

		List<JourneyBean> journeys = new ArrayList<JourneyBean>();

		for (JourneyDTO dto : result.getJourneys())
			journeys.add(dtoToBean(dto));

		return journeys;
	}

	@Override
	protected JourneyBean dtoToBean(JourneyDTO dto) {
		if (dto == null || dto.getBusId() == null || dto.getDriverId() == null
				|| dto.getItineraryId() == null)
			return null;
		JourneyDTO jd = _journeyResource.getById(dto.getId());
		ItineraryDTO id = _itineraryResource.getById(jd.getItineraryId());

		CityDTO originDTO = _cityResource.getById(id.getOrigin());
		CityDTO destinationDTO = _cityResource.getById(id.getDestination());
		StateDTO destinationStateDTO = _stateResource.getById(destinationDTO
				.getStateId());
		CountryDTO destinationCountryDTO = _countryResource
				.getById(destinationStateDTO.getCountryId());
		CountryBean destinationCountryBean = new CountryBean(
				destinationCountryDTO.getId(), destinationCountryDTO.getCode(),
				destinationCountryDTO.getName(),
				destinationCountryDTO.getDeletedAt());
		StateBean destinationStateBean = new StateBean(
				destinationStateDTO.getId(), destinationCountryBean,
				destinationStateDTO.getName(), destinationDTO.getDeletedAt());
		CityBean destination = new CityBean(destinationDTO.getId(),
				destinationStateBean, destinationDTO.getName(),
				destinationDTO.getDeletedAt());

		StateDTO originStateDTO = _stateResource
				.getById(originDTO.getStateId());
		CountryDTO originCountryDTO = _countryResource.getById(originStateDTO
				.getCountryId());
		CountryBean originCountryBean = new CountryBean(
				originCountryDTO.getId(), originCountryDTO.getCode(),
				originCountryDTO.getName(), originCountryDTO.getDeletedAt());
		StateBean originStateBean = new StateBean(originStateDTO.getId(),
				originCountryBean, originStateDTO.getName(),
				originDTO.getDeletedAt());
		CityBean origin = new CityBean(originDTO.getId(), originStateBean,
				originDTO.getName(), originDTO.getDeletedAt());
		ItineraryBean ib = new ItineraryBean(id.getId(), origin, destination,
				id.getDeletedAt());

		BusDTO bd = _busResource.getById(jd.getBusId());
		BusBean bb = new BusBean(bd.getId(), bd.getNumber(), bd.getCapacty(),
				bd.getModel(), bd.getFuel(), bd.getBrand(), bd.getYear(),
				bd.getMotor(), bd.getDeletedAt());

		DriverDTO dd = _driverResource.getById(jd.getDriverId());

		AgencyDTO agencyDTO = _agencyResource.getById(dd.getAgencyId());

		CityDTO cityDTO = _cityResource.getById(agencyDTO.getCityId());

		StateDTO stateDTO = _stateResource.getById(cityDTO.getStateId());

		CountryDTO countryDTO = _countryResource.getById(stateDTO
				.getCountryId());
		CountryBean countryBean = new CountryBean(countryDTO.getId(),
				countryDTO.getCode(), countryDTO.getName(),
				countryDTO.getDeletedAt());

		StateBean stateBean = new StateBean(stateDTO.getId(), countryBean,
				stateDTO.getName(), stateDTO.getDeletedAt());

		CityBean cityBean = new CityBean(cityDTO.getId(), stateBean,
				cityDTO.getName(), cityDTO.getDeletedAt());

		AgencyBean agencyBean = new AgencyBean(agencyDTO.getId(), cityBean,
				agencyDTO.getDescription(), agencyDTO.getDeletedAt());

		PersonDTO personDTO = _personResource.getById(dd.getPersonId());
		PersonBean personBean = new PersonBean(personDTO.getId(),
				personDTO.getFirstName(), personDTO.getLastName(),
				personDTO.getBirthDate(), personDTO.getSex(),
				personDTO.getDocument(), personDTO.getEmail(),
				personDTO.getDeletedAt());

		DriverBean db = new DriverBean(dd.getId(), agencyBean, personBean,
				dd.getDeletedAt());

		JourneyBean jb = new JourneyBean(jd.getId(), ib, jd.getDate(), bb, db,
				jd.getPrice(), jd.getInitialKm(), jd.getEndKm(),
				jd.getIncident(), jd.getDeletedAt());

		return jb;
	}

	@Override
	protected JourneyDTO beanToDTO(JourneyBean bean) {
		JourneyDTO dto = new JourneyDTO();
		dto.setId(bean.getId());
		dto.setItineraryId(bean.getItinerary().getId());
		dto.setBusId(bean.getBus().getId());
		dto.setDriverId(bean.getDriver().getId());
		dto.setDate(bean.getDate());
		dto.setPrice(bean.getPrice());
		dto.setInitialKm(bean.getInitialKm());
		dto.setEndKm(bean.getEndKm());
		dto.setIncident(bean.getIncident());
		dto.setDeletedAt(bean.getDeletedAt());
		return dto;
	}

	@Override
	public List<JourneyBean> search(String params) {
		String value = this.getValue(params);
		List<JourneyDTO> dtos = new ArrayList<JourneyDTO>();
		Map<Integer, JourneyDTO> hs = new LinkedHashMap<Integer, JourneyDTO>();
		if (value != null) {
			List<CityDTO> cities = _cityResource.search(
					"value=" + value + "&fields=name").getCitys();
			for (CityDTO c : cities) {
				List<ItineraryDTO> its = _itineraryResource.search(
						"value=" + c.getId()
								+ "&fields=destination,origin&like=false")
						.getItinerarys();
				for (ItineraryDTO a : its) {
					List<JourneyDTO> jns = _journeyResource.search(
							"value=" + a.getId()
									+ "&fields=itineraryId&like=false")
							.getJourneys();
					for (JourneyDTO b : jns)
						hs.put(b.getId(), b);
				}
			}
			List<PersonDTO> people = _personResource
					.search("value="
							+ value
							+ "&fields=birthDate,document,email,firstName,lastName,sex")
					.getPersons();
			for (PersonDTO c : people) {
				List<DriverDTO> drvs = _driverResource.search(
						"value=" + c.getId() + "&fields=personId&like=false")
						.getDrivers();
				for (DriverDTO a : drvs) {
					List<JourneyDTO> jns = _journeyResource.search(
							"value=" + a.getId()
									+ "&fields=driverId&like=false")
							.getJourneys();
					for (JourneyDTO b : jns)
						hs.put(b.getId(), b);
				}
			}
			List<BusDTO> buses = _busResource
					.search("value="
							+ value
							+ "&fields=number,brand,capacty,fuel,model,motor,year")
					.getBuss();
			for (BusDTO a : buses) {
				List<JourneyDTO> jns = _journeyResource.search(
						"value=" + a.getId() + "&fields=busId&like=false")
						.getJourneys();
				for (JourneyDTO b : jns)
					hs.put(b.getId(), b);
			}
		}
		List<JourneyDTO> jns = _journeyResource.search(params).getJourneys();
		for (JourneyDTO a : jns) {
			hs.put(a.getId(), a);
		}
		dtos.addAll(hs.values());
		return resultToList(dtos);
	}

	@Override
	public Integer count(String p) {
		return search(p).size();
	}

}
