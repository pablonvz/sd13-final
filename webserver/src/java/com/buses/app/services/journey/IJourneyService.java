package com.buses.app.services.journey;

import com.buses.app.beans.journey.JourneyBean;
import com.buses.app.services.base.IBaseService;
import com.buses.webservice.dto.journey.JourneyDTO;

public interface IJourneyService extends IBaseService<JourneyBean, JourneyDTO> {

}
