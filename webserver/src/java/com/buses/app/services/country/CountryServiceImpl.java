package com.buses.app.services.country;

import java.util.ArrayList;
import java.util.List;

import com.buses.app.beans.country.CountryBean;
import com.buses.app.resources.country.ICountryResource;
import com.buses.app.services.base.BaseServiceImpl;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.country.CountryResult;

public class CountryServiceImpl extends
		BaseServiceImpl<CountryBean, CountryDTO> implements ICountryService {

	private final ICountryResource _countryResource;

	public CountryServiceImpl(ICountryResource countryResource) {
		_countryResource = countryResource;
	}

	@Override
	public CountryBean getById(Integer id) {
		return dtoToBean(_countryResource.getById(id));
	}

	@Override
	public CountryBean save(CountryBean bean) {
		return dtoToBean(_countryResource.save(beanToDTO(bean)));
	}

	@Override
	public boolean delete(Integer id) {
		return _countryResource.delete(id);
	}

	@Override
	public List<CountryBean> list() {
		CountryResult result = _countryResource.getAll();

		List<CountryBean> countrys = new ArrayList<CountryBean>();

		for (CountryDTO dto : result.getCountrys())
			countrys.add(dtoToBean(dto));

		return countrys;
	}

	@Override
	protected CountryBean dtoToBean(CountryDTO dto) {
		return new CountryBean(dto.getId(), dto.getCode(), dto.getName(),
				dto.getDeletedAt());
	}

	@Override
	protected CountryDTO beanToDTO(CountryBean bean) {
		CountryDTO dto = new CountryDTO();
		dto.setId(bean.getId());
		dto.setCode(bean.getCode());
		dto.setName(bean.getName());
		dto.setDeletedAt(bean.getDeletedAt());
		return dto;
	}

	@Override
	public List<CountryBean> search(String params) {
		return resultToList(_countryResource.search(params).getCountrys());
	}

	public Integer count(String p) {
		return _countryResource.count(p);
	}

}
