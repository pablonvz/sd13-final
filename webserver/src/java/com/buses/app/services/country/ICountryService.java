package com.buses.app.services.country;

import com.buses.app.beans.country.CountryBean;
import com.buses.app.services.base.IBaseService;
import com.buses.webservice.dto.country.CountryDTO;

public interface ICountryService extends IBaseService<CountryBean, CountryDTO> {

}
