package com.buses.app.services.itinerary;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.buses.app.beans.city.CityBean;
import com.buses.app.beans.country.CountryBean;
import com.buses.app.beans.itinerary.ItineraryBean;
import com.buses.app.beans.state.StateBean;
import com.buses.app.resources.city.ICityResource;
import com.buses.app.resources.country.ICountryResource;
import com.buses.app.resources.itinerary.IItineraryResource;
import com.buses.app.resources.state.IStateResource;
import com.buses.app.services.base.BaseServiceImpl;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.itinerary.ItineraryDTO;
import com.buses.webservice.dto.itinerary.ItineraryResult;
import com.buses.webservice.dto.state.StateDTO;

public class ItineraryServiceImpl extends
		BaseServiceImpl<ItineraryBean, ItineraryDTO> implements
		IItineraryService {

	private final IItineraryResource _itineraryResource;
	private final ICityResource _cityResource;
	private final IStateResource _stateResource;
	private final ICountryResource _countryResource;

	public ItineraryServiceImpl(IItineraryResource itineraryResource,
			ICityResource cityResource, IStateResource stateResouce,
			ICountryResource countryResource) {
		_itineraryResource = itineraryResource;
		_cityResource = cityResource;
		_stateResource = stateResouce;
		_countryResource = countryResource;
	}

	@Override
	public ItineraryBean getById(Integer id) {
		return dtoToBean(_itineraryResource.getById(id));
	}

	@Override
	public ItineraryBean save(ItineraryBean bean) {
		return dtoToBean(_itineraryResource.save(beanToDTO(bean)));
	}

	@Override
	public boolean delete(Integer id) {
		return _itineraryResource.delete(id);
	}

	@Override
	public List<ItineraryBean> list() {
		ItineraryResult result = _itineraryResource.getAll();

		List<ItineraryBean> itinerarys = new ArrayList<ItineraryBean>();

		for (ItineraryDTO dto : result.getItinerarys())
			itinerarys.add(dtoToBean(dto));

		return itinerarys;
	}

	@Override
	protected ItineraryBean dtoToBean(ItineraryDTO dto) {
		if (dto == null || dto.getDestination() == null
				|| dto.getOrigin() == null)
			return null;
		ItineraryDTO id = dto;
		CityDTO originDTO = _cityResource.getById(id.getOrigin());
		CityDTO destinationDTO = _cityResource.getById(id.getDestination());
		StateDTO destinationStateDTO = _stateResource.getById(destinationDTO
				.getStateId());
		CountryDTO destinationCountryDTO = _countryResource
				.getById(destinationStateDTO.getCountryId());
		CountryBean destinationCountryBean = new CountryBean(
				destinationCountryDTO.getId(), destinationCountryDTO.getCode(),
				destinationCountryDTO.getName(),
				destinationCountryDTO.getDeletedAt());
		StateBean destinationStateBean = new StateBean(
				destinationStateDTO.getId(), destinationCountryBean,
				destinationStateDTO.getName(),
				destinationStateDTO.getDeletedAt());
		CityBean destination = new CityBean(destinationDTO.getId(),
				destinationStateBean, destinationDTO.getName(),
				destinationDTO.getDeletedAt());

		StateDTO originStateDTO = _stateResource
				.getById(originDTO.getStateId());
		CountryDTO originCountryDTO = _countryResource.getById(originStateDTO
				.getCountryId());
		CountryBean originCountryBean = new CountryBean(
				originCountryDTO.getId(), originCountryDTO.getCode(),
				originCountryDTO.getName(), originCountryDTO.getDeletedAt());
		StateBean originStateBean = new StateBean(originStateDTO.getId(),
				originCountryBean, originStateDTO.getName(),
				originStateDTO.getDeletedAt());
		CityBean origin = new CityBean(originDTO.getId(), originStateBean,
				originDTO.getName(), originDTO.getDeletedAt());
		ItineraryBean ib = new ItineraryBean(id.getId(), origin, destination,
				id.getDeletedAt());
		return ib;
	}

	@Override
	protected ItineraryDTO beanToDTO(ItineraryBean bean) {
		ItineraryDTO dto = new ItineraryDTO();
		dto.setId(bean.getId());
		dto.setOrigin(bean.getOrigin().getId());
		dto.setDestination(bean.getDestination().getId());
		dto.setDeletedAt(bean.getDeletedAt());
		return dto;
	}

	@Override
	public List<ItineraryBean> search(String params) {
		String value = this.getValue(params);
		List<ItineraryDTO> dtos = new ArrayList<ItineraryDTO>();
		Map<Integer, ItineraryDTO> hs = new LinkedHashMap<Integer, ItineraryDTO>();
		if (value != null) {
			List<CityDTO> cities = _cityResource.search(
					"value=" + value + "&fields=name").getCitys();
			for (CityDTO c : cities) {
				List<ItineraryDTO> its = _itineraryResource.search(
						"value=" + c.getId()
								+ "&fields=origin,destination&like=false")
						.getItinerarys();
				for (ItineraryDTO a : its) {
					hs.put(a.getId(), a);
				}
			}
			List<StateDTO> states = _stateResource.search(
					"value=" + value + "&fields=name").getStates();
			List<CountryDTO> countries = _countryResource.search(
					"value=" + value + "&fields=name,code").getCountrys();
			for (CountryDTO c : countries) {
				states.addAll(_stateResource.search(
						"value=" + c.getId() + "&fields=countryId").getStates());
			}
			for (StateDTO c : states) {
				List<CityDTO> scities = _cityResource.search(
						"value=" + c.getId() + "&fields=stateId&like=false")
						.getCitys();
				for (CityDTO sc : scities) {
					List<ItineraryDTO> its = _itineraryResource.search(
							"value=" + sc.getId()
									+ "&fields=origin,destination&like=false")
							.getItinerarys();
					for (ItineraryDTO a : its) {
						hs.put(a.getId(), a);
					}
				}
			}
			dtos.addAll(hs.values());
		} else {
			dtos = _itineraryResource.search(params).getItinerarys();
		}
		return resultToList(dtos);
	}

	@Override
	public Integer count(String p) {
		return search(p).size();
	}

}
