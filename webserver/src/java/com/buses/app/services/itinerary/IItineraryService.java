package com.buses.app.services.itinerary;

import com.buses.app.beans.itinerary.ItineraryBean;
import com.buses.app.services.base.IBaseService;
import com.buses.webservice.dto.itinerary.ItineraryDTO;

public interface IItineraryService extends
		IBaseService<ItineraryBean, ItineraryDTO> {

}
