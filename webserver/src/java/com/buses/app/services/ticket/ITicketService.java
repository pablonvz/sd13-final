package com.buses.app.services.ticket;

import java.util.List;

import com.buses.app.beans.ticket.TicketBean;
import com.buses.app.services.base.IBaseService;
import com.buses.webservice.dto.ticket.TicketDTO;

public interface ITicketService extends IBaseService<TicketBean, TicketDTO> {
	public List<Integer> getFreeSeats(Integer joureyId);
}
