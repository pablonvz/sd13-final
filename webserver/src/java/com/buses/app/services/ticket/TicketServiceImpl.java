package com.buses.app.services.ticket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.buses.app.beans.agency.AgencyBean;
import com.buses.app.beans.bus.BusBean;
import com.buses.app.beans.city.CityBean;
import com.buses.app.beans.client.ClientBean;
import com.buses.app.beans.country.CountryBean;
import com.buses.app.beans.driver.DriverBean;
import com.buses.app.beans.itinerary.ItineraryBean;
import com.buses.app.beans.journey.JourneyBean;
import com.buses.app.beans.person.PersonBean;
import com.buses.app.beans.state.StateBean;
import com.buses.app.beans.ticket.TicketBean;
import com.buses.app.resources.agency.IAgencyResource;
import com.buses.app.resources.bus.IBusResource;
import com.buses.app.resources.city.ICityResource;
import com.buses.app.resources.client.IClientResource;
import com.buses.app.resources.country.ICountryResource;
import com.buses.app.resources.driver.IDriverResource;
import com.buses.app.resources.itinerary.IItineraryResource;
import com.buses.app.resources.journey.IJourneyResource;
import com.buses.app.resources.person.IPersonResource;
import com.buses.app.resources.state.IStateResource;
import com.buses.app.resources.ticket.ITicketResource;
import com.buses.app.services.base.BaseServiceImpl;
import com.buses.webservice.dto.agency.AgencyDTO;
import com.buses.webservice.dto.bus.BusDTO;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.client.ClientDTO;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.driver.DriverDTO;
import com.buses.webservice.dto.itinerary.ItineraryDTO;
import com.buses.webservice.dto.journey.JourneyDTO;
import com.buses.webservice.dto.person.PersonDTO;
import com.buses.webservice.dto.state.StateDTO;
import com.buses.webservice.dto.ticket.TicketDTO;
import com.buses.webservice.dto.ticket.TicketResult;

public class TicketServiceImpl extends BaseServiceImpl<TicketBean, TicketDTO>
		implements ITicketService {

	private final ITicketResource _ticketResource;
	private final IClientResource _clientResource;
	private final IJourneyResource _journeyResource;
	private final IItineraryResource _itResource;
	private final IBusResource _busResource;
	private final IDriverResource _driverResource;
	private final IPersonResource _personResource;
	private final IAgencyResource _agencyResource;
	private final ICityResource _cityResource;
	private final IStateResource _stateResource;
	private final ICountryResource _countryResource;

	public TicketServiceImpl(ITicketResource ticketResource,
			IClientResource clientResource, IJourneyResource journeyResource,
			IItineraryResource itResource, IBusResource busResource,
			IDriverResource driverResource, IPersonResource personResource,
			IAgencyResource agencyResource, ICityResource cityResource,
			IStateResource stateResouce, ICountryResource countryResource) {
		_ticketResource = ticketResource;
		_clientResource = clientResource;
		_journeyResource = journeyResource;
		_itResource = itResource;
		_busResource = busResource;
		_driverResource = driverResource;
		_agencyResource = agencyResource;
		_cityResource = cityResource;
		_stateResource = stateResouce;
		_countryResource = countryResource;
		_personResource = personResource;
	}

	@Override
	public TicketBean getById(Integer id) {
		return dtoToBean(_ticketResource.getById(id));
	}

	@Override
	public TicketBean save(TicketBean bean) {
		return dtoToBean(_ticketResource.save(beanToDTO(bean)));
	}

	@Override
	public boolean delete(Integer id) {
		return _ticketResource.delete(id);
	}

	@Override
	public List<TicketBean> list() {
		TicketResult result = _ticketResource.getAll();

		List<TicketBean> tickets = new ArrayList<TicketBean>();

		for (TicketDTO dto : result.getTickets())
			tickets.add(dtoToBean(dto));

		return tickets;
	}

	public TicketBean dtoToBean(TicketDTO dto) {
		if (dto == null || dto.getClientId() == null
				|| dto.getJourneyId() == null)
			return null;
		ClientDTO cd = _clientResource.getById(dto.getClientId());
		PersonDTO pdto = _personResource.getById(cd.getPersonId());
		ClientBean cb = new ClientBean(cd.getId(), cd.getDeletedAt(),
				new PersonBean(pdto.getId(), pdto.getFirstName(),
						pdto.getLastName(), pdto.getBirthDate(), pdto.getSex(),
						pdto.getDocument(), pdto.getEmail(),
						pdto.getDeletedAt()));
		JourneyDTO jd = _journeyResource.getById(dto.getJourneyId());
		ItineraryDTO id = _itResource.getById(jd.getItineraryId());
		CityDTO originDTO = _cityResource.getById(id.getOrigin());
		CityDTO destinationDTO = _cityResource.getById(id.getDestination());
		StateDTO destinationStateDTO = _stateResource.getById(destinationDTO
				.getStateId());
		CountryDTO destinationCountryDTO = _countryResource
				.getById(destinationStateDTO.getCountryId());
		CountryBean destinationCountryBean = new CountryBean(
				destinationCountryDTO.getId(), destinationCountryDTO.getCode(),
				destinationCountryDTO.getName(),
				destinationCountryDTO.getDeletedAt());
		StateBean destinationStateBean = new StateBean(destinationDTO.getId(),
				destinationCountryBean, destinationDTO.getName(),
				destinationDTO.getDeletedAt());
		CityBean destination = new CityBean(destinationDTO.getId(),
				destinationStateBean, destinationDTO.getName(),
				destinationDTO.getDeletedAt());

		StateDTO originStateDTO = _stateResource
				.getById(originDTO.getStateId());
		CountryDTO originCountryDTO = _countryResource.getById(originStateDTO
				.getCountryId());
		CountryBean originCountryBean = new CountryBean(
				originCountryDTO.getId(), originCountryDTO.getCode(),
				originCountryDTO.getName(), originCountryDTO.getDeletedAt());
		StateBean originStateBean = new StateBean(originDTO.getId(),
				originCountryBean, originDTO.getName(),
				originDTO.getDeletedAt());
		CityBean origin = new CityBean(originDTO.getId(), originStateBean,
				originDTO.getName(), originDTO.getDeletedAt());
		ItineraryBean ib = new ItineraryBean(id.getId(), origin, destination,
				id.getDeletedAt());

		BusDTO bd = _busResource.getById(jd.getBusId());
		BusBean bb = new BusBean(bd.getId(), bd.getNumber(), bd.getCapacty(),
				bd.getModel(), bd.getFuel(), bd.getBrand(), bd.getYear(),
				bd.getMotor(), bd.getDeletedAt());

		DriverDTO dd = _driverResource.getById(jd.getDriverId());

		AgencyDTO agencyDTO = _agencyResource.getById(dd.getAgencyId());

		CityDTO cityDTO = _cityResource.getById(agencyDTO.getCityId());

		StateDTO stateDTO = _stateResource.getById(cityDTO.getStateId());

		CountryDTO countryDTO = _countryResource.getById(stateDTO
				.getCountryId());
		CountryBean countryBean = new CountryBean(countryDTO.getId(),
				countryDTO.getCode(), countryDTO.getName(),
				countryDTO.getDeletedAt());

		StateBean stateBean = new StateBean(stateDTO.getId(), countryBean,
				stateDTO.getName(), stateDTO.getDeletedAt());

		CityBean cityBean = new CityBean(cityDTO.getId(), stateBean,
				cityDTO.getName(), cityDTO.getDeletedAt());

		AgencyBean agencyBean = new AgencyBean(agencyDTO.getId(), cityBean,
				agencyDTO.getDescription(), agencyDTO.getDeletedAt());

		PersonDTO personDTO = _personResource.getById(dd.getId());
		PersonBean personBean = new PersonBean(personDTO.getId(),
				personDTO.getFirstName(), personDTO.getLastName(),
				personDTO.getBirthDate(), personDTO.getSex(),
				personDTO.getDocument(), personDTO.getEmail(),
				personDTO.getDeletedAt());

		DriverBean db = new DriverBean(dd.getId(), agencyBean, personBean,
				dd.getDeletedAt());

		JourneyBean jb = new JourneyBean(jd.getId(), ib, jd.getDate(), bb, db,
				jd.getPrice(), jd.getInitialKm(), jd.getEndKm(),
				jd.getIncident(), jd.getDeletedAt());
		return new TicketBean(dto.getId(), cb, jb, dto.getSeat(),
				dto.getStatus(), dto.getDeletedAt());
	}

	public TicketDTO beanToDTO(TicketBean bean) {
		TicketDTO dto = new TicketDTO();
		dto.setId(bean.getId());
		dto.setClientId(bean.getClient().getId());
		dto.setJourneyId(bean.getJourney().getId());
		dto.setSeat(bean.getSeat());
		dto.setStatus(bean.getStatus());
		dto.setDeletedAt(bean.getDeletedAt());
		return dto;
	}

	@Override
	public List<TicketBean> search(String params) {
		String value = this.getValue(params);
		Map<Integer, TicketDTO> hs = new LinkedHashMap<Integer, TicketDTO>();
		List<TicketDTO> dtos = new ArrayList<TicketDTO>();
		if (value != null) {
			List<PersonDTO> people = _personResource
					.search("value="
							+ value
							+ "&fields=birthDate,document,email,firstName,lastName,sex")
					.getPersons();
			for (PersonDTO c : people) {
				List<ClientDTO> clis = _clientResource.search(
						"value=" + c.getId() + "&fields=personId&like=false")
						.getClients();
				for (ClientDTO a : clis) {
					List<TicketDTO> tickets = _ticketResource.search(
							"value=" + a.getId()
									+ "&fields=clientId&like=false")
							.getTickets();
					for (TicketDTO t : tickets)
						hs.put(t.getId(), t);
				}
			}

		}
		List<TicketDTO> tckts = _ticketResource.search(params).getTickets();
		for (TicketDTO a : tckts)
			hs.put(a.getId(), a);
		dtos.addAll(hs.values());
		return resultToList(dtos);
	}

	@Override
	public Integer count(String params) {
		return search(params).size();
	}

	@Override
	public List<Integer> getFreeSeats(Integer journeyId) {
		List<TicketDTO> tickets = _ticketResource.search(
				"value=" + journeyId + "&fields=journeyId&like=false")
				.getTickets();
		JourneyDTO journey = _journeyResource.getById(journeyId);
		Map<Integer, Integer> reserved = new HashMap<Integer, Integer>();
		for (TicketDTO t : tickets) {
			if (!t.getStatus().equalsIgnoreCase("canceled"))
				reserved.put(t.getSeat(), t.getSeat());
		}
		List<Integer> free = new ArrayList<Integer>();
		BusDTO bus = _busResource.getById(journey.getBusId());
		int cap = bus.getCapacty();
		for (int i = 1; i <= cap; i++)
			if (reserved.get(i) == null)
				free.add(i);
		return free;
	}

}
