package com.buses.app.services.city;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.buses.app.beans.city.CityBean;
import com.buses.app.beans.country.CountryBean;
import com.buses.app.beans.state.StateBean;
import com.buses.app.resources.city.ICityResource;
import com.buses.app.resources.country.ICountryResource;
import com.buses.app.resources.state.IStateResource;
import com.buses.app.services.base.BaseServiceImpl;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.city.CityResult;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.state.StateDTO;

public class CityServiceImpl extends BaseServiceImpl<CityBean, CityDTO>
		implements ICityService {

	private final ICityResource _cityResource;
	private final IStateResource _stateResource;
	private final ICountryResource _countryResource;

	public CityServiceImpl(ICityResource cityResource,
			IStateResource stateResource, ICountryResource countryResource) {
		_cityResource = cityResource;
		_stateResource = stateResource;
		_countryResource = countryResource;
	}

	@Override
	public CityBean getById(Integer id) {
		return dtoToBean(_cityResource.getById(id));
	}

	@Override
	public CityBean save(CityBean bean) {
		return dtoToBean(_cityResource.save(beanToDTO(bean)));
	}

	@Override
	public boolean delete(Integer id) {
		return _cityResource.delete(id);
	}

	@Override
	public List<CityBean> list() {
		CityResult result = _cityResource.getAll();

		List<CityBean> citys = new ArrayList<CityBean>();

		for (CityDTO dto : result.getCitys())
			citys.add(dtoToBean(dto));

		return citys;
	}

	@Override
	protected CityBean dtoToBean(CityDTO cityDTO) {
		if (cityDTO == null || cityDTO.getStateId() == null)
			return null;
		StateDTO stateDTO = _stateResource.getById(cityDTO.getStateId());

		CountryDTO countryDTO = _countryResource.getById(stateDTO
				.getCountryId());
		CountryBean countryBean = new CountryBean(countryDTO.getId(),
				countryDTO.getCode(), countryDTO.getName(),
				countryDTO.getDeletedAt());

		StateBean stateBean = new StateBean(stateDTO.getId(), countryBean,
				stateDTO.getName(), stateDTO.getDeletedAt());

		CityBean cityBean = new CityBean(cityDTO.getId(), stateBean,
				cityDTO.getName(), cityDTO.getDeletedAt());
		return cityBean;
	}

	@Override
	protected CityDTO beanToDTO(CityBean bean) {
		CityDTO dto = new CityDTO();
		dto.setId(bean.getId());
		dto.setStateId(bean.getState().getId());
		dto.setName(bean.getName());
		dto.setDeletedAt(bean.getDeletedAt());
		return dto;
	}

	@Override
	public List<CityBean> search(String params) {
		String value = this.getValue(params);
		List<CityDTO> cityDtos = new ArrayList<CityDTO>();
		Map<Integer, CityDTO> cityM = new LinkedHashMap<Integer, CityDTO>();
		if (value != null) {
			List<StateDTO> states = _stateResource.search(
					"value=" + value + "&fields=name").getStates();
			for (StateDTO c : states) {
				List<CityDTO> cts = _cityResource.search(
						"value=" + c.getId() + "&fields=stateId&like=false")
						.getCitys();
				for (CityDTO a : cts) {
					cityM.put(a.getId(), a);
				}
			}

			List<CountryDTO> countries = _countryResource.search(
					"value=" + value + "&fields=name").getCountrys();
			for (CountryDTO c : countries) {
				List<StateDTO> statesC = _stateResource.search(
						"value=" + c.getId() + "&fields=countryId&like=false")
						.getStates();
				for (StateDTO s : statesC) {
					List<CityDTO> cts = _cityResource
							.search("value=" + s.getId()
									+ "&fields=stateId&like=false").getCitys();
					for (CityDTO a : cts) {
						cityM.put(a.getId(), a);
					}
				}
			}

		}
		List<CityDTO> cts = _cityResource.search(params).getCitys();
		for (CityDTO a : cts) {
			cityM.put(a.getId(), a);
		}
		cityDtos.addAll(cityM.values());
		return resultToList(cityDtos);
	}

	@Override
	public Integer count(String p) {
		return search(p).size();
	}

	@Override
	public List<CityBean> getMostPopularDestination(Date start, Date end) {
		return resultToList(_cityResource.getMostPopularDestination(start, end)
				.getCitys());
	}

	@Override
	public List<Integer> getMostPopularDestinationCount(Date start, Date end) {
		return _cityResource.getMostPopularDestinationCount(start, end)
				.getCounts();
	}

	@Override
	public List<CityBean> getMostPopularOrigin(Date start, Date end) {
		return resultToList(_cityResource.getMostPopularOrigin(start, end)
				.getCitys());
	}

	@Override
	public List<Integer> getMostPopularOriginCount(Date start, Date end) {
		return _cityResource.getMostPopularOriginCount(start, end).getCounts();
	}

	@Override
	public CityBean getCityWithMoreTickets(Date start, Date end, int id) {
		return dtoToBean(_cityResource.getCityWithMoreTickets(start, end, id));
	}

	@Override
	public Integer getCityWithMoreTicketsCount(Date start, Date end, int id) {
		return _cityResource.getCityWithMoreTicketsCount(start, end, id);
	}

}
