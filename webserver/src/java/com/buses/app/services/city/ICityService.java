package com.buses.app.services.city;

import java.util.Date;
import java.util.List;

import com.buses.app.beans.city.CityBean;
import com.buses.app.services.base.IBaseService;
import com.buses.webservice.dto.city.CityDTO;

public interface ICityService extends IBaseService<CityBean, CityDTO> {

	public List<CityBean> getMostPopularDestination(Date start, Date end);

	public List<Integer> getMostPopularDestinationCount(Date start, Date end);

	public List<CityBean> getMostPopularOrigin(Date start, Date end);

	public List<Integer> getMostPopularOriginCount(Date start, Date end);

	public CityBean getCityWithMoreTickets(Date start, Date end, int id);

	public Integer getCityWithMoreTicketsCount(Date start, Date end, int id);
}
