package com.buses.app.beans.city;

import java.util.Date;

import com.buses.app.beans.base.BaseBean;
import com.buses.app.beans.state.StateBean;

public class CityBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	private StateBean _state;
	private String _name;
	private Date _deleted_at;

	public CityBean(Integer id, StateBean state, String name, Date deleted_at) {
		super(id);
		_state = state;
		_name = name;
		_deleted_at = deleted_at;
	}

	public StateBean getState() {
		return _state;
	}

	public String getName() {
		return _name;
	}

	public Date getDeletedAt() {
		return _deleted_at;
	}
}
