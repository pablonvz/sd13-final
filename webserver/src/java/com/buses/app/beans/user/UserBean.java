package com.buses.app.beans.user;

import java.util.Date;

import com.buses.app.beans.agency.AgencyBean;
import com.buses.app.beans.base.BaseBean;
import com.buses.app.beans.person.PersonBean;

public class UserBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	private AgencyBean _agency;
	private String _password_hash;
	private Date _deleted_at;
	private PersonBean _person;

	public UserBean(Integer id, AgencyBean agency, PersonBean person,
			String password_hash, Date deleted_at) {
		super(id);
		_agency = agency;
		_person = person;
		_password_hash = password_hash;
		_deleted_at = deleted_at;
	}

	public AgencyBean getAgency() {
		return _agency;
	}

	public PersonBean getPerson() {
		return _person;
	}

	public String getPasswordHash() {
		return _password_hash;
	}

	public Date getDeletedAt() {
		return _deleted_at;
	}
}
