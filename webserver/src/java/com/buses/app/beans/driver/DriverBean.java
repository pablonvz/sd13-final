package com.buses.app.beans.driver;

import java.util.Date;

import com.buses.app.beans.agency.AgencyBean;
import com.buses.app.beans.base.BaseBean;
import com.buses.app.beans.person.PersonBean;

public class DriverBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	private AgencyBean _agency;
	private Date _deleted_at;
	private PersonBean _person;

	public DriverBean(Integer id, AgencyBean agency, PersonBean person,
			Date deleted_at) {
		super(id);
		_agency = agency;
		_person = person;
		_deleted_at = deleted_at;
	}

	public AgencyBean getAgency() {
		return _agency;
	}

	public PersonBean getPerson() {
		return _person;
	}

	public Date getDeletedAt() {
		return _deleted_at;
	}
}
