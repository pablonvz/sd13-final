package com.buses.app.beans.ticket;

import java.util.Date;

import com.buses.app.beans.base.BaseBean;
import com.buses.app.beans.client.ClientBean;
import com.buses.app.beans.journey.JourneyBean;

public class TicketBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	private ClientBean _client;

	private JourneyBean _journey;
	private Integer _seat;
	private String _status;
	private Date _deleted_at;

	public TicketBean(Integer id, ClientBean clientBean, JourneyBean journey,
			Integer seat, String status, Date deleted_at) {
		super(id);
		_client = clientBean;
		_journey = journey;
		_seat = seat;
		_status = status;
		_deleted_at = deleted_at;
	}

	public ClientBean getClient() {
		return _client;
	}

	public JourneyBean getJourney() {
		return _journey;
	}

	public Integer getSeat() {
		return _seat;
	}

	public String getStatus() {
		return _status;
	}

	public Date getDeletedAt() {
		return _deleted_at;
	}
}
