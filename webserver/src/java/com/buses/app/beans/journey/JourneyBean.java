package com.buses.app.beans.journey;

import java.util.Date;

import com.buses.app.beans.base.BaseBean;
import com.buses.app.beans.bus.BusBean;
import com.buses.app.beans.driver.DriverBean;
import com.buses.app.beans.itinerary.ItineraryBean;

public class JourneyBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	private ItineraryBean _itinerary;
	private BusBean _bus;
	private DriverBean _driver;
	private Integer _price;
	private Double _initial_km;
	private Double _end_km;
	private String _incident;
	private Date _deleted_at;
	private Date _date;

	public JourneyBean(Integer id, ItineraryBean itinerary, Date date,
			BusBean bus, DriverBean driver, Integer price, Double initial_km,
			Double end_km, String incident, Date deleted_at) {
		super(id);
		_itinerary = itinerary;
		_bus = bus;
		_date = date;
		_driver = driver;
		_price = price;
		_initial_km = initial_km;
		_end_km = end_km;
		_incident = incident;
		_deleted_at = deleted_at;
	}

	public ItineraryBean getItinerary() {
		return _itinerary;
	}

	public Date getDate() {
		return _date;
	}

	public BusBean getBus() {
		return _bus;
	}

	public DriverBean getDriver() {
		return _driver;
	}

	public Integer getPrice() {
		return _price;
	}

	public Double getInitialKm() {
		return _initial_km;
	}

	public Double getEndKm() {
		return _end_km;
	}

	public String getIncident() {
		return _incident;
	}

	public Date getDeletedAt() {
		return _deleted_at;
	}
}
