package com.buses.app.beans.state;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.buses.app.beans.base.BaseBean;
import com.buses.app.beans.country.CountryBean;
import com.buses.app.services.country.ICountryService;

public class StateBean extends BaseBean {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ICountryService countryService;
	private CountryBean _country;
	private String _name;
	private Date _deletedAt;

	public StateBean(Integer id, CountryBean country, String name,
			Date deletedAt) {
		super(id);

		_country = country;
		_name = name;
		_deletedAt = deletedAt;
	}

	public CountryBean getCountry() {
		return _country;
	}

	public String getName() {
		return _name;
	}

	public Date getDeletedAt() {
		return _deletedAt;
	}
}
