package com.buses.app.beans.itinerary;

import java.util.Date;

import com.buses.app.beans.base.BaseBean;
import com.buses.app.beans.city.CityBean;

public class ItineraryBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	private CityBean _origin;
	private CityBean _destination;
	private Date _deleted_at;

	public ItineraryBean(Integer id, CityBean origin, CityBean destination,
			Date deleted_at) {
		super(id);
		_origin = origin;
		_destination = destination;
		_deleted_at = deleted_at;
	}

	public CityBean getOrigin() {
		return _origin;
	}

	public CityBean getDestination() {
		return _destination;
	}

	public Date getDeletedAt() {
		return _deleted_at;
	}
}
