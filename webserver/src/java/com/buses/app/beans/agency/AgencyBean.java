package com.buses.app.beans.agency;

import java.util.Date;

import com.buses.app.beans.base.BaseBean;
import com.buses.app.beans.city.CityBean;

public class AgencyBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	private CityBean _city;
	private String _description;
	private Date _deleted_at;

	public AgencyBean(Integer id, CityBean city, String description,
			Date deletedAt) {
		super(id);
		_city = city;
		_description = description;
		_deleted_at = deletedAt;
	}

	public CityBean getCity() {
		return _city;
	}

	public String getDescription() {
		return _description;
	}

	public Date getDeletedAt() {
		return _deleted_at;
	}
}
