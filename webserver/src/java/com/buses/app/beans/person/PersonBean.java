package com.buses.app.beans.person;

import java.util.Date;

import com.buses.app.beans.base.BaseBean;

public class PersonBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	private String _first_name;
	private String _last_name;
	private Date _birth_date;
	private String _sex;
	private String _document;
	private String _email;
	private Date _deleted_at;

	public PersonBean(Integer id, String first_name, String last_name,
			Date birth_date, String sex, String document, String email,
			Date deleted_at) {
		super(id);
		_first_name = first_name;
		_last_name = last_name;
		_birth_date = birth_date;
		_sex = sex;
		_document = document;
		_email = email;
		_deleted_at = deleted_at;
	}

	public String getFirstName() {
		return _first_name;
	}

	public String getLastName() {
		return _last_name;
	}

	public Date getBirthDate() {
		return _birth_date;
	}

	public String getSex() {
		return _sex;
	}

	public String getDocument() {
		return _document;
	}

	public String getEmail() {
		return _email;
	}

	public Date getDeletedAt() {
		return _deleted_at;
	}
}
