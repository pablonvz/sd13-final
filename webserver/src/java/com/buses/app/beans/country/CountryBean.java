package com.buses.app.beans.country;

import java.util.Date;

import com.buses.app.beans.base.BaseBean;

public class CountryBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	private String _code;
	private String _name;
	private Date _deleted_at;

	public CountryBean(Integer id, String code, String name, Date deleted_at) {
		super(id);
		_code = code;
		_name = name;
		_deleted_at = deleted_at;
	}

	public String getCode() {
		return _code;
	}

	public String getName() {
		return _name;
	}

	public Date getDeletedAt() {
		return _deleted_at;
	}
}
