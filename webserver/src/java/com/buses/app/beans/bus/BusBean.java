package com.buses.app.beans.bus;

import java.util.Date;

import com.buses.app.beans.base.BaseBean;

public class BusBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	private Integer _capacty;
	private String _model;
	private String _fuel;
	private String _brand;
	private Date _year;
	private String _motor;
	private Date _deleted_at;
	private Integer _number;

	public BusBean(Integer id, Integer number, Integer capacty, String model,
			String fuel, String brand, Date year, String motor, Date deleted_at) {
		super(id);
		setNumber(number);
		_capacty = capacty;
		_model = model;
		_fuel = fuel;
		_brand = brand;
		_year = year;
		_motor = motor;
		_deleted_at = deleted_at;
	}

	public Integer getCapacty() {
		return _capacty;
	}

	public String getModel() {
		return _model;
	}

	public String getFuel() {
		return _fuel;
	}

	public String getBrand() {
		return _brand;
	}

	public Date getYear() {
		return _year;
	}

	public String getMotor() {
		return _motor;
	}

	public Date getDeletedAt() {
		return _deleted_at;
	}

	public Integer getNumber() {
		return _number;
	}

	public void setNumber(Integer _number) {
		this._number = _number;
	}
}
