package com.buses.app.beans.base;

import java.io.Serializable;

public abstract class BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private final Integer _id;

	public BaseBean(Integer id) {
		_id = id;
	}

	public Integer getVersion() {
		return 1;
	}

	public Integer getId() {
		return _id;
	}
}
