package com.buses.app.beans.client;

import java.util.Date;

import com.buses.app.beans.base.BaseBean;
import com.buses.app.beans.person.PersonBean;

public class ClientBean extends BaseBean {

	private static final long serialVersionUID = 1L;

	private PersonBean _person;
	private Date _deleted_at;

	public ClientBean(Integer id, Date deleted_at, PersonBean person) {
		super(id);
		_deleted_at = deleted_at;
		_person = person;
	}

	public PersonBean getPerson() {
		return _person;
	}

	public Date getDeletedAt() {
		return _deleted_at;
	}

}
