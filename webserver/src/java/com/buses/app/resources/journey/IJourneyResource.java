package com.buses.app.resources.journey;

import com.buses.app.resources.base.IBaseResource;
import com.buses.webservice.dto.journey.JourneyDTO;
import com.buses.webservice.dto.journey.JourneyResult;

public interface IJourneyResource extends
		IBaseResource<JourneyDTO, JourneyResult> {

}
