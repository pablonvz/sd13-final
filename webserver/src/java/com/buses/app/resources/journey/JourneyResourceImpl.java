package com.buses.app.resources.journey;

import com.buses.app.resources.base.BaseResourceImpl;
import com.buses.webservice.dto.journey.JourneyDTO;
import com.buses.webservice.dto.journey.JourneyResult;

public class JourneyResourceImpl extends
		BaseResourceImpl<JourneyDTO, JourneyResult> implements IJourneyResource {

	// @Autowired
	// CacheManager cacheManager;

	public JourneyResourceImpl() {
		super(JourneyDTO.class, "journey");
	}

	@Override
	public JourneyDTO save(JourneyDTO dto) {
		if (null == dto.getId()) {
			// cacheManager.getCache(CACHE_NAME).evict("journey" + dto.getId());
		}
		return super.save(dto);
	}

	@Override
	// @CacheEvict(value = CACHE_NAME, key = "#id")
	public boolean delete(Integer id) {
		return super.delete(id);
	}

	@Override
	// @Cacheable(value = CACHE_NAME, key = "#id")
	public JourneyDTO getById(Integer id) {
		return super.getById(id);
	}

	@Override
	public JourneyResult getAll() {
		final JourneyResult result = getWebResource().get(JourneyResult.class);
		return result;
	}

	@Override
	public JourneyResult search(String params) {
		return this.searchWithResult(params, JourneyResult.class);
	}
}
