package com.buses.app.resources.city;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.buses.app.resources.base.BaseResourceImpl;
import com.buses.webservice.dto.base.CountResult;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.city.CityResult;

public class CityResourceImpl extends BaseResourceImpl<CityDTO, CityResult>
		implements ICityResource {

	private SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");

	public CityResourceImpl() {
		super(CityDTO.class, "city");
	}

	@Override
	public CityResult getAll() {
		final CityResult result = getWebResource().get(CityResult.class);
		return result;
	}

	@Override
	public CityResult search(String params) {
		return this.searchWithResult(params, CityResult.class);
	}

	@Override
	public CityResult getMostPopularDestination(Date start, Date end) {
		return getWebResource().path(
				"/most-popular/kind=destination&between=" + sp.format(start)
						+ "," + sp.format(end)).get(CityResult.class);
	}

	@Override
	public CountResult getMostPopularDestinationCount(Date start, Date end) {
		SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
		return getWebResource().path(
				"/most-popular-count/kind=destination&between="
						+ sp.format(start) + "," + sp.format(end)).get(
				CountResult.class);
	}

	@Override
	public CityResult getMostPopularOrigin(Date start, Date end) {
		return getWebResource().path(
				"/most-popular/kind=origin&between=" + sp.format(start) + ","
						+ sp.format(end)).get(CityResult.class);
	}

	@Override
	public CountResult getMostPopularOriginCount(Date start, Date end) {
		return getWebResource().path(
				"/most-popular-count/kind=origin&between=" + sp.format(start)
						+ "," + sp.format(end)).get(CountResult.class);
	}

	@Override
	public CityDTO getCityWithMoreTickets(Date start, Date end, int id) {
		return getWebResource().path(
				"/with-more-tickets/between=" + sp.format(start) + ","
						+ sp.format(end) + "&id=" + id).get(CityDTO.class);
	}

	@Override
	public Integer getCityWithMoreTicketsCount(Date start, Date end, int id) {
		return Integer.parseInt(getWebResource().path(
				"/with-more-tickets-count/between=" + sp.format(start) + ","
						+ sp.format(end) + "&id=" + id).get(String.class));
	}

}
