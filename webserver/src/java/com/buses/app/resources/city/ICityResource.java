package com.buses.app.resources.city;

import java.util.Date;

import com.buses.app.resources.base.IBaseResource;
import com.buses.webservice.dto.base.CountResult;
import com.buses.webservice.dto.city.CityDTO;
import com.buses.webservice.dto.city.CityResult;

public interface ICityResource extends IBaseResource<CityDTO, CityResult> {

	public CityResult getMostPopularDestination(Date start, Date end);

	public CountResult getMostPopularDestinationCount(Date start, Date end);

	public CityResult getMostPopularOrigin(Date start, Date end);

	public CountResult getMostPopularOriginCount(Date start, Date end);

	public CityDTO getCityWithMoreTickets(Date start, Date end, int id);

	public Integer getCityWithMoreTicketsCount(Date start, Date end, int id);
}
