package com.buses.app.resources.country;

import com.buses.app.resources.base.IBaseResource;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.country.CountryResult;

public interface ICountryResource extends
		IBaseResource<CountryDTO, CountryResult> {

}
