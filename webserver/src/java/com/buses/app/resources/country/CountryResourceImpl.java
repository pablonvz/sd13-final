package com.buses.app.resources.country;

import com.buses.app.resources.base.BaseResourceImpl;
import com.buses.webservice.dto.country.CountryDTO;
import com.buses.webservice.dto.country.CountryResult;

public class CountryResourceImpl extends
		BaseResourceImpl<CountryDTO, CountryResult> implements ICountryResource {

	public CountryResourceImpl() {
		super(CountryDTO.class, "country");
	}

	@Override
	public CountryDTO save(CountryDTO dto) {
		return super.save(dto);
	}

	@Override
	public boolean delete(Integer id) {
		return super.delete(id);
	}

	@Override
	public CountryDTO getById(Integer id) {
		return super.getById(id);
	}

	@Override
	public CountryResult getAll() {
		final CountryResult result = getWebResource().get(CountryResult.class);
		return result;
	}

	@Override
	public CountryResult search(String params) {
		return this.searchWithResult(params, CountryResult.class);
	}
}
