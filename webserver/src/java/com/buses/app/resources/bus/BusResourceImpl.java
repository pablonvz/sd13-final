package com.buses.app.resources.bus;

import com.buses.app.resources.base.BaseResourceImpl;
import com.buses.webservice.dto.bus.BusDTO;
import com.buses.webservice.dto.bus.BusResult;

public class BusResourceImpl extends BaseResourceImpl<BusDTO, BusResult>
		implements IBusResource {

	// @Autowired
	// CacheManager cacheManager;

	public BusResourceImpl() {
		super(BusDTO.class, "bus");
	}

	@Override
	public BusDTO save(BusDTO dto) {
		// if (null == dto.getId()) {
		// cacheManager.getCache(CACHE_NAME).evict("bus" + dto.getId());
		// }
		return super.save(dto);
	}

	@Override
	// @CacheEvict(value = CACHE_NAME, key = "#id")
	public boolean delete(Integer id) {
		return super.delete(id);
	}

	@Override
	// @Cacheable(value = CACHE_NAME, key = "#id")
	public BusDTO getById(Integer id) {
		return super.getById(id);
	}

	@Override
	public BusResult getAll() {
		final BusResult result = getWebResource().get(BusResult.class);
		return result;
	}

	@Override
	public BusResult search(String params) {
		return this.searchWithResult(params, BusResult.class);
	}
}
