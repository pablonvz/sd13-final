package com.buses.app.resources.bus;

import com.buses.app.resources.base.IBaseResource;
import com.buses.webservice.dto.bus.BusDTO;
import com.buses.webservice.dto.bus.BusResult;

public interface IBusResource extends IBaseResource<BusDTO, BusResult> {

}
