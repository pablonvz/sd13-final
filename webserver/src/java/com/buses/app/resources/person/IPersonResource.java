package com.buses.app.resources.person;

import com.buses.app.resources.base.IBaseResource;
import com.buses.webservice.dto.person.PersonDTO;
import com.buses.webservice.dto.person.PersonResult;

public interface IPersonResource extends IBaseResource<PersonDTO, PersonResult> {

	PersonResult getByEmail(String email);

}
