package com.buses.app.resources.person;

import com.buses.app.resources.base.BaseResourceImpl;
import com.buses.webservice.dto.person.PersonDTO;
import com.buses.webservice.dto.person.PersonResult;

public class PersonResourceImpl extends
		BaseResourceImpl<PersonDTO, PersonResult> implements IPersonResource {

	// @Autowired
	// CacheManager cacheManager;

	public PersonResourceImpl() {
		super(PersonDTO.class, "person");
	}

	@Override
	public PersonDTO save(PersonDTO dto) {
		if (null == dto.getId()) {
			// cacheManager.getCache(CACHE_NAME).evict("person" + dto.getId());
		}
		return super.save(dto);
	}

	@Override
	// @CacheEvict(value = CACHE_NAME, key = "#id")
	public boolean delete(Integer id) {
		return super.delete(id);
	}

	@Override
	// @Cacheable(value = CACHE_NAME, key = "#id")
	public PersonDTO getById(Integer id) {
		return super.getById(id);
	}

	@Override
	public PersonResult getAll() {
		final PersonResult result = getWebResource().get(PersonResult.class);
		return result;
	}

	@Override
	public PersonResult getByEmail(String email) {
		return getWebResource().path("/email/" + email).get(PersonResult.class);
	}

	@Override
	public PersonResult search(String params) {
		return this.searchWithResult(params, PersonResult.class);
	}
}
