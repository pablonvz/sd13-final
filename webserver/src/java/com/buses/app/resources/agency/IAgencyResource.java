package com.buses.app.resources.agency;

import com.buses.app.resources.base.IBaseResource;
import com.buses.webservice.dto.agency.AgencyDTO;
import com.buses.webservice.dto.agency.AgencyResult;

public interface IAgencyResource extends IBaseResource<AgencyDTO, AgencyResult> {

}
