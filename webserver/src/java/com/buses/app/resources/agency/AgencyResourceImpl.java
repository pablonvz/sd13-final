package com.buses.app.resources.agency;

import com.buses.app.resources.base.BaseResourceImpl;
import com.buses.webservice.dto.agency.AgencyDTO;
import com.buses.webservice.dto.agency.AgencyResult;

public class AgencyResourceImpl extends
		BaseResourceImpl<AgencyDTO, AgencyResult> implements IAgencyResource {
	// @Autowired
	// CacheManager cacheManager;

	public AgencyResourceImpl() {
		super(AgencyDTO.class, "agency");
	}

	@Override
	public AgencyDTO save(AgencyDTO dto) {
		// if (null == dto.getId()) {
		// cacheManager.getCache(CACHE_NAME).evict("agency" + dto.getId());
		// }
		return super.save(dto);
	}

	@Override
	// @CacheEvict(value = CACHE_NAME, key = "#id")
	public boolean delete(Integer id) {
		return super.delete(id);
	}

	@Override
	// @Cacheable(value = CACHE_NAME, key = "#id")
	public AgencyDTO getById(Integer id) {
		return super.getById(id);
	}

	@Override
	public AgencyResult getAll() {
		final AgencyResult result = getWebResource().get(AgencyResult.class);
		return result;
	}

	@Override
	public AgencyResult search(String params) {
		return this.searchWithResult(params, AgencyResult.class);
	}

}
