package com.buses.app.resources.ticket;

import com.buses.app.resources.base.BaseResourceImpl;
import com.buses.webservice.dto.ticket.TicketDTO;
import com.buses.webservice.dto.ticket.TicketResult;

public class TicketResourceImpl extends
		BaseResourceImpl<TicketDTO, TicketResult> implements ITicketResource {

	// @Autowired
	// CacheManager cacheManager;

	public TicketResourceImpl() {
		super(TicketDTO.class, "ticket");
	}

	@Override
	public TicketDTO save(TicketDTO dto) {
		if (null == dto.getId()) {
			// cacheManager.getCache(CACHE_NAME).evict("ticket" + dto.getId());
		}
		return super.save(dto);
	}

	@Override
	// @CacheEvict(value = CACHE_NAME, key = "#id")
	public boolean delete(Integer id) {
		return super.delete(id);
	}

	@Override
	// @Cacheable(value = CACHE_NAME, key = "#id")
	public TicketDTO getById(Integer id) {
		return super.getById(id);
	}

	@Override
	public TicketResult getAll() {
		final TicketResult result = getWebResource().get(TicketResult.class);
		return result;
	}

	@Override
	public TicketResult search(String params) {
		return this.searchWithResult(params, TicketResult.class);
	}
}
