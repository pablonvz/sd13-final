package com.buses.app.resources.ticket;

import com.buses.app.resources.base.IBaseResource;
import com.buses.webservice.dto.ticket.TicketDTO;
import com.buses.webservice.dto.ticket.TicketResult;

public interface ITicketResource extends IBaseResource<TicketDTO, TicketResult> {

}
