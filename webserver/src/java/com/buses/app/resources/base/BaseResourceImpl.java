package com.buses.app.resources.base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;

import com.buses.webservice.dto.base.BaseDTO;
import com.buses.webservice.dto.base.BaseResult;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public abstract class BaseResourceImpl<DTO extends BaseDTO, RESULT extends BaseResult<DTO>>
		implements IBaseResource<DTO, RESULT> {

	private final String _resourcePath;
	private final String _cachePath;
	private final Class<DTO> _dtoClass;
	private final WebResource _webResource;
	
	private final static Properties prop = new Properties();
	private static String BASE_URL = null;

	protected static final String CACHE_NAME = "buses-cache";

	@Autowired
	CacheManager cacheManager;

	public BaseResourceImpl(Class<DTO> dtoClass, String resourcePath) {
		_dtoClass = dtoClass;
		_cachePath = resourcePath + "#";
		
		// Load base url
		if(BASE_URL == null){
			try {
				prop.load(new FileInputStream("configuration.properties"));
				BASE_URL = prop.getProperty("base_url");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		_resourcePath = BASE_URL + "/" + resourcePath;

		final Client jerseyClient = Client.create();

		_webResource = jerseyClient.resource(_resourcePath);
	}

	protected WebResource getWebResource() {
		return _webResource;
	}

	protected Class<DTO> getDtoClass() {
		return _dtoClass;
	}

	public String cacheId(Integer id) {
		return _cachePath + id;
	}

	@Override
	public DTO save(DTO dto) {
		return putInCache(removeFromCache(getWebResource().entity(dto).post(
				getDtoClass())));
	}

	@SuppressWarnings("unchecked")
	@Override
	public DTO getById(Integer id) {
		DTO dto = null;
		ValueWrapper vw = cacheManager.getCache(CACHE_NAME).get(cacheId(id));
		if (vw == null) {
			dto = getWebResource().path("/" + id).get(getDtoClass());
			putInCache(dto);
		} else {
			dto = (DTO) vw.get();
		}
		return dto;
	}

	private DTO putInCache(DTO dto) {
		cacheManager.getCache(CACHE_NAME).put(cacheId(dto.getId()), dto);
		return dto;
	}

	private DTO removeFromCache(DTO dto) {
		cacheManager.getCache(CACHE_NAME).evict(dto);
		return dto;
	}

	@Override
	public boolean delete(Integer id) {
		removeFromCache(getById(id));
		getWebResource().path("/" + id).delete(getDtoClass());
		return true;
	}

	protected RESULT searchWithResult(String params, Class<RESULT> resultClass) {
		return getWebResource().path("/search/" + params).get(resultClass);
	}

	public Integer count(String params) {
		return Integer.parseInt(getWebResource().path("/count/" + params).get(
				String.class));
	}
}