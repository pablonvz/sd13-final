package com.buses.app.resources.base;

import com.buses.webservice.dto.base.BaseDTO;
import com.buses.webservice.dto.base.BaseResult;

public interface IBaseResource<DTO extends BaseDTO, RESULT extends BaseResult<DTO>> {
	public DTO getById(Integer id);

	public DTO save(DTO dto);

	public boolean delete(Integer id);

	public RESULT getAll();

	public RESULT search(String params);

	public Integer count(String params);
}
