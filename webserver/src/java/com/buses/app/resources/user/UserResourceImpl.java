package com.buses.app.resources.user;

import com.buses.app.resources.base.BaseResourceImpl;
import com.buses.webservice.dto.user.UserDTO;
import com.buses.webservice.dto.user.UserResult;

public class UserResourceImpl extends BaseResourceImpl<UserDTO, UserResult>
		implements IUserResource {

	// @Autowired
	// CacheManager cacheManager;

	public UserResourceImpl() {
		super(UserDTO.class, "user");
	}

	@Override
	public UserDTO save(UserDTO dto) {
		if (null == dto.getId()) {
			// cacheManager.getCache(CACHE_NAME).evict("user" + dto.getId());
		}
		return super.save(dto);
	}

	@Override
	// @CacheEvict(value = CACHE_NAME, key = "#id")
	public boolean delete(Integer id) {
		return super.delete(id);
	}

	@Override
	// @Cacheable(value = CACHE_NAME, key = "#id")
	public UserDTO getById(Integer id) {
		return super.getById(id);
	}

	@Override
	public UserResult getAll() {
		final UserResult result = getWebResource().get(UserResult.class);
		return result;
	}

	@Override
	public UserResult getByPersonId(Integer id) {
		return getWebResource().path("/person/" + id).get(UserResult.class);
	}

	@Override
	public UserResult search(String params) {
		return this.searchWithResult(params, UserResult.class);
	}
}
