package com.buses.app.resources.user;

import com.buses.app.resources.base.IBaseResource;
import com.buses.webservice.dto.user.UserDTO;
import com.buses.webservice.dto.user.UserResult;

public interface IUserResource extends IBaseResource<UserDTO, UserResult> {

	UserResult getByPersonId(Integer id);
}
