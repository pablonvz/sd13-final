package com.buses.app.resources.state;

import com.buses.app.resources.base.BaseResourceImpl;
import com.buses.webservice.dto.state.StateDTO;
import com.buses.webservice.dto.state.StateResult;

public class StateResourceImpl extends BaseResourceImpl<StateDTO, StateResult>
		implements IStateResource {

	// @Autowired
	// CacheManager cacheManager;

	public StateResourceImpl() {
		super(StateDTO.class, "state");
	}

	@Override
	public StateDTO save(StateDTO dto) {
		if (null == dto.getId()) {
			// cacheManager.getCache(CACHE_NAME).evict("state" + dto.getId());
		}
		return super.save(dto);
	}

	@Override
	// @CacheEvict(value = CACHE_NAME, key = "#id")
	public boolean delete(Integer id) {
		return super.delete(id);
	}

	@Override
	// @Cacheable(value = CACHE_NAME, key = "#id")
	public StateDTO getById(Integer id) {
		return super.getById(id);
	}

	@Override
	public StateResult getAll() {
		final StateResult result = getWebResource().get(StateResult.class);
		return result;
	}

	@Override
	public StateResult search(String params) {
		return this.searchWithResult(params, StateResult.class);
	}
}
