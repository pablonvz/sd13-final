package com.buses.app.resources.state;

import com.buses.app.resources.base.IBaseResource;
import com.buses.webservice.dto.state.StateDTO;
import com.buses.webservice.dto.state.StateResult;

public interface IStateResource extends IBaseResource<StateDTO, StateResult> {

}
