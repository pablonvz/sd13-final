package com.buses.app.resources.driver;

import com.buses.app.resources.base.BaseResourceImpl;
import com.buses.webservice.dto.driver.DriverDTO;
import com.buses.webservice.dto.driver.DriverResult;

public class DriverResourceImpl extends
		BaseResourceImpl<DriverDTO, DriverResult> implements IDriverResource {

	// @Autowired
	// CacheManager cacheManager;

	public DriverResourceImpl() {
		super(DriverDTO.class, "driver");
	}

	@Override
	public DriverDTO save(DriverDTO dto) {
		if (null == dto.getId()) {
			// cacheManager.getCache(CACHE_NAME).evict("driver" + dto.getId());
		}
		return super.save(dto);
	}

	@Override
	// @CacheEvict(value = CACHE_NAME, key = "#id")
	public boolean delete(Integer id) {
		return super.delete(id);
	}

	@Override
	// @Cacheable(value = CACHE_NAME, key = "#id")
	public DriverDTO getById(Integer id) {
		return super.getById(id);
	}

	@Override
	public DriverResult getAll() {
		final DriverResult result = getWebResource().get(DriverResult.class);
		return result;
	}

	@Override
	public DriverResult search(String params) {
		return this.searchWithResult(params, DriverResult.class);
	}

}
