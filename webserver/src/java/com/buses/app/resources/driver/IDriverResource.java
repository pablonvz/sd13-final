package com.buses.app.resources.driver;

import com.buses.app.resources.base.IBaseResource;
import com.buses.webservice.dto.driver.DriverDTO;
import com.buses.webservice.dto.driver.DriverResult;

public interface IDriverResource extends IBaseResource<DriverDTO, DriverResult> {

}
