package com.buses.app.resources.itinerary;

import com.buses.app.resources.base.IBaseResource;
import com.buses.webservice.dto.itinerary.ItineraryDTO;
import com.buses.webservice.dto.itinerary.ItineraryResult;

public interface IItineraryResource extends
		IBaseResource<ItineraryDTO, ItineraryResult> {

}
