package com.buses.app.resources.itinerary;

import com.buses.app.resources.base.BaseResourceImpl;
import com.buses.webservice.dto.itinerary.ItineraryDTO;
import com.buses.webservice.dto.itinerary.ItineraryResult;

public class ItineraryResourceImpl extends
		BaseResourceImpl<ItineraryDTO, ItineraryResult> implements
		IItineraryResource {

	// @Autowired
	// CacheManager cacheManager;

	public ItineraryResourceImpl() {
		super(ItineraryDTO.class, "itinerary");
	}

	@Override
	public ItineraryDTO save(ItineraryDTO dto) {
		if (null == dto.getId()) {
			// cacheManager.getCache(CACHE_NAME).evict("itinerary" +
			// dto.getId());
		}
		return super.save(dto);
	}

	@Override
	// @CacheEvict(value = CACHE_NAME, key = "#id")
	public boolean delete(Integer id) {
		return super.delete(id);
	}

	@Override
	// @Cacheable(value = CACHE_NAME, key = "#id")
	public ItineraryDTO getById(Integer id) {
		return super.getById(id);
	}

	@Override
	public ItineraryResult getAll() {
		final ItineraryResult result = getWebResource().get(
				ItineraryResult.class);
		return result;
	}

	@Override
	public ItineraryResult search(String params) {
		return this.searchWithResult(params, ItineraryResult.class);
	}
}
