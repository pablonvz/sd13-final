package com.buses.app.resources.client;

import com.buses.app.resources.base.BaseResourceImpl;
import com.buses.webservice.dto.client.ClientDTO;
import com.buses.webservice.dto.client.ClientResult;

public class ClientResourceImpl extends
		BaseResourceImpl<ClientDTO, ClientResult> implements IClientResource {

	// @Autowired
	// CacheManager cacheManager;

	public ClientResourceImpl() {
		super(ClientDTO.class, "client");
	}

	@Override
	public ClientDTO save(ClientDTO dto) {
		// if (null == dto.getId()) {
		// cacheManager.getCache(CACHE_NAME).evict("client" + dto.getId());
		// }
		return super.save(dto);
	}

	@Override
	// @CacheEvict(value = CACHE_NAME, key = "#id")
	public boolean delete(Integer id) {
		return super.delete(id);
	}

	@Override
	// @Cacheable(value = CACHE_NAME, key = "#id")
	public ClientDTO getById(Integer id) {
		return super.getById(id);
	}

	@Override
	public ClientResult getAll() {
		final ClientResult result = getWebResource().get(ClientResult.class);
		return result;
	}

	@Override
	public ClientResult search(String params) {
		return this.searchWithResult(params, ClientResult.class);
	}
}
