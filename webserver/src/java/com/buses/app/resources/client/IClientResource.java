package com.buses.app.resources.client;

import com.buses.app.resources.base.IBaseResource;
import com.buses.webservice.dto.client.ClientDTO;
import com.buses.webservice.dto.client.ClientResult;

public interface IClientResource extends IBaseResource<ClientDTO, ClientResult> {

}
