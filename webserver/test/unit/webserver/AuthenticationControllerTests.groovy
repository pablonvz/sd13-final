package webserver



import org.junit.*
import grails.test.mixin.*

/**
 * AuthenticationControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(AuthenticationController)
@Mock(Authentication)
class AuthenticationControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/authentication/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.authenticationInstanceList.size() == 0
        assert model.authenticationInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.authenticationInstance != null
    }

    void testSave() {
        controller.save()

        assert model.authenticationInstance != null
        assert view == '/authentication/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/authentication/show/1'
        assert controller.flash.message != null
        assert Authentication.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/authentication/list'


        populateValidParams(params)
        def authentication = new Authentication(params)

        assert authentication.save() != null

        params.id = authentication.id

        def model = controller.show()

        assert model.authenticationInstance == authentication
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/authentication/list'


        populateValidParams(params)
        def authentication = new Authentication(params)

        assert authentication.save() != null

        params.id = authentication.id

        def model = controller.edit()

        assert model.authenticationInstance == authentication
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/authentication/list'

        response.reset()


        populateValidParams(params)
        def authentication = new Authentication(params)

        assert authentication.save() != null

        // test invalid parameters in update
        params.id = authentication.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/authentication/edit"
        assert model.authenticationInstance != null

        authentication.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/authentication/show/$authentication.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        authentication.clearErrors()

        populateValidParams(params)
        params.id = authentication.id
        params.version = -1
        controller.update()

        assert view == "/authentication/edit"
        assert model.authenticationInstance != null
        assert model.authenticationInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/authentication/list'

        response.reset()

        populateValidParams(params)
        def authentication = new Authentication(params)

        assert authentication.save() != null
        assert Authentication.count() == 1

        params.id = authentication.id

        controller.delete()

        assert Authentication.count() == 0
        assert Authentication.get(authentication.id) == null
        assert response.redirectedUrl == '/authentication/list'
    }
}
